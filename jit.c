#include <stdio.h> 
#include <sys/mman.h>
// A normal function with an int parameter 
// and void return type 
int fun(int a, int b) 
{ 
	return a+b;
} 
  
int temp = 0;
int main(){ 
	uint64_t in0 = 1, in1 = 2, out;
    __asm__ (
        "add %[out], %[in0], %[in1];"
        : [out] "=r" (out)
        : [in0] "r"  (in0),
          [in1] "r"  (in1)
    );
		fprintf(stderr, "out: %ld\n",out);

    // fun_ptr is a pointer to function fun()  
    int (*fun_ptr)(int,int) = &fun; 
		printf("sizeof(fun): %ld\n",sizeof(*fun));
		void * ptr=fun_ptr;
    void *newFunc = mmap(NULL, 8*4, PROT_WRITE | PROT_EXEC,
                   MAP_ANON | MAP_PRIVATE, -1, 0);
		/*
		unsigned char code[]={
			0xff,
			0x43,
			0x00,
			0xd1,

			0x0f,
			0x00,
			0xb9,
			0xe0,

			0xb9,
			0x00,
			0x0b,
			0xe1,

			0xb9,
			0x40,
			0x0f,
			0xe8,

			0xb9,
			0x40,
			0x0b,
			0xe9,       

			0x0b,
			0x09,
			0x01,
			0x00,

			0x91,
			0x00,
			0x43,
			0xff,

			0xd6,
			0x5f,
			0x03,
			0xc0
		};*/
		/*
		for(int i=0;i<8;++i){
			newFunc[i]=ptr[i];
			printf("0x%08x\n",ptr[i]);
		}*/
		memcpy(newFunc,ptr,8*4);
		for(int i=0;i<8;++i){
			printf("oldfun: 0x%08x\n",*(int*)(ptr+i));
			printf("newfun: 0x%08x\n",*(int*)(newFunc+i));
		}

  
    /* The above line is equivalent of following two 
       void (*fun_ptr)(int); 
       fun_ptr = &fun;  
    */
  
    // Invoking fun() using fun_ptr 
		printf("oldfunc: %d\n", (*fun_ptr)(7,5));
		fun_ptr=newFunc;
		printf("newfunc: %d\n", (*fun_ptr)(7,5));
  
    return 0; 
} 
