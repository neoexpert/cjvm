# JVM C implementation

you need the libzip library to compile the JVM and maven to compile the JDK.

```
git submodule update --init --recursive &&
sudo apt install libzip-dev &&
make &&
cd jdk/minijdk &&
#sudo apt install maven &&
mvn clean install &&
cd ../../ &&
cp jdk/minijdk/target/minijdk.jar rt.jar &&
javac Main.java &&
./java Main

```
