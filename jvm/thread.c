#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stdarg.h>
#include <pthread.h>
#include <ffi.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../util/linkedlist.h"
#include "../util/iarraylist.h"
#include "../lang/strings.h"
#include "../io/buffer.h"
#include "constant.h"
#include "attribute.h"
#include "method.h"
#include "field.h"
#include "class.h"
#include "heap.h"
#include "frame.h"
#include "thread.h"
#include "bytecodeexecutor.h"

Thread * newThread(Method * m, Constant ** cpool){
	Thread * t=malloc(sizeof(Thread));
	t->stack=newFrameStack(4096,4096, 1024);
	t->threadNode=LinkedList_add(threads, t);
	return t;
}


int Thread_start(Thread * t, Method * m, Constant ** cpool, int argc, ...){
	pthread_setspecific(glob_threadID_key, t);

	va_list ap;
	va_start(ap, argc);
	t->stack->lv_stack[0]=0;
	for (int i = 0; i < argc; ++i) {
		t->stack->lv_stack[i+1]=va_arg(ap, int32_t);
	}

	uint16_t max_lv=m->max_lv;
	Frame * f=newFrame(t->stack, m, t->stack->lv_stack+1 ,max_lv+1);
	//pushFrame(t->stack,f);
	int res=step(m, f, max_lv+1, t->stack);
	LinkedList_removeNode(threads, t->threadNode);
	deleteThread(t);
	return res;
}
void deleteThread(Thread * t){
	deleteFrameStack(t->stack);
	//free(t);
}
