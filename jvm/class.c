#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <ffi.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../lang/strings.h"
#include "../util/linkedlist.h"
#include "../util/iarraylist.h"
#include "../util/arraylist.h"
#include "../util/i8arraylist.h"
#include "../io/buffer.h"
#include "constant.h"
#include "attribute.h"
#include "method.h"
#include "field.h"
#include "class.h"
#include "classloader.h"
#include "heap.h"

Class * readClass(Buffer * buf){
	int head = getInt(buf);
	if(head!=HEAD){
		fprintf(stderr, "corrupt class file\n");
		return NULL;
	}
	uint16_t ver=getU16(buf);
	uint16_t minor=getU16(buf);
#ifdef VERBOSE
	fprintf(stderr, "loading class version %d.%d\n",minor,ver);
#endif

	Class * class = malloc(sizeof(Class));
	class->orefIndex=-1;

	uint16_t cpoolSize=getU16(buf);
	class->cpoolSize=cpoolSize;
#ifdef VERBOSE
	fprintf(stderr, "constantPoolCount: %d\n",cpoolSize);
#endif
	class->cpool=malloc(sizeof(Constant*)*cpoolSize);
	Constant ** cpool=class->cpool;

	//methods_to_resolve
	LinkedList * mtr=newLinkedList();
	//fields_to_resolve
	LinkedList * ftr=newLinkedList();
	for(uint16_t i=1;i<cpoolSize;++i){
		Constant * c=readConstant(buf);
		class->cpool[i]=c;
		switch(c->tag){
			case CLong:
			case CDouble:
				++i;
				class->cpool[i]=NULL;
				break;
			case CMethodRef:
			case CInterfaceMethodRef:
				LinkedList_add(mtr,c);
				break;
			case CFieldRef:
				LinkedList_add(ftr,c);
				break;
		}
	}
	Constant * c;
	Constant * cclass;
	Constant * cNameAndType;
	Constant * cname;
	Constant * ctype;
	while((c=LinkedList_removeFirst(mtr))!=NULL){
		cclass=cpool[c->index1];
		cclass=cpool[cclass->index1];
		//c->classname=cclass->str;
		
		cNameAndType=cpool[c->index2];
		cname=cpool[cNameAndType->index1];
		ctype=cpool[cNameAndType->index2];
		
		MethodType * mt=ctype->data;
		if(mt==NULL){
			mt=parseMethodType(ctype->str);
			ctype->data=mt;
		}
		String * signature=intern(concat(cname->str, ctype->str));
		//String * type=ctype->str;
		MethodHead * mh=malloc(sizeof(MethodHead));
		mh->classname=cclass->str;
		mh->mt=mt;
		mh->signature=signature;
		mh->name=cname->str;
		c->data=mh;
	}
	free(mtr);

	while((c=LinkedList_removeFirst(ftr))!=NULL){
		cclass=cpool[c->index1];
		cclass=cpool[cclass->index1];
		FieldHead * fh=malloc(sizeof(FieldHead));
		fh->classname=cclass->str;
		c->data=fh;
		//c->classname=cclass->str;
		
		cNameAndType=cpool[c->index2];
		cname=cpool[cNameAndType->index1];
		ctype=cpool[cNameAndType->index2];
		fh->type=ctype->str;
	}
	free(ftr);

	class->accessFlags=getU16(buf);
	uint16_t this=getU16(buf);
	uint16_t super=getU16(buf);
	
	class->name = cpool[cpool[this]->index1]->str;
	class->fileName = class->name;
#ifdef VERBOSE
	println(class->name);
#endif
	Class * superClass=NULL;
	int foffset=0;
	int iRefsCount=0;
	if(super!=0){
		String * superClassName=cpool[cpool[super]->index1]->str;
		superClass=getClass(superClassName, false);
		if(superClass==NULL){
			fprintf(stderr, "could not read Class ");
			fprint(stderr,superClassName);
			fprintf(stderr,"\n");
			return NULL;
		}
		foffset=superClass->iFieldsCount;
	 	//iRefsCount=superClass->iRefsCount;
#ifdef VERBOSE
		fprintf(stderr, "super: ");
		fprintln(stderr, superClass->name);
#endif
	}
	class->superClass = superClass;
	//interfaces count
	uint16_t ifacesCount=getU16(buf);
	for(uint16_t i=0;i<ifacesCount;++i){
		uint16_t ix=getU16(buf);
		Constant * iref=cpool[ix];
	}
	uint16_t fieldsCount=getU16(buf);
	class->fieldsCount=fieldsCount;
#ifdef VERBOSE
	fprintf(stderr, "fields: %d\n", fieldsCount);
#endif
	//fieldsTable
	class->ft=newHashMapRU(fieldsCount);
	class->fields
	= malloc(fieldsCount * sizeof(Field*));

	//instance refs list
	LinkedList * irl=newLinkedList();
	//instance fields list
	LinkedList * ifl=newLinkedList();
	int iFieldsCount=0;
	for(uint16_t i=0;i<fieldsCount;++i){
		Field * f=readField(buf,cpool);
		class->fields[i]=f;
		HashMap_put(class->ft,f->name, f);
		if(f->isStatic){
			switch(f->type->str[0]){
				case 'L':
				case '[':
					f->index=addStaticRef();
					break;
				default:
					f->index=addStaticField();
					*(int32_t*)(static_fields+f->index)
						=f->default_value;
					break;
				case 'F':
					f->index=addStaticField();
					*(float*)(static_fields+f->index)
						=f->default_value;
					break;
				case 'J':
					f->index=addStaticField64();
					*(int64_t*)(static_fields+f->index)
						=f->default_value;
					break;
				case 'D':
					f->index=addStaticField64();
					*(double*)(static_fields+f->index)
						=f->default_value;
					break;

			}
		}
		else{
			switch(f->type->str[0]){
				case 'L':
				case '[':
					iRefsCount+=1;
					iFieldsCount+=1;
					LinkedList_add(irl, f);
					break;
				default:
					iFieldsCount+=1;
					LinkedList_add(ifl, f);
					break;
				case 'J':
				case 'D':
					iFieldsCount+=2;
					LinkedList_add(ifl, f);
					break;
			}
		}
#ifdef VERBOSE
		fprintf(stderr, "field ");
		if(f->isStatic)
			fprintf(stderr, "static ");
		print(f->type);
		fprintf(stderr, ".");
		fprintln(stderr, f->name);
#endif
	}
	class->iRefsCount=iRefsCount;
	//IArrayList_fixSize(ifa);

	if(superClass!=NULL){
#ifdef VERBOSE
		fprintf(stderr, "instance fields size: %d * 4 bytes\n",iFieldsCount);
#endif

		class->iFieldsCount=iFieldsCount+superClass->iFieldsCount;
	}
	else{
		class->iFieldsCount=iFieldsCount;
	}
	int32_t * instanceTemplate=malloc(4*iFieldsCount);
	//fprintf(stderr, "iFieldsCount: %d, foffset: %d\n",class->iFieldsCount, foffset);
	int8_t * refsMap=calloc(class->iFieldsCount,1);
	Field * f;
	int findex=0;
	while((f=LinkedList_removeFirst(irl))){
			f->index=foffset+findex;
			switch(f->type->str[0]){
				case 'L':
				case '[':
					*(int32_t*)(instanceTemplate+findex)=0;
					refsMap[f->index]=1;
					findex+=1;
					break;
				default:
					fprintf(stderr, "should not happen");
					exit(-1);
					break;
			}
	}
	free(irl);
	while((f=LinkedList_removeFirst(ifl))){
			f->index=foffset+findex;
			switch(f->type->str[0]){
				default:
					*(int32_t*)(instanceTemplate+findex)=
						f->default_value;
					findex+=1;
					break;
				case 'J':
				case 'D':
					*(int64_t*)(instanceTemplate+findex)=
						f->default_value;
					findex+=2;
					break;
			}
	}
	free(ifl);
	
	class->instanceFieldsTemplate=malloc(4*class->iFieldsCount);

	if(superClass!=NULL){
		//merge all instance fields from all super classes into one array
		memcpy(class->instanceFieldsTemplate, superClass->instanceFieldsTemplate, 4*superClass->iFieldsCount);
		memcpy(refsMap, superClass->refsMap, superClass->iFieldsCount);
	}
	class->refsMap=refsMap;
#ifdef VERBOSE
	fprintln(stderr, class->name);
	fprintf(stderr, "refsMap:\n");
	for(int i=0;i<class->iFieldsCount;++i){
		fprintf(stderr, "%d, ",refsMap[i]);
	}
	fprintf(stderr, "\n");
	fprintf(stderr,"foffset is %d\n",foffset);
#endif
	memcpy(class->instanceFieldsTemplate+foffset, instanceTemplate, 4*iFieldsCount);
	free(instanceTemplate);
#ifdef VERBOSE
	for(int i=0;i<class->iFieldsCount;++i){
		if(superClass!=NULL)
			if(i==superClass->iFieldsCount)
				fprintf(stderr, "my fields:\n");
		fprintf(stderr, "field %d, value: %d\n",i,class->instanceFieldsTemplate[i]);
	}
	fprintf(stderr, "\n");
#endif

	class->iSize=(class->iFieldsCount)*4;

	uint16_t methodsCount=getU16(buf);
	class->methodsCount=methodsCount;
	class->methods
	= malloc(methodsCount * sizeof(Method*));

	//methodTable
	HashMap * smt=newHashMapRU(methodsCount);
	HashMap * vmt=newHashMapRU(methodsCount);
	class->smt=smt;
	class->vmt=vmt;
#ifdef VERBOSE
	fprintf(stderr, "_methods: %d\n", methodsCount);
#endif
	for(uint16_t i=0;i<methodsCount;++i){
		Method * m=readMethod(buf,cpool);
		m->clazz=class;
		class->methods[i]=m;
#ifdef VERBOSE
		fprintf(stderr, "%d method: ",i);
		println(m->signature);
#endif
		if(m->isVirtual)
			HashMap_put(vmt,m->signature, m);
		else
			HashMap_put(smt,m->signature, m);
	}
	uint16_t attrsCount=getU16(buf);
	class->attrsCount=attrsCount;
	Attribute ** attrs
	= malloc(attrsCount * sizeof(Attribute*));
	class->attrs=attrs;

	for(uint16_t i=0;i<attrsCount;++i){
		attrs[i]=readAttribute(buf,cpool);
	}

	return class;
}

void deleteClass(Class * clazz){
	Constant ** cpool=clazz->cpool;
	for(int i=0;i<clazz->fieldsCount;++i)
		deleteField(clazz->fields[i]);
	free(clazz->fields);
	for(int i=0;i<clazz->methodsCount;++i)
		deleteMethod(clazz->methods[i],cpool);
	free(clazz->methods);
	free(clazz->refsMap);
	free(clazz->instanceFieldsTemplate);
	for(int i=1;i<clazz->cpoolSize;++i){
		Constant * c=cpool[i];
		if(c!=NULL)
			deleteConstantContents(c,cpool);
	}
	for(int i=1;i<clazz->cpoolSize;++i){
		Constant * c=cpool[i];
		if(c!=NULL)
			free(c);
	}
	free(cpool);
	HashMap_delete(clazz->ft);
	HashMap_delete(clazz->smt);
	HashMap_delete(clazz->vmt);
	for(uint16_t i=0;i<clazz->attrsCount;++i){
		free(clazz->attrs[i]);	
	}
	free(clazz->attrs);
	free(clazz);
}

Method * getMethod(Class * clazz, String * signature){
#ifdef VERBOSE
	fprintf(stderr, "getMethod ");
	fprint(stderr, clazz->name);
	fprintf(stderr, ".");
 	fprintln(stderr, signature);
#endif
	Method * m=HashMap_get(clazz->vmt, signature);
	if(m!=NULL)return m;
	Class * super = clazz->superClass;
	if(super==NULL)return NULL;
	return getMethod(super, signature);
}
bool isInstance(Class * this, Class * other){
	if(this==other)
		return true;
	Class * super=this->superClass;
	if(super==NULL)
		return false;
	return isInstance(super, other);
}
/*
void Class_LDC(Constant ** cpool, uint16_t index){
	Constant * c=cpool[index];
	switch(c->tag){
		default:
			fprintf(stderr, "Class_LDC: unknown constant type. index: %d, tag: %d\n",index, c->tag);
			fprintf(stderr, "index1: %d index2: %d\n",c->index1, c->index2);
			exit(0);
			return;
		case CInteger:
		case CFloat:
		case CLong:
		case CDouble:
			break;
		case CString:
			c->value=static_refs+createStringInstance(cpool[c->index1]->str);
			fprintf(stderr, "LDC String: ref: %d\n",*(int32_t*)c->value);
			break;
	}
}*/
Field * resolveField(Class * clazz, String * name){
	Field * f = HashMap_get(clazz->ft,name);
	if(f!=NULL)
		return f;
	if(clazz->superClass==NULL)
		return NULL;
	return resolveField(clazz->superClass, name);
}

