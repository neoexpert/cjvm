#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <ffi.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../util/linkedlist.h"
#include "../util/iarraylist.h"
#include "../util/arraylist.h"
#include "../lang/strings.h"
#include "../io/buffer.h"
#include "constant.h"
#include "attribute.h"
#include "method.h"
#include "field.h"
#include "class.h"
#include "classloader.h"
#include "heap.h"
#include "frame.h"
#include "jni.h"
Frame * newFrame(FrameStack * fstack, Method * m, int32_t * lv, int stackoffset){
	if(fstack->pos>=fstack->capacity){
		int newCapacity=(fstack->frameCapacity*3)/2+1;
		void * newStack=realloc(fstack->stack, newCapacity*sizeof(Frame));
		if(newStack==NULL){
			fprintf(stderr, "STACKOWERFLOW: %d frames \n", fstack->frameCapacity);
		}
		fstack->stack=newStack;
		fprintf(stderr, "frameStack increased %d -> %d\n", fstack->frameCapacity, newCapacity);
		fstack->frameCapacity=newCapacity;
		fstack->capacity=newCapacity*sizeof(Frame);
	}
	Frame * f = fstack->stack+fstack->pos;
	f->code=m->code;
	f->method=m;
	f->stackpos=stackoffset;
	f->cpool=m->cpool;
	f->lv=lv;
	f->pc=0;
	fstack->pos+=sizeof(Frame);
	return f;
}

FrameStack * newFrameStack(int frameCapacity, int lv_stack_capacity, int16_t max_jni_refs){
	FrameStack * fstack=malloc(sizeof(FrameStack));
	fstack->lv_stack=malloc(4*lv_stack_capacity);
	fstack->lv_stack[0]=0;
	fstack->lv_stack_capacity=lv_stack_capacity;
	fstack->stack=malloc(frameCapacity*sizeof(Frame));
	//fstack->stack[0]=NULL;
	fstack->pos=0;
	fstack->frameCapacity=frameCapacity;
	fstack->capacity=frameCapacity*sizeof(Frame);
	fstack->jni_refs=calloc(max_jni_refs, sizeof(uint32_t));
	fstack->max_jni_refs=max_jni_refs;
	fstack->jni_refs_count=0;
	return fstack;
}

uint32_t * createJNIRef(FrameStack * fstack, uint32_t ref){
	if(fstack->jni_refs_count>=fstack->max_jni_refs){
		fprintf(stderr, "max_jni_references overflow: %d\n", fstack->jni_refs_count);
		exit(-1);
	}
	uint16_t index=fstack->jni_refs_count;
	fstack->jni_refs_count += 1;
	fstack->jni_refs[index]=ref;
	return &fstack->jni_refs[index];
}

void clearJNIRefs(FrameStack * fstack){
	fstack->jni_refs_count = 0;
}

void freeJNIRef(FrameStack * fstack, uint32_t * ref){
	//*ref=0;
	int jni_refs_count=fstack->jni_refs_count;
	uint32_t * refs=fstack->jni_refs;
	if(refs+fstack->jni_refs_count==ref){
		//last ref deleted
		--jni_refs_count;
		while(jni_refs_count>0){
			if(refs[jni_refs_count-1]!=0)
				break;
			--jni_refs_count;
		}
	}
}

void growFrameStack(FrameStack * fstack, int min_capacity){
	int newCapacity=(fstack->lv_stack_capacity*3)/2;
	if(newCapacity<min_capacity)
		newCapacity=min_capacity;
	void * newLVStack=realloc(fstack->lv_stack, newCapacity);
	if(newLVStack==NULL){
			fprintf(stderr, "STACKOWERFLOW: %d capacity: \n", newCapacity);
			exit(-1);
	}
	if(newLVStack!=fstack->lv_stack){
			fprintf(stderr, "lv_stackpointrer changed\n");
			fstack->lv_stack=newLVStack;
			uint32_t lvpos=0;
			int size=getStackSize(fstack);
			for(int i=0;i<size;++i){
				Frame * f = getFrame(fstack, i);
				Method * m=f->method;
				f->lv=fstack->lv_stack+lvpos;
				lvpos+=(m->max_lv + m->max_stack - m->argsLength);
			}
	}
}


/*
void pushFrame(FrameStack * fstack, Frame * f){
	if(fstack->pos>=fstack->frameCapacity){
		int newCapacity=(fstack->frameCapacity*3)/2+1;
		void * newStack=realloc(fstack->stack, newCapacity*sizeof(Frame));
		if(newStack==NULL){
			fprintf(stderr, "STACKOWERFLOW: %d frames \n", fstack->frameCapacity);
		}
		fstack->stack=newStack;
		fprintf(stderr, "frameStack increased %d -> %d\n", fstack->frameCapacity, newCapacity);
		fstack->frameCapacity=newCapacity;
	}
	fstack->stack[fstack->pos++]=f;
}*/

Frame * popFrameAndPeek(FrameStack * fstack){
	fstack->pos-=sizeof(Frame);
	if(fstack->pos<=0)
		return NULL;
	return fstack->stack+fstack->pos-sizeof(Frame);
}

int getStackSize(FrameStack * fstack){
	return (fstack->pos)/sizeof(Frame);
}
Frame * getFrame(FrameStack * fstack, uint32_t index){
	return fstack->stack+index*sizeof(Frame);
}
Frame * throwException(Class * eClazz, FrameStack * fstack, Frame * cF, int pc, int stackPos){
	//fprintf(stderr, "stackPos: %d\n", stackPos);
#ifdef VERBOSE
	fprintf(stderr, "throw exception: ");
	fprintln(stderr, eClazz->name);
	fprintf(stderr, "method is: ");
	fprintln(stderr, cF->method->signature);
#endif
	Exception ** etable=cF->method->etable;
	int len=cF->method->etableLen;
	Constant ** cpool = cF->cpool;
	for(int i=0;i<len;++i){
		Exception * e=etable[i];
		if (e->start_pc <= pc && e->end_pc >= pc){
			Class * class;
			if(e->catch_type!=0){
				Constant * c=cpool[e->catch_type];
				c=cpool[c->index1];
				class = getClass(c->str,false);
			}
			else class=NULL;
			if(isInstance(eClazz, class)){
				//exception caught
				cF->pc=e->handler_pc;
				//Frame * prevFrame=getFrame(fstack, stackPos-1);
				//clear operand stack
				cF->stackpos = cF->lv + cF->method->max_lv - fstack->lv_stack;
				//cF->stackpos = cF->lv + cF->method->max_lv;

				//push exception instance reference to the operand stack
				fstack->lv_stack[cF->stackpos++]=fstack->lv_stack[0];
				fstack->lv_stack[0]=0;
				fstack->pos=(stackPos+1)*sizeof(Frame);
				return cF;
			}
#ifdef VERBOSE
			fprintf(stderr, "class is: ");
			if(class!=NULL)
				fprintln(stderr, class->name);
			else
				fprintf(stderr, "null\n");
#endif
		}
	}
	//free(cF);
	//fprintf(stderr, "stackpos: %d\n",stackPos);
	if(stackPos>0)
		cF=getFrame(fstack, stackPos-1);
	else{
		Class * throwableClass=getClass(JAVA_LANG_THROWABLE, false);
		Method * m=HashMap_get(throwableClass->vmt,intern(_s("printStackTrace()V")));
		uint16_t max_lv=m->max_lv;
		fstack->pos=0;
		Frame * f=newFrame(fstack, m, fstack->lv_stack+1, max_lv+1);
		//fprintln(stderr, eClazz->name);
		//fprintf(stderr, "eoref: %d\n", fstack->lv_stack[0]);
		//dumpInstance(fstack->lv_stack[0]);
		fstack->lv_stack[1]=fstack->lv_stack[0];
		//pushFrame(fstack, f);
		return f;
	}
	return throwException(eClazz, fstack, cF, cF->pc, stackPos-1);
}

Frame * throwNewException(String * name, FrameStack * fstack, Frame * cF, int pc, String * message){
	//fprintln(stderr, name);
	Class * clazz=getClass(name,true);
	//fprintln(stderr, clazz->name);
	if(clazz==NULL){
		fprintf(stderr ," clazz was NULL\n");
		exit(-1);
	}
	uint32_t oref=createClassInstance(clazz);
	if(message!=NULL)
		fprintln(stderr, message);
	fstack->lv_stack[0]=oref;
	fillInStackTrace(jniEnv, (uint32_t*)&fstack->lv_stack[0]);
	//ClassInstance * ci=heap+oref-sizeof(ClassInstance);
	return throwException(clazz, fstack, cF, pc, (fstack->pos-1)/sizeof(Frame));
}

void deleteFrameStack(FrameStack * fstack){
	free(fstack->lv_stack);
	/*
	for(int i=0;i<fstack->pos;++i){
		Frame * f=fstack->stack[i];
		if(f!=NULL)
			free(f);
	}*/
	free(fstack->stack);
	free(fstack);
}
