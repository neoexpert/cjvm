#ifndef CONSTANT_H
#define CONSTANT_H
typedef struct {
	char tag;
	int index1;
	int index2;
	void * data;
	union{
		String * str;
		void * value;
		/*
		int32_t ivalue;
		int64_t lvalue;
		float fvalue;
		double dvalue;*/
	};
	//void * value;
}Constant;
typedef struct {
	int argsLength;
	int8_t * args;
	int8_t returnType;
	int8_t returnWidth;
}MethodType;
MethodType * parseMethodType(String * type);
void deleteMethodType(MethodType * mt);
Constant * readConstant(Buffer * buf);
void deleteConstantContents(Constant * c, Constant ** cpool);
#define CUtf8 1
#define CInteger 3
#define CFloat 4
#define CLong 5
#define CDouble 6
#define CClass 7
#define CString 8
#define CFieldRef 9
#define CMethodRef 10
#define CInterfaceMethodRef 11
#define CNameAndType 12
#define CMethodHandle 15
#define CMethodType 16
#define CInvokeDynamic 18

#define CInvokeStatic 64
#define CInvokeStaticNative 65
#define CInvokeSpecial 66
#define CInvokeSpecialNative 67
#define CInvokeVirtual 68
#endif
