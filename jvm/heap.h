#ifndef HEAP_H
#define HEAP_H
void * heap;
pthread_mutex_t heapLock;
LinkedList * threads;

//static refs arraylist
IArrayList * sra;
uint32_t * static_refs;

//uint16_t dogc;
//static fields arraylist
IArrayList * sfa;
int32_t * static_fields;


typedef struct {
	int32_t capacity;
	int32_t size;
	void * heap;
}Heap;
Heap * _heap;

typedef struct {
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t dummy;
}ClassInstance;

typedef struct{
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}ArrayInstance;

typedef struct{
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}ByteArrayInstance;

typedef struct {
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}ShortArrayInstance;

typedef struct {
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}CharArrayInstance;

typedef struct {
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}IntArrayInstance;

typedef struct {
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}ObjectArrayInstance;

typedef struct {
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}FloatArrayInstance;

typedef struct {
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}LongArrayInstance;

typedef struct {
	union{
	uint32_t isize;
	uint32_t newAddr;
	};
	uint32_t gc_beats;
	uint8_t type;
	Class * clazz;
	int32_t size;
}DoubleArrayInstance;

void initHeap(int edenCapacity);
void dumpHeap(uint32_t offset, Heap * heap);
void dumpCurrentHeap();
void dumpThreads();
uint32_t createClassInstance(Class * class);
void dumpInstance(uint32_t oref);
uint32_t createArrayInstance(int8_t type, int32_t size);
uint32_t multianewarray(uint8_t dimensions, uint8_t dimension, int32_t * sizes, char type);
void removeGarbage();
void putField(uint32_t oref, String * name, int32_t value);

int32_t getField(uint32_t oref, String * name);

int addStaticRef();
int addStaticField();
int addStaticField64();
uint32_t processRef(Heap * heap, uint32_t ref);
uint32_t processRefM(uint32_t ref);
Heap * createHeap(uint32_t capacity);
void deleteHeap(Heap * heap);
void gc();
void disableGC();
void enableGC();
#define T_OBJECT 3
#define T_BOOLEAN 4
#define T_CHAR 5
#define T_FLOAT 6
#define T_DOUBLE 7
#define T_BYTE 8
#define T_SHORT 9
#define T_INT 10
#define T_LONG 11
#define CLASSINSTANCE 1
#define OBJECT_ARRAYINSTANCE 3
#define BOOLEAN_ARRAYINSTANCE  4
#define CHAR_ARRAYINSTANCE  5
#define FLOAT_ARRAYINSTANCE  6
#define DOUBLE_ARRAYINSTANCE  7
#define BYTE_ARRAYINSTANCE  8
#define SHORT_ARRAYINSTANCE  9
#define INT_ARRAYINSTANCE  10
#define LONG_ARRAYINSTANCE  11
#define MOVED_INSTANCE  15
#endif
