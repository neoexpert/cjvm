#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../util/i8arraylist.h"
#include "../io/buffer.h"
#include "constant.h"

Constant * readConstant(Buffer * buf){
	Constant * c =malloc(sizeof(Constant));
	uint8_t tag=get(buf);
	c->tag=tag;
	c->data=NULL;
	switch (tag) {
		default:
			printf("wrong constant tag: %d\n", tag);
			exit(-1);
			return c;
		case CUtf8:
			c->str=getUTF8(buf);
			return c;
		case CClass:
		case CString:
		case CMethodType:
			c->index1 = getU16(buf);
			c->index2=0;
			c->value=NULL;
			return c;
		case CFieldRef:
		case CMethodRef:
		case CInterfaceMethodRef:
		case CNameAndType:
			c->index1 = getU16(buf);
			c->index2 = getU16(buf);
			c->value=NULL;
			c->data=NULL;
			return c;
		case CInteger:
			c->value=malloc(4);
			*(int32_t*)(c->value) = getInt(buf);
			return c;
		case CFloat:
			c->value=malloc(4);
			*(float*)(c->value) = getFloat(buf);
			return c;
		case CDouble:
			c->value=malloc(8);
			*(double*)(c->value) = getDouble(buf);
			return c;
		case CLong:
			c->value=malloc(8);
			*(int64_t*)(c->value) = getLong(buf);
			return c;
		case CMethodHandle:
			c->index1 = get(buf);
			c->index2 = getU16(buf);
			c->value=NULL;
			return c;
		case CInvokeDynamic:
			c->index1 = getU16(buf);
			c->index2 = getU16(buf);
			c->value=NULL;
			return c;
	}
}

void deleteConstantContents(Constant * c, Constant ** cpool){
	switch(c->tag){
		//case CString:
		case CUtf8:
			break;
		case CFieldRef:
			if(c->data!=NULL)
				free(c->data);
			break;
		case CMethodRef:
		case CInterfaceMethodRef:
			free(c->data);
			Constant * cNameAndType=cpool[c->index2];
			//cname=cpool[cNameAndType->index1];
			Constant * ctype=cpool[cNameAndType->index2];
			if(ctype->data!=NULL){
				deleteMethodType(ctype->data);
				ctype->data=NULL;
			}
			break;
		case CNameAndType:
			ctype=cpool[c->index2];
			if(ctype->data!=NULL){
				deleteMethodType(ctype->data);
				ctype->data=NULL;
			}
			break;
		default:
		if(c->value!=NULL)
			free(c->value);
	}
}
MethodType * parseMethodType(String * type){
	/*
	fprintf(stderr, "processing MethodType: ");
	fprintln(stderr, type);
	*/
	I8ArrayList * args=newI8ArrayList(8);
	int length=0;
	uint16_t * t=type->str;
	uint32_t len=type->length;
	int8_t returnType;
	for(uint32_t i=1;i<len;++i){
		switch(t[i]){
			case 'L':
				I8ArrayList_add(args, 'L');
				++length;
				while(t[i]!=';')++i;
				break;
			case '[':
				I8ArrayList_add(args, '[');
				++length;
				while(t[i]=='[')++i;
				if(t[i]=='L')
					while(t[i]!=';')++i;
				break;
			case 'J':
			case 'D':
				I8ArrayList_add(args, t[i]);
				I8ArrayList_add(args, t[i]);
				length+=2;
				break;
			default:
				//fprintf(stderr, "param: %c \n",t[i]);
				I8ArrayList_add(args, t[i]);
				++length;
				break;
			case ')':
				returnType=t[i+1];
				//fprintf(stderr, "length: %d \n",length);
				I8ArrayList_fixSize(args);
				MethodType * mt=malloc(sizeof(MethodType));
				mt->argsLength=length;
				mt->args=args->values;
				mt->returnType=returnType;
				switch(returnType){
					case 'V':
						mt->returnWidth=0;
						break;
					case 'L':
					case '[':
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						mt->returnWidth=1;
						break;
					case 'J':
					case 'D':
						mt->returnWidth=2;
						break;
					default:
						fprintf(stderr, "unknown returntype: %c\n", returnType);
						exit(-1);
				}
				free(args);
				return mt;
		}
	}
	fprintf(stderr, "could not parse MethodType:");
	fprintln(stderr, type);
	exit(-1);
	return NULL;
}
void deleteMethodType(MethodType * mt){
	free(mt->args);
	free(mt);
}
