#ifndef JVM_H
#define JVM_H
typedef struct {
	int * framestack;
	int max_stack_size;
}JVM;
JVM * createJVM();
void invoke(JVM * jvm, Method * m, int32_t args, Constant ** cpool);
void initializeConstants();
#endif
