#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../lang/strings.h"
#include "../io/buffer.h"
#include "constant.h"
#include "attribute.h"
#include "field.h"
#include "ACCESS_MODIFIERS.h"

Field * readField(Buffer * buf,Constant ** cpool){
	Field * f=malloc(sizeof(Field));
	f->accessFlags=getU16(buf);
	f->isStatic=ISSTATIC(f->accessFlags);
	uint16_t nameIndex=getU16(buf);
	f->name=cpool[nameIndex]->str;
	uint16_t typeIndex=getU16(buf);
	f->type=cpool[typeIndex]->str;
	char t=f->type->str[0];
	f->isRef=(t=='L'||t=='[');
	
	uint16_t attrsCount=getU16(buf);
	f->attrsCount=attrsCount;

	Attribute ** attrs
	= malloc(attrsCount * sizeof(Attribute*));
	f->attrs=attrs;
	f->default_value=0;

	for(uint16_t i=0;i<attrsCount;++i){
		Attribute * a=readAttribute(buf,cpool);
		if(a->name==constantValue){
			printf("constantValueAttribute found\n");
		}
		attrs[i]=a;
	}

	return f;
}

void initFields(){
	constantValue=intern(_s("ConstantValue"));
}

void deleteField(Field * f){
	for(uint16_t i=0;i<f->attrsCount;++i){
		deleteAttribute(f->attrs[i]);
	}
	free(f->attrs);
	free(f);
}
