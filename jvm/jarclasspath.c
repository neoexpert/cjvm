#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <zip.h>
#include <ffi.h>
#include "../types.h"
#include "../lang/string.h"
#include "../util/arraylist.h"
#include "../util/hashmap.h"
#include "../io/buffer.h"
#include "../lang/strings.h"
#include "constant.h"
#include "attribute.h"
#include "field.h"
#include "method.h"
#include "class.h"
#include "classloader.h"
#include "jarclasspath.h"


Buffer * getBufferFromJarFile(ClassPath * cp, char * entryName){
	Buffer * buf=readzipEntry(cp->source, entryName);
	return buf;
}

ClassPath * addJarClassPath(char * path){
	ClassPath * cp=malloc(sizeof(ClassPath));
	void * zip = openZipFile(path);
	if(zip==NULL){
		fprintf(stderr, "error opening zip file: %s\n", path);
		return NULL;
	}
	cp->source=zip;
	cp->code_source=intern(_s(path));
	cp->getBuffer=(BufferGetter*)&getBufferFromJarFile;
	cp->close=(ClassPathCloser*)&closeJarClassPath;
	ArrayList_add(classpath, cp);
	return cp;
}

void closeJarClassPath(ClassPath * cp){
    zip_close(cp->source);
}


