#ifndef JARCLASSPATH_H
#define JARCLASSPATH_H
typedef struct zip zip;
typedef struct zip_file zip_file;
Buffer * readzipEntry(zip * zipFile, const char * path);
void * openZipFile(char * path);
zip * rt;
Buffer * getBufferFromJarFile(ClassPath * cp, char * className);
void closeJarClassPath(ClassPath * cp);
ClassPath * addJarClassPath(char * path);
#endif
