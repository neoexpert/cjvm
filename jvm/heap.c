#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <pthread.h>
#include <string.h>
#include <ffi.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../util/ihashmap.h"
#include "../lang/strings.h"
#include "../util/linkedlist.h"
#include "../util/iarraylist.h"
#include "../util/arraylist.h"
#include "../io/buffer.h"
#include "constant.h"
#include "attribute.h"
#include "field.h"
#include "method.h"
#include "class.h"
#include "classloader.h"
#include "heap.h"
#include "frame.h"
#include "thread.h"
#include "jni.h"
#define VERBOSED

// Declaration of thread condition variable
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;

// declaring mutex
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t heapLock = PTHREAD_MUTEX_INITIALIZER;

void *myThreadFun(void *vargp){
	while(true){
#ifdef VERBOSE
		printf("Printing GeeksQuiz from Thread \n");
#endif
		pthread_mutex_lock(&lock);
#ifdef VERBOSE
		printf("Waiting on condition variable cond1\n");
#endif
		pthread_cond_wait(&cond1, &lock);

		// Let's signal condition variable cond1
#ifdef VERBOSE
		printf("Signaling condition variable cond1\n");
#endif
		pthread_cond_signal(&cond1);
		pthread_mutex_unlock(&lock);

#ifdef VERBOSE
		printf("Returning thread\n");
#endif

	}
	return NULL;
}

void removeGarbage(){
		pthread_cond_signal(&cond1);
}

uint32_t eden_capacity;
uint32_t current_heap_capacity;
uint32_t old_gen_size;
uint32_t old_gen_capacity;
uint32_t current_addr;
uint32_t gc_beats=0;
IArrayList * global_refs;
//static uint32_t dogc=0;
void initHeap(int edenCapacity){
	sfa=newIArrayList(1);
	static_fields=sfa->values;

	sra=newIArrayList(3);
	static_refs=(uint32_t*)sra->values;

	eden_capacity=edenCapacity;
	current_heap_capacity = eden_capacity*2;
	_heap=createHeap(current_heap_capacity);
	old_gen_capacity=current_heap_capacity - eden_capacity;
	current_addr = old_gen_capacity;
	old_gen_size=0;
	heap=_heap->heap;
	//heap[0]=0;
	threads=newLinkedList();
	pthread_t thread_id;
#ifdef VERBOSE
	printf("Before Thread\n");
#endif
	//pthread_create(&thread_id, NULL, myThreadFun, NULL);
}
static inline void reallocHeap(){
	void * newHeap=realloc(heap, current_heap_capacity);
	_heap->capacity=current_heap_capacity;
	if(newHeap==NULL){
		fprintf(stderr,"error while growing heap to: %d bytes\n",current_heap_capacity);
		exit(-1);
	}
	else{
#ifdef VERBOSE
		fprintf(stderr,"growing heap to: %d MB\n",current_heap_capacity/(1024*1024));
#endif
		heap=newHeap;
		_heap->heap=newHeap;
		old_gen_capacity=current_heap_capacity - eden_capacity;
		//if(dogc==0)
			current_addr=old_gen_capacity;
		//else
		//	current_addr=_heap->size;
	}
}

int addStaticRef(){
	IArrayList_add(sra, 0);
	static_refs=(uint32_t*)sra->values;
	return sra->size-1;
}

int addStaticField(){
	IArrayList_add(sfa, 0);
	static_fields=sfa->values;
	return sfa->size-1;
}

int addStaticField64(){
	IArrayList_add64(sfa, 0);
	static_fields=sfa->values;
	return sfa->size-2;
}

Heap * createHeap(uint32_t capacity){
	Heap * heap=malloc(sizeof(Heap));
	heap->heap=malloc(capacity);
	heap->capacity=capacity;
	heap->size=0;
	return heap;
}
void deleteHeap(Heap * heap){
	free(heap->heap);
	free(heap);
}

//serial gc
void gc(){
	++gc_beats;
#ifdef VERBOSED
	fprintf(stderr, "FULL GC\n");
	fprintf(stderr, "HEAPBEFORE:\n");
	dumpCurrentHeap();
#endif
	pthread_mutex_lock(&heapLock);
	//return;
	//dumpInstance(4040);
	Heap * newHeap=createHeap(current_heap_capacity);
	//IHashMap * living=newIHashMap(128);
	int32_t len=sra->size;
	for(int32_t i=0;i<len;++i){
		uint32_t ref=static_refs[i];
		if(ref!=0)
			static_refs[i]=processRef(newHeap, ref);
			//IHashMap_put(living, ref,NULL);
	}

	Node * n=threads->first;
	int tid=0;
	while(n!=NULL){
		Thread * t = n->value;
		FrameStack * stack=t->stack;
		int16_t jni_refs_count=stack->jni_refs_count;
	
		uint32_t ref;
		for(int16_t i=0;i<jni_refs_count;++i){
			ref=stack->jni_refs[i];
			if(ref!=0)
				stack->jni_refs[i]=processRef(newHeap, ref);
		}
		//currently throwing Exception ref:
		ref=stack->lv_stack[0];
		if(ref!=0){
			stack->lv_stack[0]=processRef(newHeap, ref);
		}
		int size=getStackSize(stack);
#ifdef VERBOSED
		fprintf(stderr,"THREAD %d: \n",tid++);
		fprintf(stderr, "stack size: %d\n",size);
#endif
		for(int i=0;i<size;++i){
			Frame * f=getFrame(stack, i);
			Method * m= f->method;
			StackMapEntry * sme = getStackMapEntry(m, f->pc);
			if(sme==NULL) continue;
#ifdef VERBOSED
			fprint(stderr, m->clazz->name);
			fprintf(stderr, ".");
			fprintln(stderr, m->signature);
			fprintf(stderr, "pc = %d sme->pc=%d\n",f->pc, sme->pc);
			
			fprintf(stderr, "processing lv\n");
#endif
			for(int j=0;j<m->max_lv;++j){
				uint8_t ri=sme->lv[j];
#ifdef VERBOSED
				fprintf(stderr, "lv[%d]=%d\n",j,ri);
#endif
				if(ri==1){
					ref=f->lv[j];
#ifdef VERBOSED
					fprintf(stderr, "method lv ref found: %d at:%d\n",ref,j);
#endif
					if(ref!=0){
						ref=processRef(newHeap, ref);
						f->lv[j]=ref;
					}
				}
      }         
			//LSSLSS
			int32_t stackstart=f->lv+m->max_lv-stack->lv_stack;
			//int32_t * opstack=f->lv+m->max_lv;
#ifdef VERBOSED
			fprintf(stderr, "_stackstart: %d\n",stackstart);
			fprintf(stderr, "_stackpos: %d\n",f->stackpos);
#endif
			for(int j=stackstart;j<f->stackpos;++j){
				uint8_t ri=sme->opstack[j-stackstart];
#ifdef VERBOSED
				fprintf(stderr, "opstack[%d]=%d\n",j,ri);
#endif
				if(ri==1){
					ref=stack->lv_stack[j];
#ifdef VERBOSED
					fprintf(stderr, "method opstack ref found: %d at:%d\n",ref,j);
#endif
					if(ref!=0){
						stack->lv_stack[j]=processRef(newHeap, ref);
					}
				}
			}
		}
		n=n->next;
	}
#ifdef VERBOSED
	fprintf(stderr, "HEAP AFTER:\n");
	dumpCurrentHeap();
#endif
	deleteHeap(_heap);
	_heap=newHeap;
	heap=newHeap->heap;
	//current_heap_capacity=newHeap->capacity;
	old_gen_size=newHeap->size;
	if(old_gen_capacity-old_gen_size<eden_capacity){
		current_heap_capacity=(current_heap_capacity*3)/2+eden_capacity;
		reallocHeap();
	}
	//if(dogc==0)
		current_addr=old_gen_capacity;
	//else
	//	current_addr=_heap->size;

	pthread_mutex_unlock(&heapLock);
}

uint32_t processRef(Heap * newHeap, uint32_t ref){
#ifdef VERBOSE
	fprintf(stderr, "living ref: %d\n",ref);
#endif
	ClassInstance * ci=heap+ref-sizeof(ClassInstance);
	uint8_t type=ci->type;
	switch(type){
		case OBJECT_ARRAYINSTANCE:
		case BOOLEAN_ARRAYINSTANCE:
		case CHAR_ARRAYINSTANCE:
		case FLOAT_ARRAYINSTANCE:
		case DOUBLE_ARRAYINSTANCE:
		case BYTE_ARRAYINSTANCE:
		case SHORT_ARRAYINSTANCE:
		case INT_ARRAYINSTANCE:
		case LONG_ARRAYINSTANCE:
		case CLASSINSTANCE:
			break;
		case MOVED_INSTANCE:
#ifdef VERBOSE
			fprintf(stderr, "MOVED_INSTANCE\n");
#endif
			return ci->newAddr;
		default: 
			//fprintf(stderr, "dogc was: %d\n", dogc);
			fprintf(stderr, "_UNKNOWN INSTANCE TYPE: ref: %d, type: %d\n", ref, ci->type);
			//dumpCurrentHeap();
			exit(-1);
			return ref;
	}
	
	ci->gc_beats=gc_beats;

	memcpy(newHeap->heap+newHeap->size, ci, ci->isize);
	ci->type=MOVED_INSTANCE;
	uint32_t newRef=newHeap->size+sizeof(ClassInstance);
	newHeap->size+=ci->isize;
	ci->newAddr=newRef;
#ifdef VERBOSE
	if(newRef!=ref){
			fprintf(stderr, "instance moved: oref: %d newOref: %d\n",ref,newRef);
	}
#endif
	switch(type){
		case OBJECT_ARRAYINSTANCE:
			{
			ObjectArrayInstance * oai=(ObjectArrayInstance*)ci;
			uint32_t * refs=newHeap->heap+newRef;
			for(int32_t i=0;i<oai->size;++i){
				uint32_t oref=refs[i];
				if(oref!=0){
					refs[i]=processRef(newHeap, oref);
				}
			}
			}
			break;
		case CLASSINSTANCE:
			{
			Class * clazz=ci->clazz;
			int8_t * refsMap=clazz->refsMap;
			uint16_t fieldsCount=clazz->iFieldsCount;
			uint32_t * fields=newHeap->heap+newRef;
#ifdef VERBOSE
			fprintf(stderr, "fieldsMap:");
			for(uint16_t i=0;i<fieldsCount;++i){
				fprintf(stderr, "%d, ",refsMap[i]);
			}
			fprintf(stderr, "\n");
#endif
			for(uint16_t i=0;i<fieldsCount;++i){
				if(refsMap[i]==1){
					uint32_t oref=fields[i];
					if(oref!=0){
						fields[i]=processRef(newHeap,oref);
#ifdef VERBOSE
						fprintf(stderr, "ref processed: %d, newRef: %d\n",oref,fields[i]);
#endif
					}
				}
			}
#ifdef VERBOSE
			fprintf(stderr, "\n");
#endif
			}
			break;
		case BOOLEAN_ARRAYINSTANCE:
		case CHAR_ARRAYINSTANCE:
		case FLOAT_ARRAYINSTANCE:
		case DOUBLE_ARRAYINSTANCE:
		case BYTE_ARRAYINSTANCE:
		case SHORT_ARRAYINSTANCE:
		case INT_ARRAYINSTANCE:
		case LONG_ARRAYINSTANCE:
			break;
		default: 
			//fprintf(stderr, "dogc was: %d\n", dogc);
			fprintf(stderr, "UNKNOWN INSTANCE TYPE: ref: %d, type: %d\n", ref, ci->type);
			exit(-1);
			return ref;
	}
	return newRef;
}

void minor_gc(){
	//gc();
	//return;
	++gc_beats;
#ifdef VERBOSED
	fprintf(stderr, "minor GC\n");
	fprintf(stderr, "old_gen_size: %d\n", old_gen_size);
	fprintf(stderr, "HEAPBEFORE:\n");
	dumpCurrentHeap();
#endif
	//return;
	//IHashMap * living=newIHashMap(128);
	int32_t len=sra->size;
	for(int32_t i=0;i<len;++i){
		uint32_t ref=static_refs[i];
		if(ref!=0)
			static_refs[i]=processRefM(ref);
			//IHashMap_put(living, ref,NULL);
	}

	Node * n=threads->first;
	int tid=0;
	while(n!=NULL){
		Thread * t = n->value;
		FrameStack * stack=t->stack;
		int16_t jni_refs_count=stack->jni_refs_count;
	
		uint32_t ref;
		for(int16_t i=0;i<jni_refs_count;++i){
			ref=stack->jni_refs[i];
			if(ref!=0)
				stack->jni_refs[i]=processRefM(ref);
		}

		//currently throwing Exception ref:
		ref=stack->lv_stack[0];
		if(ref!=0){
			stack->lv_stack[0]=processRefM(ref);
		}
		int size=getStackSize(stack);
#ifdef VERBOSED
		fprintf(stderr,"THREAD %d: \n",tid++);
		fprintf(stderr, "stack size: %d\n",size);
#endif
		for(int i=0;i<size;++i){
			Frame * f=getFrame(stack, i);
			Method * m= f->method;
			StackMapEntry * sme = getStackMapEntry(m, f->pc);
			if(sme==NULL) continue;
#ifdef VERBOSED
			fprint(stderr, m->clazz->name);
			fprintf(stderr, ".");
			fprintln(stderr, m->signature);
			fprintf(stderr, "pc = %d sme->pc=%d\n",f->pc, sme->pc);
			
			fprintf(stderr, "processing lv\n");
#endif
			for(int j=0;j<m->max_lv;++j){
				uint8_t ri=sme->lv[j];
#ifdef VERBOSED
				fprintf(stderr, "lv[%d]=%d\n",j,ri);
#endif
				if(ri==1){
					ref=f->lv[j];
#ifdef VERBOSED
					fprintf(stderr, "method lv ref found: %d at:%d\n",ref,j);
#endif
					if(ref!=0){
						ref=processRefM(ref);
						f->lv[j]=ref;
					}
				}
      }         
			//LSSLSS
			int32_t stackstart=f->lv+m->max_lv-stack->lv_stack;
			//int32_t * opstack=f->lv+m->max_lv;
#ifdef VERBOSED
			fprintf(stderr, "_stackstart: %d\n",stackstart);
			fprintf(stderr, "_stackpos: %d\n",f->stackpos-stackstart);
			fprintf(stderr, "max_stack: %d\n", m->max_stack);
#endif
			for(int j=stackstart;j<f->stackpos;++j){
				//fprintf(stderr, "index: %d\n",j-stackstart);
				uint8_t ri=sme->opstack[j-stackstart];
#ifdef VERBOSED
				fprintf(stderr, "opstack[%d]=%d\n",j-stackstart, ri);
#endif
				if(ri==1){
					ref=stack->lv_stack[j];
#ifdef VERBOSED
					fprintf(stderr, "method opstack ref found: %d at:%d\n",ref,j);
#endif
					if(ref!=0){
						stack->lv_stack[j]=processRefM(ref);
					}
				}
			}
		}
		n=n->next;
	}
#ifdef VERBOSED
	dumpCurrentHeap();
	fprintf(stderr, "old_gen_size: %d B\n", old_gen_size);
#endif
	if(old_gen_capacity-old_gen_size<eden_capacity)
		gc();
	current_addr=old_gen_capacity;

}

uint32_t processRefM(uint32_t ref){
#ifdef VERBOSED
	fprintf(stderr, "living ref: %d\n",ref);
#endif
	uint32_t ciRef=ref-sizeof(ClassInstance);
	ClassInstance * ci=heap+ciRef;
	uint8_t type=ci->type;
	switch(type){
		case OBJECT_ARRAYINSTANCE:
		case BOOLEAN_ARRAYINSTANCE:
		case CHAR_ARRAYINSTANCE:
		case FLOAT_ARRAYINSTANCE:
		case DOUBLE_ARRAYINSTANCE:
		case BYTE_ARRAYINSTANCE:
		case SHORT_ARRAYINSTANCE:
		case INT_ARRAYINSTANCE:
		case LONG_ARRAYINSTANCE:
		case CLASSINSTANCE:
			break;
		case MOVED_INSTANCE:
#ifdef VERBOSED
			fprintf(stderr, "MOVED_INSTANCE\n");
#endif
			return ci->newAddr;
		default: 
			//fprintf(stderr, "dogc was: %d\n", dogc);
			fprintf(stderr, "_UNKNOWN INSTANCE TYPE: ref: %d, type: %d\n", ref, ci->type);
			exit(-1);
			return ref;
	}
	
	if(ci->gc_beats==gc_beats){
		//fprintf(stderr, "already processed: gc_beats: %d\n",gc_beats);
		return ref;
	}
	ci->gc_beats=gc_beats;

	if(ciRef>=old_gen_capacity){
#ifdef VERBOSED
			fprintf(stderr, "moving instance to old gen from %d to %d\n", ciRef, old_gen_size);
			fprintf(stderr, "old_gen_size: %d old old_gen_capacity: %d isize: %d\n", old_gen_size, old_gen_capacity, ci->isize);
#endif
			memcpy(heap+old_gen_size, ci, ci->isize);
			ci->type=MOVED_INSTANCE;
			ref=old_gen_size+sizeof(ClassInstance);
			old_gen_size+=ci->isize;
			ci->newAddr=ref;
			//fprintf(stderr, "oldref: %ld, ref: %d, ci size: %d\n", ciRef+sizeof(ClassInstance), ref, ci->isize);
	}
	switch(type){
		case OBJECT_ARRAYINSTANCE:
			{
			ObjectArrayInstance * oai=(ObjectArrayInstance*)ci;
			uint32_t * refs=heap+ref;
			for(int32_t i=0;i<oai->size;++i){
				uint32_t oref=refs[i];
				if(oref!=0){
					refs[i]=processRefM(oref);
				}
			}
			}
			break;
		case CLASSINSTANCE:
			{
			Class * clazz=ci->clazz;
			int8_t * refsMap=clazz->refsMap;
			uint16_t fieldsCount=clazz->iFieldsCount;
			uint32_t * fields=heap+ref;
#ifdef VERBOSED
			fprintf(stderr, "fieldsMap:");
			for(uint16_t i=0;i<fieldsCount;++i){
				fprintf(stderr, "%d, ",refsMap[i]);
			}
			fprintf(stderr, "\n");
#endif
			for(uint16_t i=0;i<fieldsCount;++i){
				if(refsMap[i]==1){
					uint32_t oref=fields[i];
					if(oref!=0){
						fields[i]=processRefM(oref);
#ifdef VERBOSED
						fprintf(stderr, "ref processed: %d, newRef: %d\n",oref,fields[i]);
#endif
					}
				}
			}
#ifdef VERBOSED
			fprintf(stderr, "\n");
#endif
			}
			break;
		case BOOLEAN_ARRAYINSTANCE:
		case CHAR_ARRAYINSTANCE:
		case FLOAT_ARRAYINSTANCE:
		case DOUBLE_ARRAYINSTANCE:
		case BYTE_ARRAYINSTANCE:
		case SHORT_ARRAYINSTANCE:
		case INT_ARRAYINSTANCE:
		case LONG_ARRAYINSTANCE:
			break;
		default: 
			//fprintf(stderr, "dogc was: %d\n", dogc);
			fprintf(stderr, "UNKNOWN INSTANCE TYPE: ref: %d, type: %d\n", ref, ci->type);
			exit(-1);
			return ref;
	}
	return ref;
}


static inline uint32_t _malloc(uint32_t size){
	uint32_t addr;
	if(current_addr+size>=current_heap_capacity){
		//if(dogc==0){
			minor_gc();
			//gc();
			if(current_heap_capacity/4>(old_gen_size+eden_capacity)){
				current_heap_capacity=old_gen_size*4+eden_capacity+size;
				reallocHeap();
			}
			if(size>eden_capacity){
					gc();
					if(current_addr+size>=current_heap_capacity){
							current_heap_capacity=(current_heap_capacity*3)/2+size+eden_capacity;
							reallocHeap();
					}
					if(old_gen_capacity-old_gen_size<eden_capacity)
							fprintf(stderr, "old_gen_capacity too small!\n");
					addr=old_gen_size;
					old_gen_size+=size;
					return addr;
			}

		/*}
		else{
			fprintf(stderr, "capacity before: %d\n",current_heap_capacity);
			current_heap_capacity=(current_heap_capacity*3)/2+size+eden_capacity;
			fprintf(stderr, "capacity after: %d\n",current_heap_capacity);
			reallocHeap();
		}*/
	}
	
	addr=current_addr;
	current_addr+=size;
	return addr;
}

uint32_t createClassInstance(Class * class){
	uint32_t isize=class->iSize+sizeof(ClassInstance);
	uint32_t addr = _malloc(isize);

	ClassInstance * ci=heap+addr;
	ci->isize=isize;
	ci->clazz=class;
	ci->gc_beats=gc_beats;
	ci->type=CLASSINSTANCE;
	addr+=sizeof(ClassInstance);
	memcpy(heap+addr,class->instanceFieldsTemplate, 4*class->iFieldsCount);
	//current_addr+=isize;
	//dumpHeap();
	//heap + addr points to the first field of a ClassInstance
	//the ClassInstance structure itself can be obtained with:
	//ClassInstance * ci = heap + addr - sizeof(ClassInstance);
	return addr;
}


uint32_t createArrayInstance(int8_t type, int32_t size){
	uint32_t isize;
	uint32_t addr;
	switch(type){
		case T_BOOLEAN:
		case T_BYTE:
		case 'Z':
		case 'B':
			isize=size+sizeof(ByteArrayInstance);
			addr=_malloc(isize);
			{
			ByteArrayInstance * ai=heap+addr;
			ai->isize=isize;
			addr+=sizeof(ByteArrayInstance);
			memset(heap + addr, 0, size);
			ai->gc_beats=gc_beats;
			ai->type=BYTE_ARRAYINSTANCE;
			ai->size=size;
			}
			break;
		case T_SHORT:
		case 'S':
			isize=size*2+sizeof(ShortArrayInstance);
			addr=_malloc(isize);
			{
			ShortArrayInstance * ai=heap+addr;
			ai->isize=isize;
			addr+=sizeof(ShortArrayInstance);
			memset(heap + addr, 0, size*2);
			ai->gc_beats=gc_beats;
			ai->type=SHORT_ARRAYINSTANCE;
			ai->size=size;
			}
			break;
		case T_CHAR:
		case 'C':
			isize=size*2+sizeof(CharArrayInstance);
			addr=_malloc(isize);
			{
			CharArrayInstance * ai=heap+addr;
			ai->isize=isize;
			addr+=sizeof(CharArrayInstance);
			memset(heap + addr, 0, size*2);
			ai->gc_beats=gc_beats;
			ai->type=CHAR_ARRAYINSTANCE;
			ai->size=size;
			}
			break;
		case T_INT:
		case 'I':
			isize=size*4+sizeof(ShortArrayInstance);
			addr=_malloc(isize);
			{
			IntArrayInstance * ai=heap+addr;
			ai->isize=isize;
			addr+=sizeof(ShortArrayInstance);
			memset(heap + addr, 0, size*4);
			ai->gc_beats=gc_beats;
			ai->type=INT_ARRAYINSTANCE;
			ai->size=size;
			}
			break;
		case T_OBJECT:
		case 'L':
			isize=size*4+sizeof(ObjectArrayInstance);
			addr=_malloc(isize);
			{
			ObjectArrayInstance * ai=heap+addr;
			ai->isize=isize;
			addr+=sizeof(ObjectArrayInstance);
			memset(heap + addr, 0, size*4);
			ai->gc_beats=gc_beats;
			ai->type=OBJECT_ARRAYINSTANCE;
			ai->size=size;
			}
			break;
		case T_FLOAT:
		case 'F':
			isize=size*4+sizeof(FloatArrayInstance);
			addr=_malloc(isize);
			{
			FloatArrayInstance * ai=heap+addr;
			ai->isize=isize;
			addr+=sizeof(FloatArrayInstance);
			memset(heap + addr, 0, size*4);
			ai->gc_beats=gc_beats;
			ai->type=FLOAT_ARRAYINSTANCE;
			ai->size=size;
			}
			break;
		case T_DOUBLE:
		case 'D':
			isize=size*8+sizeof(DoubleArrayInstance);
			addr=_malloc(isize);
			{
			DoubleArrayInstance * ai=heap+addr;
			ai->isize=isize;
			addr+=sizeof(DoubleArrayInstance);
			memset(heap + addr, 0, size*8);
			ai->gc_beats=gc_beats;
			ai->type=DOUBLE_ARRAYINSTANCE;
			ai->size=size;
			}
			break;
		case T_LONG:
		case 'J':
			isize=size*8+sizeof(LongArrayInstance);
			addr=_malloc(isize);
			{
			LongArrayInstance * ai=heap+addr;
			ai->isize=isize;
			addr+=sizeof(LongArrayInstance);
			memset(heap + addr, 0, size*8);
			ai->gc_beats=gc_beats;
			ai->type=LONG_ARRAYINSTANCE;
			ai->size=size;
			}
			break;
		default :
				fprintf(stderr,"unknown array type\n");
				exit(-1);
			return 0;
	}
	return addr;
}

uint32_t multianewarray(uint8_t dimensions, uint8_t dimension, int32_t * sizes, char type) {
	int32_t size=sizes[dimension];
	if(dimension==0)
	{
		return createArrayInstance(type, size);
	}
	uint32_t arroref=createArrayInstance(T_OBJECT, size);
	//ObjectArrayInstance * oarr=heap+arroref-sizeof(ArrayInstance); 
	for(int i=0;i<size;++i){
		uint32_t oref= multianewarray(dimensions, dimension - 1, sizes, type);
		//important, because realloc
		uint32_t * arr=heap+arroref;
		arr[i]=oref;
	}
	return arroref;
}

void putField(uint32_t oref, String * name, int32_t value){
	ClassInstance * ci = heap + oref -sizeof(ClassInstance);
	//fprintf(stderr, "oref: %d\n",oref);
	//println(ci->clazz->name);
	Field * f = HashMap_get(ci->clazz->ft,name); 
	//fprintln(stderr, name);
	//fprintf(stderr, "index: %d value: %d\n",f->index, value);
	*(int32_t*)(heap+oref+f->index*4)=value;
}

int32_t getField(uint32_t oref, String * name){
	ClassInstance * ci = heap + oref -sizeof(ClassInstance);
	Field * f = HashMap_get(ci->clazz->ft,name); 
	return *(int32_t*)(heap+oref+f->index*4);
}

/*
void putFieldRef(ClassInstance * ci, String * name, int32_t value){
	Field * f = HashMap_get(ci->clazz->ft,name); 
	ci->fields[f->index]=value;
}
int32_t getFieldRef(ClassInstance * ci, String * name){
	Field * f = HashMap_get(ci->clazz->ft,name); 
	return ci->fields[f->index];
}
*/


void dumpInstance(uint32_t oref){
	ClassInstance * ci = heap+oref-sizeof(ClassInstance);
	char * typestr;
	switch(ci->type){
		case OBJECT_ARRAYINSTANCE:
			typestr="OBJECT_ARRAYINSTANCE";
			break;
		case BOOLEAN_ARRAYINSTANCE:
			typestr="BOOLEAN_ARRAYINSTANCE";
			break;
		case CHAR_ARRAYINSTANCE:
			typestr="CHAR_ARRAYINSTANCE";
			break;
		case FLOAT_ARRAYINSTANCE:
			typestr="FLOAT_ARRAYINSTANCE";
			break;
		case DOUBLE_ARRAYINSTANCE:
			typestr="DOUBLE_ARRAYINSTANCE";
			break;
		case BYTE_ARRAYINSTANCE:
			typestr="BYTE_ARRAYINSTANCE";
			break;
		case SHORT_ARRAYINSTANCE:
			typestr="SHORT_ARRAYINSTANCE";
			break;
		case INT_ARRAYINSTANCE:
			typestr="INT_ARRAYINSTANCE";
			break;
		case LONG_ARRAYINSTANCE:
			typestr="LONG_ARRAYINSTANCE";
			break;
		case CLASSINSTANCE:
			typestr="CLASSINSTANCE";
			break;
		case MOVED_INSTANCE:
			typestr="CLASSINSTANCE";
			break;
		default: 
			oref=0;
			typestr="UNKNOWN";
			break;
	}
	fprintf(stderr, "INSTANCEDUMP: oref: %d type: %s\n",oref,typestr);
	fprintf(stderr, "class: ");
	fprintln(stderr, ci->clazz->name);
	/*
	for(int i=0;i<ci->clazz->iRefsCount;++i){
		fprintf(stderr, "%d = %d\n",i,ci->refs[i]);
	}*/
	fprintf(stderr,"fields:\n");

	int32_t * fields = heap+oref;

	for(int i=0;i<ci->clazz->iFieldsCount;++i){
		fprintf(stderr, "%d = %d\n",i,fields[i]);
	}
} 

void dumpCurrentHeap(){
	fprintf(stderr, "OLD:\n");
	dumpHeap(0, _heap);
	//if(dogc==0)
	fprintf(stderr, "EDEN:\n");
	dumpHeap(current_heap_capacity-eden_capacity, _heap);
}

void dumpHeap(uint32_t offset, Heap * _heap){
	int32_t object=0;
	//fprintf(stderr,"HEAPDUMP:\n");
	void * heap=_heap->heap;

	//fprintf(stderr," watch: %p value: %d\n", heap+offset, *(int32_t*)(heap+offset));
	while(offset<=_heap->capacity){
		ClassInstance * ci = heap+offset;
		int32_t isize=ci->isize;
		if(isize==0){
			fprintf(stderr, "isize is zero\n");
			return;
		}
		if(isize<0){
			fprintf(stderr, "isize is negative: %d\n",isize);
			return;
		}
		if(offset+isize>=_heap->capacity){
			fprintf(stderr, "heap overflow: %d\n",isize);
			return;
		}
		int8_t type=ci->type;
		char * typestr;
		String * classname=NULL;
		uint32_t oref=offset+sizeof(ArrayInstance);
		fprintf(stderr, "OBJECT %d oref: %d at %d  size: %d",object, oref, offset, isize);
		switch(type){
			case OBJECT_ARRAYINSTANCE:
				typestr="OBJECT_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case BOOLEAN_ARRAYINSTANCE:
				typestr="BOOLEAN_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case CHAR_ARRAYINSTANCE:
				typestr="CHAR_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case FLOAT_ARRAYINSTANCE:
				typestr="FLOAT_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case DOUBLE_ARRAYINSTANCE:
				typestr="DOUBLE_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case BYTE_ARRAYINSTANCE:
				typestr="BYTE_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case SHORT_ARRAYINSTANCE:
				typestr="SHORT_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case INT_ARRAYINSTANCE:
				typestr="INT_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case LONG_ARRAYINSTANCE:
				typestr="LONG_ARRAYINSTANCE";
				oref=offset+sizeof(ArrayInstance);
				break;
			case CLASSINSTANCE:
				typestr="CLASSINSTANCE";
				oref=offset+sizeof(ClassInstance);
				fprint(stderr, ci->clazz->name);
				fprintf(stderr, " :");
				break;
			case MOVED_INSTANCE:
				typestr="MOVED_INSTANCE";
				break;
			default: 
				oref=0;
				typestr="UNKNOWN";
				break;
		}
		fprintf(stderr, " type: %s", typestr);
		if(classname!=NULL){
			fprintf(stderr, " class: ");
			fprint(stderr, classname);
			Class * clazz=ci->clazz;
			int8_t * refsMap=clazz->refsMap;
			uint16_t fieldsCount=clazz->iFieldsCount;
			int32_t * fields=heap+oref;
			fprintf(stderr, " refs: ");
			for(uint16_t i=0;i<fieldsCount;++i){
				if(refsMap[i]==1){
					uint32_t oref=fields[i];
					fprintf(stderr, "%d, ",oref);
				}
			}
		}
		fprintf(stderr, "\n");

		offset+=isize;
		++object;
	}

}

/*
void disableGC(){
	if(dogc==0){
		minor_gc();
		current_addr=old_gen_size;
		//fprintf(stderr, "GC disabled: current_addr is now: %d\n", current_addr);
	}
	++dogc;
}

void enableGC(){
	--dogc;
	if(dogc==0){
		old_gen_size=current_addr;
		current_addr=current_heap_capacity - eden_capacity;
		//fprintf(stderr, "GC enabled: current_addr is now: %d\n", current_addr);
	}
}*/

void dumpThreads(){
	Node * n=threads->first;
	for(int i=0;n!=NULL;++i){
		fprintf(stderr,"Thread %d: \n",i);
		n=n->next;
	}
}
