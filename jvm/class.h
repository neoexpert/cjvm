#ifndef CLASS_H
#define CLASS_H
static const int HEAD=0xcafebabe;

typedef struct Class{
	uint16_t accessFlags;
	String * name;
	String * fileName;
	struct Class * superClass;
	uint16_t cpoolSize;
	Constant ** cpool;
	uint8_t ** interfaces;
	uint16_t fieldsCount;
	Field ** fields;
	uint16_t methodsCount;
	Method ** methods;
	//static_methodTable
	HashMap * smt;
	//virtual_methodTable
	HashMap * vmt;
	uint16_t attrsCount;
	Attribute ** attrs;
	//fieldsTable
	HashMap * ft;
	bool isInitialized;
	//instanceSize
	uint16_t iSize;
	//instance Fields count 
	uint16_t iFieldsCount;
	uint16_t iRefsCount;
	int8_t * refsMap;
	int32_t * instanceFieldsTemplate;
	//reference to java.lang.Class Instance
	uint32_t orefIndex;
	String * code_source;
}Class;
Class * readClass(Buffer * buf);
void deleteClass(Class * clazz);
Method * getMethod(Class * clazz, String * signature);
bool isInstance(Class * this, Class * other);
void Class_LDC(Constant ** cpool, uint16_t index);
Field * resolveField(Class * clazz, String * name);
#endif
