#ifndef BYTECODEEXECUTOR_H
#define BYTECODEEXECUTOR_H
int step(Method * m, Frame * frame, int stackoffset, FrameStack * frameStack);
void printFrame(Frame * f, int32_t * lv_stack, int stackpos);
int invokeNative(int* lv);
#endif
