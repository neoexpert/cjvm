#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../util/linkedlist.h"
#include "../io/buffer.h"
#include "memory.h"

uint32_t total_mem;
uint32_t mem_offset=0;
uint32_t checker_offset=0;
int gcbeats=0;
int header_size=8;
void initMem(){
	total_mem=4*1024;
	mem=malloc(total_mem);
	mem[mem_offset]=0;
}

int32_t checkHoles(int size){
	int addr=checker_offset;
	int holesize=0;
	int32_t nextChunk;
	while(true){
		nextChunk=mem[checker_offset];
		int32_t _gcbeats=mem[checker_offset+1];
		if(_gcbeats<gcbeats){
			//hole found
			if(nextChunk==0){
				checker_offset=0;
				break;
			}
			holesize+=(nextChunk-checker_offset);
			mem[addr]=nextChunk;
			checker_offset=nextChunk;
			continue;
		}
		else
			//
			break;
	}
	if(holesize>=size){
		mem[addr]=addr+size;
		mem[addr+size]=nextChunk;

		return addr;
	}
	return -1;
}

int32_t _malloc(int size){
	int32_t addr=checkHoles(size);
	if(addr!=-1){
		return addr;
	}
	addr=mem_offset;
	size+=header_size;
	mem_offset+=size;
	mem[addr]=mem_offset;
	mem[addr+1]=gcbeats;
	mem[mem_offset]=0;
	if(mem_offset>total_mem){
		total_mem=(mem_offset*3)/2;
		printf("realloc mem to %d\n",total_mem);
		mem=realloc(mem,total_mem);
	}
	return addr+header_size;
}

void checkMem(){
	int32_t off=mem[0];
	while(off){
		printf("off: %d\n",off);
		printf("gcbeats: %d\n",mem[off+1]);
		off=mem[off];
	}
}
