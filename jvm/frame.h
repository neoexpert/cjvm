#ifndef FRAME_H
#define FRAME_H
typedef struct {
	int pc;
	uint8_t* code;
	int32_t * lv;
	int stackpos;
	Method * method;
	Constant ** cpool;
}Frame;
typedef struct {
	void * stack;
	int32_t * lv_stack;
	int lv_stack_capacity;
	int pos;
	int frameCapacity;
	uint32_t capacity;
	uint32_t * jni_refs;
	int16_t jni_refs_count;
	int16_t max_jni_refs;
}FrameStack;
Frame * newFrame(FrameStack * fstack, Method * m, int32_t * lv, int stackoffset);
Frame * getFrame(FrameStack * fstack, uint32_t index);
int getStackSize(FrameStack * fstack);
FrameStack * newFrameStack(int size, int lv_stack_size, int16_t max_jni_refs);
uint32_t * createJNIRef(FrameStack * fstack, uint32_t ref);
void freeJNIRef(FrameStack * fstack, uint32_t * ref);
void growFrameStack(FrameStack * fstack, int newCapacity);
void deleteFrameStack(FrameStack * fstack);
//void pushFrame(FrameStack * fstack, Frame * f);
Frame * popFrameAndPeek(FrameStack * fstack);
Frame * throwException(Class * clazz, FrameStack * fstack, Frame * cF, int pc, int stackPos);
Frame * throwNewException(String * name, FrameStack * fstack, Frame * cF, int pc, String * message);
#endif
