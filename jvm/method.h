#ifndef METHOD_H
#define METHOD_H
/*
#define UNKNOWN -1
#define __V 0
#define __I 1
#define __J 2
#define _I_V 3
#define _I_I 4
#define _I_J 5
#define _II_V 6
#define _II_I 7
#define _IJ_V 9
#define _III_I 10
#define _IJ_I 13
#define _IIII_I 17
#define _JI_I 19
#define _JI_J 20
#define _IIIII_V 33
*/
typedef struct {
	String * classname;
	String * name;
	String * signature;
	MethodType * mt;
}MethodHead;

typedef struct {
	int32_t pc;
	int8_t * lv;
	int8_t * opstack;
	
}StackMapEntry;

typedef struct {
	uint16_t accessFlags;
	bool isNative;
	bool isVirtual;
	String * name;
	String * type;
	String * signature;
	uint16_t attrsCount;
	Attribute ** attrs;
	CodeAttribute * codeAttr;
	Exception ** etable;
	int etableLen;
	uint16_t max_stack;
	uint16_t max_lv;
	uint16_t argsLength;
	uint8_t * code;
	struct Class * clazz;
	Constant ** cpool;
	void * nativeHandler;
	int32_t nativeMethodType;
	//used for calling native function pointer
	//ffi_cif * cif;
	//uint16_t refs;
	//uint8_t * ref_indexes;
	int32_t stackMapTableSize;
	StackMapEntry ** stackMapTable;
	MethodType * mt;
	uint16_t typeIndex;
	uint16_t lineNumbersTableLength;
	uint16_t * lineNumbersTable;
}Method;
Method * readMethod(Buffer * buf, Constant ** cpool);
void readCodeAttribute(Method * m, Attribute * a, Constant ** cpool);
void readLineNumbers(Method * m, Attribute * a);
int16_t getLineNumber(Method * m, int pc);
void deleteMethod(Method * m, Constant ** cpool);
void initMethods();
StackMapEntry * getStackMapEntry(Method * m, int32_t pc);
int8_t calcNativeMethodType(int8_t returnType, int8_t * args, int8_t offset, int8_t len);
#endif
