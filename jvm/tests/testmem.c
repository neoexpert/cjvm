#include <stdio.h>
#include <stdlib.h>
#include "../../util/mymalloc.h"
#include <inttypes.h>
#include <stdbool.h>
#include <signal.h>
#include <unistd.h>
#include "../../util/linkedlist.h"
#include "../memory.h"

int main(int argc, char ** argv){
	initMem();
	for(int i=0;i<1024;++i){
		int32_t addr=_malloc(4);
		mem[addr]=i;
	}
	checkMem();
	return 0;
}

