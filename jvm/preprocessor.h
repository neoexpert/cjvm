#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H
typedef struct {
	Method * m;
	uint8_t * code;
	Constant ** cpool;
	int32_t code_length;
	int8_t * processed;
	ArrayList * stackMapTable;
}CodeProcessorFrame;
CodeProcessorFrame * createCodeProcessorFrame(Method * m, Constant ** cpool);
void deleteCodeProcessorFrame(CodeProcessorFrame * cpf);
int processcode(CodeProcessorFrame * cpf, int32_t pc, int32_t stackpos, int8_t * lv, int8_t * opstack);
void addStackEntry(Method * m, ArrayList * stackMapTable, int32_t pc, int8_t * lv, int8_t * opstack);
#endif
