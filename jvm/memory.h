#ifndef MEMORY_H
#define MEMORY_H
int32_t * mem;
typedef struct {
	int32_t next;
	int32_t gcbeats;
}ChunkHeader;

void initMem();
void checkMem();
int32_t _malloc(int size);
LinkedList * holes;
#endif
