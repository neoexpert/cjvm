#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../lang/strings.h"
#include "../io/buffer.h"
#include "constant.h"
#include "attribute.h"
//#include "preprocessor.h"

void initAttributes(){
	codeAttrName=intern(_s("Code"));
	lineNumberTableAttrName=intern(_s("LineNumberTable"));
}

Attribute * readAttribute(Buffer * buf,Constant ** cpool){
	Attribute * attr=malloc(sizeof(Attribute));
	uint16_t nameIndex=getU16(buf);
	attr->name=cpool[nameIndex]->str;

	int length=getInt(buf);
	attr->length=length;
	attr->info=buf->buf+buf->pos;
	buf->pos+=length;
	return attr;
}



void deleteAttribute(Attribute * attr){
	free(attr);
}

void deleteCodeAttribute(CodeAttribute * ca){
	int exception_table_length=ca->exception_table_length;
	for(uint16_t i=0;i<exception_table_length;i++){
		free(ca->exceptionTable[i]);
	}
	int attrsCount=ca->attrsCount;
	for(int i=0;i<attrsCount;++i)
		deleteAttribute(ca->attrs[i]);
	free(ca->attrs);
	free(ca->exceptionTable);
	free(ca->code);
	free(ca);
}
