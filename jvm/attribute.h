#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H
typedef struct {
	String * name;
	int length;
	int8_t * info;
}Attribute;
typedef struct {
	uint16_t start_pc;
	uint16_t end_pc;
	uint16_t handler_pc;
	uint16_t catch_type;
}Exception;
typedef struct {
	uint16_t max_stack;
	uint16_t max_locals;
	int code_length;
	uint8_t * code;
	uint16_t exception_table_length;
	Exception ** exceptionTable;
	uint16_t attrsCount;
	Attribute ** attrs;
}CodeAttribute;
Attribute * readAttribute(Buffer * buf, Constant ** cpool);
void deleteAttribute(Attribute * buf);
void deleteCodeAttribute(CodeAttribute * attr);
String * codeAttrName;
String * lineNumberTableAttrName;
void initAttributes();
#endif
