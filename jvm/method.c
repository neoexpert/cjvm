#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>
//#include <ffi.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../util/arraylist.h"
#include "../util/i8arraylist.h"
#include "../lang/strings.h"
#include "../io/buffer.h"
#include "constant.h"
#include "attribute.h"
#include "method.h"
#include "ACCESS_MODIFIERS.h"
#include "preprocessor.h"

/*
ffi_cif * calcFFIMethodType(int8_t returnType, int8_t * args, int16_t len){
	int actual_length=0;
	ffi_type ** targs=malloc(len*sizeof(ffi_type));
	for(int i=0;i<len;++i){
		switch(args[i]){
			default:
				targs[actual_length++]=&ffi_type_sint32;
				continue;
			case 'L':
			case '[':
				targs[actual_length++]=&ffi_type_sint32;
				continue;
			case 'J':
			case 'D':
				targs[actual_length++]=&ffi_type_sint64;
				++i;
				continue;

		}
	}
  ffi_cif * cif=malloc(sizeof(ffi_cif));
	switch(returnType){
		default:
			if (ffi_prep_cif(cif, FFI_DEFAULT_ABI, actual_length, 
						&ffi_type_sint32, targs) != FFI_OK)
				return NULL;
		case 'J':
		case 'D':
			if (ffi_prep_cif(cif, FFI_DEFAULT_ABI, actual_length, 
						&ffi_type_sint64, targs) != FFI_OK)
				return NULL;
	}
	return cif;
}
*/

//(JI)V : 12
//(J)V : 12
int16_t _calcNativeMethodType(bool isStatic, int8_t returnType, int8_t * args, int16_t len){
	int actual_length;
	int16_t type;
	int e;
	if(isStatic){
		type=0;
		e=0;
	}
	else{
		type=2;
		e=1;
	}
	for(int i=0;i<len;++i,++e){
		switch(args[i]){
			default:
				type+=1*pow(4,e);
				continue;
			case 'L':
			case '[':
				type+=2*pow(4,e);
				continue;
			case 'J':
			case 'D':
				type+=3*pow(4,e);
				++i;
				continue;

		}
	}
	type*=4;
	switch(returnType){
		case 'V':
			return type;
		default:
			return type+1;
		case 'L':
		case '[':
			return type+2;
		case 'J':
		case 'D':
			return type+3;
	}
}

Method * readMethod(Buffer * buf, Constant ** cpool){
	Method * m=malloc(sizeof(Method));
	m->code=NULL;
	m->cpool=cpool;
	m->accessFlags=getU16(buf);
	m->nativeHandler=NULL;
	bool isNative=ISNATIVE(m->accessFlags);
	m->isNative=isNative;
	//virtual methods can't be static or private
	bool isStatic=(ISSTATIC(m->accessFlags));
	bool isVirtual=!isStatic&&(!ISPRIVATE(m->accessFlags));
	uint16_t nameIndex=getU16(buf);
	m->name=cpool[nameIndex]->str;
	isVirtual=isVirtual&&m->name->str[0]!='<';
	m->isVirtual=isVirtual;
	uint16_t typeIndex=getU16(buf);
	m->type=cpool[typeIndex]->str;
	MethodType * mt=cpool[typeIndex]->data;
	if(mt==NULL){
		mt=parseMethodType(cpool[typeIndex]->str);
		cpool[typeIndex]->data=mt;
	}
	m->typeIndex=typeIndex;
	m->mt=mt;
	int argsLength=mt->argsLength;
	if(!isStatic)
		++argsLength;
	m->argsLength=argsLength;
	uint16_t attrsCount=getU16(buf);
	m->attrsCount=attrsCount;
	m->signature=intern(concat(m->name,m->type));

	if(isNative){
		m->max_lv=argsLength;

		//m->nativeMethodType=calcNativeMethodType(mt->returnType, mt->args, 0, mt->argsLength);
		m->nativeMethodType=_calcNativeMethodType(isStatic, mt->returnType, mt->args, mt->argsLength);
		//m->cif=calcFFIMethodType(mt->returnType, mt->args, mt->argsLength);
		//if(m->cif==NULL)
		//	fprintf(stderr, "cif is NULL\n");
		//if(!isStatic) m->nativeMethodType+=3;
#ifdef VERBOSE
		uint16_t ntype=_calcNativeMethodType(isStatic, mt->returnType, mt->args,  mt->argsLength);
		fprintf(stderr, "nmt: %d for:", ntype);
		fprintln(stderr, m->signature);
#endif
		/*
		switch(mt->returnWidth){
			case 0:
				switch(argsLength){
					case 0:
						m->nativeMethodType=MT_V0;
						break;
					case 1:
						m->nativeMethodType=MT_V1;
						break;
					case 2:
						m->nativeMethodType=MT_V2;
						break;
					case 3:
						m->nativeMethodType=MT_V3;
						break;
					case 4:
						m->nativeMethodType=MT_V4;
						break;
					case 5:
						m->nativeMethodType=MT_V5;
						break;
					default:
						fprintf(stderr, "error: max 5 arguments for native methods are supported\n");
						fprintf(stderr, "signature was: ");
						fprintln(stderr, m->signature);
						exit(-1);

				}
				break;
			case 1:
				switch(argsLength){
					case 0:
						m->nativeMethodType=MT_0;
						break;
					case 1:
						m->nativeMethodType=MT_1;
						break;
					case 2:
						m->nativeMethodType=MT_2;
						break;
					case 3:
						m->nativeMethodType=MT_3;
						break;
					case 4:
						m->nativeMethodType=MT_4;
						break;
					case 5:
						m->nativeMethodType=MT_5;
						break;
					default:
						fprintf(stderr, "error: max 5 arguments for native methods are supported\n");
						fprintf(stderr, "signature was: ");
						fprintln(stderr, m->signature);
						exit(-1);

				}
				break;
			case 2:
				switch(argsLength){
					case 0:
						m->nativeMethodType=MT_W0;
						break;
					case 1:
						m->nativeMethodType=MT_W1;
						break;
					case 2:
						m->nativeMethodType=MT_W2;
						break;
					case 3:
						m->nativeMethodType=MT_W3;
						break;
					case 4:
						m->nativeMethodType=MT_W4;
						break;
					case 5:
						m->nativeMethodType=MT_W4;
						break;
					default:
						fprintf(stderr, "error: max 5 arguments for native methods are supported\n");
						fprintf(stderr, "signature was: ");
						fprintln(stderr, m->signature);
						exit(-1);

				}
				break;
		}
	*/
	}

	Attribute ** attrs
	= malloc(attrsCount * sizeof(Attribute*));
	m->attrs=attrs;

	m->codeAttr=NULL;

	m->stackMapTableSize = 0;
	m->stackMapTable= NULL;
	for(uint16_t i=0;i<attrsCount;++i){
		Attribute * a=readAttribute(buf,cpool);
		attrs[i]=a;
		if(a->name==codeAttrName){
			readCodeAttribute(m, a, cpool);

			m->max_lv=m->codeAttr->max_locals;
			m->max_stack=m->codeAttr->max_stack;
			//ArrayList * stackMapTable = newArrayList(16);
			m->code=m->codeAttr->code;
			m->etable=m->codeAttr->exceptionTable;
			m->etableLen=m->codeAttr->exception_table_length;

			CodeProcessorFrame * cpf = createCodeProcessorFrame(m, cpool);
			int result=processcode(cpf, 0, 0, NULL, NULL);

			if(result!=0){
				fprintf(stderr, "processcode failed\n");
				exit(-1);
				return NULL;
			}
			//ArrayList_fixSize(cpf->stackMapTable);
			//m->stackMapTable = (StackMapEntry**)cpf->stackMapTable->values;
			//m->stackMapTableSize = cpf->stackMapTable->size;

			Exception ** etable=m->etable;
			int len=m->etableLen;
			if(len>0){
				m->stackMapTable=malloc(cpf->stackMapTable->size*sizeof(void*));	
				m->stackMapTableSize = cpf->stackMapTable->size;
				memcpy(m->stackMapTable, cpf->stackMapTable->values, cpf->stackMapTable->size*sizeof(void*));
			}
			for(int i=0;i<len;++i){
				Exception * e=etable[i];
				StackMapEntry * sme = getStackMapEntry(m, e->handler_pc);
				int result=processcode(cpf, e->handler_pc, 1, sme->lv, NULL);
				if(result!=0){
					fprintf(stderr, "processcode failed\n");
					exit(-1);
					return NULL;
				}
			}
			if(len>0){
				free(m->stackMapTable);
			}
			ArrayList_fixSize(cpf->stackMapTable);
			m->stackMapTable = (StackMapEntry**)cpf->stackMapTable->values;
			m->stackMapTableSize = cpf->stackMapTable->size;


			deleteCodeProcessorFrame(cpf);
			//fprintln(stderr, m->signature);
		}
		else{
#ifdef VERBOSE
			fprintf(stderr, "method Attribute found: \n");
			fprintln(stderr, a->name);
#endif
		}
	}
	if(m->code==NULL){
#ifdef VERBOSE
		fprintf(stderr, "no codeattr for\n");
		fprintln(stderr, m->signature);
#endif
	}

	return m;
}

void readCodeAttribute(Method * m, Attribute * a, Constant ** cpool){
	CodeAttribute * ca=malloc(sizeof(CodeAttribute));
	Buffer * buf=Buffer_wrap(a->info);
	ca->max_stack=getU16(buf);
	ca->max_locals=getU16(buf);
	ca->code_length=getInt(buf);
	uint8_t * code=malloc(ca->code_length);
	getArray(buf, (int8_t*)code, ca->code_length);
	ca->code=code;
		
	//buf->pos+=ca->code_length;
	uint16_t exception_table_length=getU16(buf);
	ca->exception_table_length=exception_table_length;
	ca->exceptionTable=malloc(sizeof(Exception)*exception_table_length);
	for(uint16_t i=0;i<exception_table_length;i++){
		Exception * e =malloc(sizeof(Exception));	
		e->start_pc=getU16(buf);
		e->end_pc=getU16(buf);
		e->handler_pc=getU16(buf);
		e->catch_type=getU16(buf);
		ca->exceptionTable[i]=e;
	}
	uint16_t attrsCount=getU16(buf);
	ca->attrsCount=attrsCount;

	Attribute ** attrs
	= malloc(attrsCount * sizeof(Attribute*));
	ca->attrs=attrs;

	for(uint16_t i=0;i<attrsCount;++i){
		Attribute * a=readAttribute(buf,cpool);
		if(a->name==lineNumberTableAttrName){
			readLineNumbers(m, a);
		}
		attrs[i]=a;
	}
	deleteBuffer(buf);
	m->codeAttr=ca;
}

void readLineNumbers(Method * m, Attribute * a){
	Buffer * buf=Buffer_wrap(a->info);
	uint16_t table_length=getU16(buf);
	m->lineNumbersTableLength=table_length;
	uint16_t * table=malloc(table_length*2*2);
	for(int i=0;i<table_length;++i){
		table[i*2]=getU16(buf);
		table[i*2+1]=getU16(buf);
	}
	m->lineNumbersTable=table;
}
int16_t getLineNumber(Method * m, int pc){
	uint16_t * table=m->lineNumbersTable;
	int lastLineNumber=-2;
	for(int i=0;i<m->lineNumbersTableLength;++i){
		if(pc>table[i*2]){
			lastLineNumber=table[i*2+1]-1;
			continue;
		}
		else
			return table[i*2+1]-1;
	}
	return lastLineNumber;
}
/*
int parseType(String * type){
	int length=0;
	uint16_t * t=type->str;
	uint32_t len=type->length;
	for(uint32_t i=1;i<len;++i){
		switch(t[i]){
			case 'L':
				while(t[i]!=';')++i;
				++length;
				break;
			case '[':
				++length;
				while(t[i]=='[')++i;
				if(t[i]=='L')
					while(t[i]!=';')++i;
				break;
			case 'J':
			case 'D':
				length+=2;
				break;
			default:
				++length;
				break;
			case ')':
				return length;
		}
	}
	return length;
}
*/

void deleteMethod(Method * m, Constant ** cpool){
	for(uint16_t i=0;i<m->attrsCount;++i){
		deleteAttribute(m->attrs[i]);
	}
	for(int32_t i=0;i<m->stackMapTableSize;++i){
		StackMapEntry *sme=m->stackMapTable[i];
		free(sme->lv);
		free(sme->opstack);
		free(sme);
	}
	if(m->stackMapTable!=NULL)
		free(m->stackMapTable);
	if(m->codeAttr!=NULL){
		deleteCodeAttribute(m->codeAttr);
	}
	Constant * mt=cpool[m->typeIndex];
	if(mt->data!=NULL){
		deleteMethodType(mt->data);
		mt->data=NULL;
	}
	free(m->attrs);
	free(m);
}
StackMapEntry * getStackMapEntry(Method * m, int32_t pc){
	StackMapEntry * entry=NULL;
	for(int32_t i=0;i<m->stackMapTableSize;++i){
		entry=m->stackMapTable[i];
		if(pc<=entry->pc)
			return entry;
	}
	return entry;
}

int8_t calcNativeMethodType(int8_t returnType, int8_t * args, int8_t off, int8_t len){
	int8_t type;
	switch(len){
		case 0:
			type=0;
			break;
		case 1:
			type=3;
			break;
		case 2:
			switch(args[off+0]){
				default:
					type=6;
					break;
				case 'J':
				case 'D':
					type=9;
					break;
			}
			break;
		case 3:
			switch(args[off+0]){
				default:
					//same as from case 2 +6
					type = 6 + calcNativeMethodType(returnType, args, 1, len-1);
					break;
				case 'J':
				case 'D':
					type=18;
					break;
			}
			break;
		case 4:
			switch(args[off+0]){
				default:
					//same as from case 3 + 9
					type = 9 + calcNativeMethodType(returnType, args, 1, len-1);
					break;
				case 'J':
				case 'D':
					//same as from case 2 + 24
					type = 24 + calcNativeMethodType(returnType, args, 2, len-2);
					break;

			}
			break;
		case 5:
			switch(args[off+0]){
				default:
					//same as ...
					type = 12 + calcNativeMethodType(returnType, args, 1, len-1);
					break;
				case 'J':
				case 'D':
					//same as ...
					type = 46 + calcNativeMethodType(returnType, args, 2, len-2);
					break;

			}
			break;
	}
	switch(returnType){
		case 'V':
			return type;
		default:
			return type+1;
		case 'J':
		case 'D':
			return type+2;
	}
	return type;
}
