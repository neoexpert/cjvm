#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <ffi.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../io/buffer.h"
#include "constant.h"
#include "../util/hashmap.h"
#include "../util/arraylist.h"
#include "../lang/strings.h"
#include "attribute.h"
#include "field.h"
#include "method.h"
#include "class.h"
#include "classloader.h"
#include "preprocessor.h"
#include "ops.h"
#include "ACCESS_MODIFIERS.h"
#ifndef LITTLE_ENDIAN 
#define LITTLE_ENDIAN (*(unsigned char *)&(uint16_t){1})
#endif
//#define VERBOSED

int processcode(CodeProcessorFrame * cpf, int32_t pc, int32_t stackpos, int8_t * lv, int8_t * opstack){
	Method * m=cpf->m;
	int32_t length=cpf->code_length;
	uint8_t * code=cpf->code;
	int8_t * processed=cpf->processed;
	if(processed[pc]==1)return 0;
	uint16_t index;
	int16_t jump16;
	Constant * c;
	if(lv==NULL)
		lv = calloc(m->max_lv,1);
	else{
		int8_t * lv_copy = calloc(m->max_lv,1);
		memcpy(lv_copy, lv, m->max_lv);
		lv = lv_copy;
	}
	if(opstack==NULL)
		opstack = calloc(m->max_stack,1);
	else{
		int8_t * opstack_copy = calloc(m->max_stack,1);
		memcpy(opstack_copy, opstack, m->max_stack);
		opstack = opstack_copy;
	}

	int32_t i1,i2,i3,i4;
	MethodType * mt=m->mt;
	if(pc==0){
		int argsoffset=0;
		if(!ISSTATIC(m->accessFlags)){
			lv[argsoffset++]=1;
		}
		for(int i=0; i<mt->argsLength;++i){
			int8_t arg=mt->args[i];
			switch(arg){
				case 'L':
				case '[':
					lv[argsoffset++]=1;
					break;
				default:
					lv[argsoffset++]=0;
					break;
			}
		}
		addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
	}
#ifdef VERBOSED
	fprintf(stderr, "processing method: ");
	fprint(stderr, m->signature);
	fprintf(stderr, " from %d to %d \n", pc, length);
	fprintf(stderr, "max_lv: %d, max_stack: %d \n",m->max_lv, m->max_stack);
#endif
	mainLoop:
	do {
		if(stackpos<0||stackpos>m->max_stack){
			fprintf(stderr,"error, stackoverflow %d\n",stackpos);
			exit(-1);
		}
#ifdef VERBOSED
		fprintf(stderr,"stackpos: %d\n",stackpos);
		fprintf(stderr,"%d %s\n",pc,OP_toString(code[pc]));
		//printf("bigtolittle op: %#010x\n",code[pc]);
#endif
		//printf("%d %s\n",pc,OP_toString(code[pc]));
		if(processed[pc]==1){
#ifdef VERBOSED
			fprintf(stderr,"already processed at %d\n",pc);
#endif
			free(lv);free(opstack);
			return 0;
		}
		processed[pc]=1;
		switch (code[pc]) {
			case NOP:
				pc += 1;
				continue;
			case ACONST_NULL:
			case ICONST_M1:
			case ICONST_0:
			case ICONST_1:
			case ICONST_2:
			case ICONST_3:
			case ICONST_4:
			case ICONST_5:
			case FCONST_0:
			case FCONST_1:
			case FCONST_2:
				opstack[stackpos++]=0;
				pc+=1;
				continue;
			case LCONST_0:
			case LCONST_1:
			case DCONST_0:
			case DCONST_1:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc+=1;
				continue;
			case BIPUSH:
				opstack[stackpos++]=0;
				pc += 2;
				continue;
			case SIPUSH:
				opstack[stackpos++]=0;
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				pc += 3;
				continue;
			case LDC:
				c=cpf->cpool[code[pc+1]];
				switch(c->tag){
					default:
						opstack[stackpos++]=0;
						break;
					case CString:
					case CClass:
						opstack[stackpos++]=1;
						addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				}
				pc += 2;
				continue;
			case LDC_W:
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				c=cpf->cpool[index];
				switch(c->tag){
					default:
						opstack[stackpos++]=0;
						break;
					case CString:
					case CClass:
						opstack[stackpos++]=1;
						addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				}
				pc += 3;
				continue;
			case LDC2_W:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				pc += 3;
				continue;
			case ILOAD:
			case FLOAD:
				opstack[stackpos++]=0;
				pc += 2;
				continue;
			case LLOAD:
			case DLOAD:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 2;
				continue;
			case ALOAD:
				opstack[stackpos++]=1;
				pc += 2;
				continue;
			case ILOAD_0:
			case FLOAD_0:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ILOAD_1:
			case FLOAD_1:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ILOAD_2:
			case FLOAD_2:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ILOAD_3:
			case FLOAD_3:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LLOAD_0:
			case DLOAD_0:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LLOAD_1:
			case DLOAD_1:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LLOAD_2:
			case DLOAD_2:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LLOAD_3:
			case DLOAD_3:
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ALOAD_0:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ALOAD_1:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ALOAD_2:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ALOAD_3:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case IALOAD:
			case FALOAD:
			case BALOAD:
			case CALOAD:
			case SALOAD:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case LALOAD:
			case DALOAD:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case AALOAD:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ISTORE:
			case FSTORE:
				lv[code[pc+1]]=0;
				opstack[--stackpos]=0;
				pc += 2;
				continue;
			case LSTORE:
			case DSTORE:
				lv[code[pc+1]]=0;
				lv[code[pc+1]+1]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc += 2;
				continue;
			case ASTORE_0:
				lv[0]=1;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case ASTORE_1:
				lv[1]=1;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case ASTORE_2:
				lv[2]=1;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case ASTORE_3:
				lv[3]=1;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case ASTORE:
				lv[code[pc+1]]=1;
				opstack[--stackpos]=0;
				pc += 2;
				continue;
			case LSTORE_0:
			case DSTORE_0:
				lv[0]=0;
				lv[1]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case LSTORE_1:
			case DSTORE_1:
				lv[1]=0;
				lv[2]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case LSTORE_2:
			case DSTORE_2:
				lv[2]=0;
				lv[3]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case LSTORE_3:
			case DSTORE_3:
				lv[3]=0;
				lv[4]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case ISTORE_0:
			case FSTORE_0:
				lv[0]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case ISTORE_1:
			case FSTORE_1:
				lv[1]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case ISTORE_2:
			case FSTORE_2:
				lv[2]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case ISTORE_3:
			case FSTORE_3:
				lv[3]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case IASTORE:
			case FASTORE:
			case AASTORE:
			case CASTORE:
			case SASTORE:
			case BASTORE:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case LASTORE:
			case DASTORE:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case POP:
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case POP2:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc +=1;
				continue;
			case DUP:
				opstack[stackpos]=opstack[stackpos-1];
				++stackpos;
				pc += 1;
				continue;
			case DUP_X1:
				i1 = opstack[stackpos-1];
				i2 = opstack[stackpos-2];
				opstack[stackpos-2]=i1;
				opstack[stackpos-1]=i2;
				opstack[stackpos]=i1;
				++stackpos;
				pc += 1;
				continue;
			case DUP_X2:
				i1 = opstack[stackpos-1];
				i2 = opstack[stackpos-2];
				i3 = opstack[stackpos-3];
				opstack[stackpos-3]=i1;
				opstack[stackpos-2]=i3;
				opstack[stackpos-1]=i2;
				opstack[stackpos]=i1;
				++stackpos;
				pc += 1;
				continue;
			case DUP2:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				opstack[stackpos-2]=i1;
				opstack[stackpos-1]=i2;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case DUP2_X1:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				i3 = opstack[stackpos-3];
				opstack[stackpos-3]=i1;
				opstack[stackpos-2]=i2;
				opstack[stackpos-1]=i3;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case DUP2_X2:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				i4 = opstack[stackpos-3];
				i3 = opstack[stackpos-4];
				opstack[stackpos-4]=i1;
				opstack[stackpos-3]=i2;
				opstack[stackpos-2]=i3;
				opstack[stackpos-1]=i4;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case SWAP:
				i1=opstack[stackpos-1];
				i2=opstack[stackpos-2];
				opstack[stackpos-2]=i1;
				opstack[stackpos-1]=i2;
				pc += 1;
				continue;
			case IADD:
			case FADD:
			case ISUB:
			case FSUB:
			case IMUL:
			case FMUL:
			case IDIV:
			case FDIV:
			case IREM:
			case FREM:
			case ISHL:
			case ISHR:
			case IUSHR:
			case IAND:
			case IOR:
			case IXOR:
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case LADD:
			case DADD:
			case LSUB:
			case DSUB:
			case LMUL:
			case DMUL:
			case LDIV:
			case DDIV:
			case LREM:
			case DREM:
			case LAND:
			case LOR:
			case LXOR:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case LSHL:
			case LSHR:
			case LUSHR:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case INEG:
			case LNEG:
			case FNEG:
			case DNEG:
				pc += 1;
				continue;
			case IINC:
				pc += 3;
				continue;
			case I2L:
			case I2D:
			case F2L:
			case F2D:
				opstack[stackpos++]=0;
				pc+=1;
				continue;
			case L2I:
			case L2F:
			case D2I:
			case D2F:
				opstack[--stackpos]=0;
				pc+=1;
				continue;
			case I2F:
			case L2D:
			case F2I:
			case D2L:
			case I2B:
			case I2C:
			case I2S:
				pc += 1;
				continue;
			case LCMP:
			case DCMPL:
			case DCMPG:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case FCMPL:
			case FCMPG:
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case IFEQ:
			case IFNE:
			case IFLT:
			case IFGE:
			case IFGT:
			case IFLE:
				opstack[--stackpos]=0;

				index=*((uint16_t*)(code+pc+1));
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				jump16=(uint16_t)index;
				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					//if(pc+jump16<length){
						int res=processcode(cpf, pc+jump16 , stackpos, lv, opstack); 
						if(res!=0) return res;
					//}
					//pc+=jump16;
					//continue;
				//}
				pc+=3;
				continue;
			case IF_ICMPEQ:
			case IF_ICMPNE:
			case IF_ICMPLT:
			case IF_ICMPGE:
			case IF_ICMPGT:
			case IF_ICMPLE:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				index=*((uint16_t*)(code+pc+1));
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				jump16=(uint16_t)index;
				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					//if(pc+jump16<length){
						res=processcode(cpf, pc+jump16 , stackpos, lv, opstack); 
						if(res!=0) return res;
					//}
					//pc+=jump16;
					//continue;
				//}
				pc += 3;
				continue;
			case IF_ACMPEQ:
			case IF_ACMPNE:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				index=*((uint16_t*)(code+pc+1));
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				jump16=(uint16_t)index;
				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					//if(pc+jump16<length){
						res=processcode(cpf, pc+jump16 , stackpos, lv, opstack); 
						if(res!=0) return res;
					//}
					//pc+=jump16;
					//continue;
				//}
				pc += 3;
				continue;
			case GOTO:
				index=*((uint16_t*)(code+pc+1));
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				jump16=(uint16_t)index;
				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					/*
					if(pc+jump16<length){
						int res=processcode(m, cpool, pc+3, stackpos, pc+jump16, lv, opstack); 
						if(res!=0) return res;
					}*/
					pc+=jump16;
					continue;
				//}
				//else return 0;
				//pc+=3;
				//continue;
			case JSR:
				opstack[stackpos++]=0;
				++pc;
				continue;
			case RET:
				opstack[--stackpos]=0;
				++pc;
				continue;
			case TABLESWITCH:
				opstack[--stackpos]=0;
				int origpc=pc;
        pc = (pc + 4)&0xfffffffc;
				int32_t default_offset = *((int32_t*)(code+pc));
#ifdef LITTLE_ENDIAN
				default_offset =__builtin_bswap32(default_offset);
					*((int32_t*)(code+pc))=
						default_offset;
#endif
					res=processcode(cpf, origpc+default_offset , stackpos, lv, opstack); 
					if(res!=0) return res;

				pc += 4;
				int32_t low=*(int32_t*)(code+pc);
#ifdef LITTLE_ENDIAN
				low=__builtin_bswap32(low);
					*(int32_t*)(code+pc)=
						low;
#endif
				pc += 4;
				int32_t high=*(int32_t*)(code+pc);
#ifdef LITTLE_ENDIAN
				high=__builtin_bswap32(high);
					*(int32_t*)(code+pc)=
						high;
#endif
				pc += 4;
				int32_t pc2=pc;
				for(int i=low;i<=high;++i){
					//printf("case %d\n",i);
					pc=pc2 + (i - low) * 4;
					int32_t jump32= *(int32_t*)(code+pc);

#ifdef LITTLE_ENDIAN
					jump32 =__builtin_bswap32(jump32);
					*((int32_t*)(code+pc))=jump32;
#endif
					int res=processcode(cpf, origpc+jump32, stackpos, lv, opstack); 
					if(res!=0) return res;
				}
				pc+=8;
				
				free(lv);free(opstack);
				return 0;
			case LOOKUPSWITCH:
				opstack[--stackpos]=0;
				origpc=pc;
        			pc = (pc + 4)&0xfffffffc;
				default_offset = *((int32_t*)(code+pc));
#ifdef LITTLE_ENDIAN
				default_offset =__builtin_bswap32(default_offset);
					*((int32_t*)(code+pc))=
						default_offset;
#endif
					res=processcode(cpf, origpc+default_offset, stackpos, lv, opstack); 
					if(res!=0) return res;
					pc += 4;

					uint32_t len=*((uint32_t*)(code+pc));
#ifdef LITTLE_ENDIAN
					len =__builtin_bswap32(len);
					*((int32_t*)(code+pc))=
						len;
#endif
					pc += 4;
					for (int i = 0; i < len; ++i) {
						int32_t key=*((int32_t*)(code+pc));
#ifdef LITTLE_ENDIAN
						key =__builtin_bswap32(key);
						*((int32_t*)(code+pc))=
							key;
#endif
						pc += 4;
						int32_t value=*((int32_t*)(code+pc));
#ifdef LITTLE_ENDIAN
						value =__builtin_bswap32(value);
						*((int32_t*)(code+pc))=
							value;
						int res=processcode(cpf, origpc+value, stackpos, lv, opstack); 
						if(res!=0) return res;
#endif
						pc += 4;
					}
				free(lv);free(opstack);
				return 0;
			case IRETURN:
			case FRETURN:
			case ARETURN:
				opstack[--stackpos]=0;
				pc += 1;
				free(lv);free(opstack);
				return 0;
			case LRETURN:
			case DRETURN:
				opstack[--stackpos]=0;
				opstack[--stackpos]=0;
				pc += 1;
				free(lv);free(opstack);
				return 0;
			case RETURN:
				pc += 1;
				free(lv);free(opstack);
				return 0;
			case GETSTATIC:
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				c=cpf->cpool[index];
				FieldHead *fh=c->data;
				switch(fh->type->str[0]){
					case 'J':
					case 'D':
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						break;
					case 'L':
					case '[':
						opstack[stackpos++]=1;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						opstack[stackpos++]=0;
						break;
					default:
						fprintf(stderr, "unknown field type: %c\n",fh->type->str[0]);
						exit(-1);
						return -1;
				}
				pc += 3;
				//class=getClass(c3->str, 1);
				//f = HashMap_get(class->sft,c2->str);
				//c1->value=f->value;
				//c1->tag=0;
				continue;
			case PUTSTATIC:
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				c=cpf->cpool[index];
				fh=c->data;
				//fprintln(stderr, fh->classname);
				//fprintf(stderr, "%c\n",fh->type->str[0]);
				switch(fh->type->str[0]){
					case 'J':
					case 'D':
						opstack[--stackpos]=0;
						opstack[--stackpos]=0;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'L':
					case '[':
					case 'Z':
						opstack[--stackpos]=0;
						break;
					default:
						fprintf(stderr, "unknown field type: %c\n",fh->type->str[0]);
						exit(-1);
						return -1;
				}
				pc += 3;
				//class=getClass(c3->str, 1);
				//f = HashMap_get(class->sft,c2->str);
				//c1->value=f->value;
				//c1->tag=0;
				continue;
			case GETFIELD:
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				//fprintf(stderr, "PRE GETFIELD index: %d\n",index);

				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				c=cpf->cpool[index];
				fh=c->data;
				opstack[--stackpos]=0;
				switch(fh->type->str[0]){
					case 'J':
					case 'D':
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						break;
					case 'L':
					case '[':
						opstack[stackpos++]=1;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						opstack[stackpos++]=0;
						break;
					default:
						fprintf(stderr, "unknown field type: %c\n",fh->type->str[0]);
						exit(-1);
						return -1;
				}
				pc += 3;
				//class=getClass(c3->str, 1);
				//f = HashMap_get(class->sft,c2->str);
				//c1->value=f->value;
				//c1->tag=0;
				continue;
			case PUTFIELD:
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);

				c=cpf->cpool[index];
				fh=c->data;
				//fprintln(stderr, fh->classname);
				//fprintf(stderr, "%c\n",fh->type->str[0]);
				opstack[--stackpos]=0;
				switch(fh->type->str[0]){
					case 'J':
					case 'D':
						opstack[--stackpos]=0;
						opstack[--stackpos]=0;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'L':
					case '[':
					case 'Z':
						opstack[--stackpos]=0;
						break;
					default:
						fprintf(stderr, "unknown field type: %c\n",fh->type->str[0]);
						exit(-1);
						return -1;
				}
				pc += 3;
				//class=getClass(c3->str, 1);
				//f = HashMap_get(class->sft,c2->str);
				//c1->value=f->value;
				//c1->tag=0;
				continue;
			case INVOKEVIRTUAL:
			case INVOKESPECIAL:
				opstack[--stackpos]=0;
			case INVOKESTATIC:
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				c = cpf->cpool[index];
				MethodHead * mh=c->data;
				MethodType * mt=mh->mt;

				//uint8_t returnType=c->ivalue;
				//signature->str[signature->length-1];
				/*
				fprint(stderr,mh->classname);
				fprintf(stderr,".");
				fprintln(stderr,mh->signature);
				fprintf(stderr, "argsCount: %d\n",mt->argsLength);
				*/
				for(int i=0;i<mt->argsLength;++i){
					opstack[--stackpos]=0;
				}
				switch(mt->returnType){
					case 'L':
					case '[':
						opstack[stackpos++]=1;
						break;
					case 'J':
					case 'D':
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						opstack[stackpos++]=0;
						break;
					case 'V':
						break;
					default:
						fprintf(stderr, "unknown return type: %c\n",mt->returnType);
						exit(-1);
						return -1;


				}
				//fprintf(stderr, "%c\n",mt->returnType);
				pc += 3;
				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				continue;
			case INVOKEINTERFACE:
				opstack[--stackpos]=0;
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				c = cpf->cpool[index];
				mh=c->data;
				mt=mh->mt;
				/*
				fprint(stderr,mh->classname);
				fprintf(stderr,".");
				fprintln(stderr,mh->signature);
				*/
				for(int i=0;i<mt->argsLength;++i){
					opstack[--stackpos]=0;
				}
				switch(mt->returnType){
					case 'L':
						opstack[stackpos++]=1;
						break;
					case 'J':
					case 'D':
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
						break;
					case 'I':
					case 'S':
					case 'B':
					case 'C':
					case 'F':
					case 'Z':
						opstack[stackpos++]=0;
						break;
					case 'V':
						break;
					default:
						fprintf(stderr, "unknown return type: %c\n",mt->returnType);
						exit(-1);
						return -1;


				}

				pc += 5;
				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				continue;
			case INVOKEDYNAMIC:
				pc+=5;
				continue;
			case NEW:
				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				opstack[stackpos++]=1;
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				pc += 3;
				continue;
			case NEWARRAY:
				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				opstack[--stackpos]=0;
				opstack[stackpos++]=1;
				pc += 2;
				continue;
			case ANEWARRAY:
				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				opstack[--stackpos]=0;
				opstack[stackpos++]=1;
				pc += 3;
				continue;
			case ARRAYLENGTH:
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ATHROW:
				//stackpos=0;
				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				pc += 1;
				free(lv);free(opstack);
				return 0;
			case CHECKCAST:
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				pc += 3;
				continue;
			case INSTANCEOF:
				opstack[--stackpos]=0;
				opstack[stackpos++]=0;
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif
				pc += 3;
				continue;
			case MONITORENTER:
			case MONITOREXIT:
				opstack[--stackpos]=0;
				pc += 1;
				continue;
			case WIDE:
				pc += 1;
				switch (code[pc]) {
					case ILOAD:
					case FLOAD:
						opstack[stackpos++]=0;
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
						pc += 3;
						continue;
					case LLOAD:
					case DLOAD:
						opstack[stackpos++]=0;
						opstack[stackpos++]=0;
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
						pc += 3;
						continue;
					case ALOAD:
						opstack[stackpos++]=1;
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
						pc += 3;
						continue;
					case ISTORE:
					case FSTORE:
						opstack[--stackpos]=0;
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
						pc += 3;
						continue;
					case LSTORE:
					case DSTORE:
						opstack[--stackpos]=0;
						opstack[--stackpos]=0;
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
						pc += 3;
						continue;
					case ASTORE:
						opstack[--stackpos]=0;
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;
#endif
						lv[index]=1;
						pc += 3;
						continue;
					case IINC:
#ifdef LITTLE_ENDIAN
						index=*((uint16_t*)(code+pc+1));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+1))=index;

						index=*((uint16_t*)(code+pc+3));
						index=(index>>8) | (index<<8);
						*((uint16_t*)(code+pc+3))=index;
#endif
						pc += 5;
						continue;

				}
				printf("wide error\n");
				return -1;

				//throw new RuntimeException("wide not implememted");
				//continue;
			case MULTIANEWARRAY:
				addStackEntry(m, cpf->stackMapTable, pc, lv, opstack);
				{
				uint8_t dimensions=code[pc + 3];
				//int32_t * sizes = malloc(4*dimensions);
				for (uint8_t i= 0; i < dimensions; ++i) {
					opstack[--stackpos]=0;
				}
				opstack[stackpos++]=1;
				}
#ifdef LITTLE_ENDIAN
				index=*((uint16_t*)(code+pc+1));
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				pc += 4;
				continue;
			case IFNULL:
			case IFNONNULL:
				opstack[--stackpos]=0;
				index=*((uint16_t*)(code+pc+1));
#ifdef LITTLE_ENDIAN
				index=(index>>8) | (index<<8);
				*((uint16_t*)(code+pc+1))=index;
#endif

				jump16=(int16_t)index;
				if(pc+jump16<0||pc+jump16>=length)
					return -1;
				//if(jump16>0){
					//if(pc+jump16<length){
						res=processcode(cpf,pc+jump16, stackpos, lv, opstack); 
						if(res!=0) return res;
					//}
					//pc+=jump16;
					//continue;
				//}
				pc += 3;
				continue;
			case GOTO_W:
			case JSR_W:
				//pc +=((code[pc + 1]) << 24) | ((code[pc + 2]) << 16) |
					//((code[pc + 3]) << 8) | (code[pc + 4]) ;
					pc+=5;
				continue;
			default:
				pc += 1;
				printf("bigtolittle unsupported opcode: %d\n", code[pc]);
		}
	}while (pc<length);
	free(lv);free(opstack);
	return 0;
}

void printStackMapTable(Method * m, ArrayList * stackMapTable){
	for(int i=0; i<stackMapTable->size;++i){
		StackMapEntry * sme = stackMapTable->values[i];
		fprintf(stderr, "StackMapEtnry %d: pc= %d ", i ,sme->pc);
		fprintf(stderr, "lv_map: ");
		for(int i=0;i<m->max_lv;++i){
			fprintf(stderr, "lv[%d]=%d ,",i, sme->lv[i]);
		}
		fprintf(stderr, "stack_map: ");
		for(int i=0;i<m->max_stack;++i){
			fprintf(stderr, "%d ,",sme->opstack[i]);
		}
		fprintf(stderr, "\n");

	}
}
void addStackEntry(Method * m, ArrayList * stackMapTable, int32_t pc, int8_t * lv, int8_t * opstack){
	StackMapEntry * sme = malloc(sizeof(StackMapEntry));
	sme->pc=pc;
	sme->opstack = malloc(m->max_stack);
	memcpy(sme->opstack, opstack, m->max_stack);
	sme->lv = malloc(m->max_lv);
	memcpy(sme->lv, lv, m->max_lv);

#ifdef VERBOSED
	fprintln(stderr, m->signature);
	fprintf(stderr, "pc = %d: \n",pc);
	fprintf(stderr, "lv_map: ");
	for(int i=0;i<m->max_lv;++i){
		fprintf(stderr, "%d, ", lv[i]);
	}
	fprintf(stderr, "\nopstack_map: ");
	for(int i=0;i<m->max_stack;++i){
		fprintf(stderr, "%d, ", opstack[i]);
	}
	fprintf(stderr, "\n");

	fprintf(stderr, "before:\n");
	//printStackMapTable(m, stackMapTable);
#endif
	for(int i=0;i<stackMapTable->size;++i){
		StackMapEntry * _sme=stackMapTable->values[i];
		if(_sme->pc>=sme->pc){
			ArrayList_addAt(stackMapTable, i, sme);
#ifdef VERBOSED
			fprintf(stderr, "after addAt:\n");
			//printStackMapTable(m, stackMapTable);
#endif
			return;
		}

	}
	ArrayList_add(stackMapTable, sme);
#ifdef VERBOSE
	//printStackMapTable(m, stackMapTable);
#endif
			return;
}

CodeProcessorFrame * createCodeProcessorFrame(Method * m, Constant ** cpool){
	CodeProcessorFrame * cpf=malloc(sizeof(CodeProcessorFrame));
	cpf->m=m;
	cpf->code=m->codeAttr->code;
	cpf->cpool=cpool;
	int32_t code_length=m->codeAttr->code_length;
	cpf->processed=calloc(code_length,1);
	cpf->stackMapTable = newArrayList(16);
	cpf->code_length=code_length;
	return cpf;
}
void deleteCodeProcessorFrame(CodeProcessorFrame * cpf){
	free(cpf->processed);
	free(cpf->stackMapTable);
	free(cpf);
}
