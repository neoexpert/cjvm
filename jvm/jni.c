#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <stdbool.h>
#include <wchar.h>
#include <locale.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <pthread.h>
#include <limits.h>
#include <ffi.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../io/buffer.h"
#include "constant.h"
#include "../util/hashmap.h"
#include "../util/linkedlist.h"
#include "../util/iarraylist.h"
#include "../util/arraylist.h"
#include "../lang/strings.h"
#include "attribute.h"
#include "field.h"
#include "method.h"
#include "class.h"
#include "classloader.h"
#include "ops.h"
#include "heap.h"
#include "frame.h"
#include "jni.h"
#include "thread.h"

/*
int nativeHandler(int type, void * handler, int32_t * lv, int32_t * opstack){
#ifdef VERBOSE
	fprintf(stderr, "nativeHandler entered, type: %d\n", type);
#endif
	switch(type){
		case __V:
			((M__V*)handler)(jniEnv);
			return 0;
		case __I:
			opstack[0]=((M__I*)handler)(jniEnv);
			return 1;
		case __J:
			*(int64_t*)(opstack)=((M__J*)handler)(jniEnv);
			return 2;
		case _I_V:
			((M_I_V*)handler)(jniEnv, lv[0]);
			return 0;
		case _I_I:
			opstack[0]=((M_I_I*)handler)(jniEnv,lv[0]);
			return 1;
		case _I_J:
			*(int64_t*)(opstack)=((M_I_J*)handler)(jniEnv,lv[0]);
			return 2;
		case _II_I:
			opstack[0]=((M_II_I*)handler)(jniEnv,lv[0], lv[1]);
			return 1;
		case _IIII_I:
			opstack[0]=((M_IIII_I*)handler)(jniEnv,lv[0], lv[1], lv[2], lv[3]);
			return 1;
		case _JI_I:
			opstack[0]=((M_JI_I*)handler)(jniEnv,*(int64_t*)(lv+0), lv[2]);
			return 1;
		case _JI_J:
			*(int64_t*)(opstack)=((M_JI_J*)handler)(jniEnv,*(int64_t*)(lv+0), lv[2]);
			return 2;
		case _IIIII_V:
			((M_IIIII_V*)handler)(jniEnv, lv[0], lv[1], lv[2], lv[3], lv[4]);
			return 0;
		default:
			fprintf(stderr, "unknown native methodtype: %d\n", type);
			exit(-1);
			return 0;
	}
}
*/

void fillInStackTrace(JNINativeInterface ** env, uint32_t * oref){
	Thread * currentThread = pthread_getspecific(glob_threadID_key);
	FrameStack * stack=currentThread->stack;
	Class * throwableClass=getClass(JAVA_LANG_THROWABLE, false);
	jfieldID stackTraceFieldID = (*env)->GetFieldID(env, throwableClass, "stacktrace", "[Ljava/lang/StackTraceElement;");
	Class * steClass=getClass(JAVA_LANG_STACKTRACEELEMENT, false);
	jfieldID classNameFieldID = (*env)->GetFieldID(env, steClass, "declaringClass", "Ljava/lang/String;");
	jfieldID methodNameFieldID = (*env)->GetFieldID(env, steClass, "methodName", "Ljava/lang/String;");
	jfieldID fileNameFieldID = (*env)->GetFieldID(env, steClass, "fileName", "Ljava/lang/String;");
	jfieldID lineNumberFieldID = (*env)->GetFieldID(env, steClass, "lineNumber", "I");
	int size=getStackSize(stack);
	uint32_t aref=createArrayInstance('L', size);
	jobjectArray stacktrace=(*env)->NewLocalRef(env, &aref);
	*(uint32_t*)(heap+ *oref + stackTraceFieldID * 4)=aref;
	uint32_t * arr=heap+aref;
	for(int i=0;i<size;++i){
		Frame * f=getFrame(stack, i);
		Method * m=f->method;
		uint32_t steOref=createClassInstance(steClass);
		jobject ste=(*env)->NewLocalRef(env, &steOref);
		((uint32_t*)(heap+*stacktrace))[i]=*ste;
		*(uint32_t*)(heap+*ste+classNameFieldID*4)=*createStringInstance(stack, m->clazz->name);
		*(uint32_t*)(heap+*ste+methodNameFieldID*4)=*createStringInstance(stack, m->signature);
		*(uint32_t*)(heap+*ste+fileNameFieldID*4)=*createStringInstance(stack, f->method->clazz->fileName);
		*(int32_t*)(heap+*ste+lineNumberFieldID*4)=getLineNumber(m, f->pc);
#ifdef VERBOSE
		fprintf(stderr, "______");
		fprint(stderr, m->clazz->name);
		fprintf(stderr, ".");
		fprint(stderr, m->signature);
		fprintf(stderr, " (%d)\n",getLineNumber(m, f->pc));
#endif
	}
		
}

int32_t getClassClassInstanceIndex(JNINativeInterface **env, Class * class){
	if(class->orefIndex!=-1)
		return class->orefIndex;
	//fprintf(stderr, "getClassClassInstanceIndex for ");
	//fprintln(stderr, class->name);
	//disableGC();
	Thread * currentThread = pthread_getspecific(glob_threadID_key);
	FrameStack * stack=currentThread->stack;
	uint32_t oref = createClassInstance(java_lang_Class);
	jobject this=(*env)->NewLocalRef(env, &oref);

	int32_t index=addStaticRef();
	static_refs[index]=*this;
	class->orefIndex=index;
	ClassInstance * ci=heap+*this-sizeof(ClassInstance);
	uint32_t *  name=createStringInstance(stack, class->name);
	putField(*this, intern(_s("name")), *name);
	uint32_t * code_source=createStringInstance(stack, class->code_source);
	putField(*this, intern(_s("code_source")), *code_source);
	//fprint(stderr, class->name);
	//fprintf(stderr, " : orefIndex: %d : codeSourceRef: %d ", class->orefIndex, code_source_ref);
	//fprintln(stderr, class->code_source);
	//enableGC();
	return index;
}

uint32_t * java_lang_Object_getClass(void * env, uint32_t * this){
	ClassInstance *ci=heap+*this-sizeof(ClassInstance);
	int32_t index=getClassClassInstanceIndex(jniEnv, ci->clazz);
	return &static_refs[index];

}

uint32_t * forName(void * env, uint32_t * classname){
	return NULL;
}

void print0(void * env, int32_t * oref){
	//int32_t oref=lv[0];
	if(oref==0){
		wprintf(L"null");
		return;
	}
	int32_t voref=getField(*oref, intern(_s("value")));
	CharArrayInstance * cai=heap+voref-sizeof(ArrayInstance);
	uint16_t * arr=heap+voref;

	for(int i=0;i<cai->size;++i){
		putwchar((uint32_t)arr[i]);
	}
}

void printI(void * env, int32_t i){
	wprintf(L"%d\n",i);
}

void loadLibrary(void * env, uint32_t * oref){
	//ClassInstance * this=heap+lv[0];
	//int32_t oref=lv[0];
	int32_t voref=getField(*oref, intern(_s("value")));
	uint16_t * arr=heap+voref;
	CharArrayInstance * cai=heap+voref-sizeof(ArrayInstance);
	char * libname=malloc(cai->size+1);
	for(int i=0;i<cai->size;++i){
		libname[i]=(char)arr[i];
	}
	libname[cai->size]='\0';
	fprintf(stderr, "loadlibrary: %s\n",libname);
#ifdef VERBOSE
#endif

	char libpath[PATH_MAX+1];
	snprintf(libpath, PATH_MAX, "%s%s%s", "./bin/shared/", libname, ".so");

	void * library= dlopen(libpath,  RTLD_LAZY | RTLD_GLOBAL);
	free(libname);
	if (!library) {
		//
		// Apparently, the library could not be opened
		fprintf(stderr, "Could not open \"%s\"\n", libpath);
		exit(1);
	}
	ArrayList_add(nativeLibraries, library);
	fprintf(stderr, "loadlibrary: done \n");
}

void arraycopy(void * env, uint32_t * arr1ref, int32_t offset1, uint32_t * arr2ref, int32_t offset2, int32_t count){
	void * arr1=heap+*arr1ref;//lv[0];
	ArrayInstance *a1=arr1-sizeof(ArrayInstance);
	//int32_t offset1=lv[1];
	void * arr2=heap+*arr2ref;//lv[2];
	ArrayInstance *a2=arr2-sizeof(ArrayInstance);
	//int32_t offset2=lv[3];
	//int32_t count=lv[4];
	if(a1->type!=a2->type){
		dumpCurrentHeap();
		fprintf(stderr, "ERROR: array types don't match: %d!=%d aref1: %d, aref2: %d\n",a1->type, a2->type, *arr1ref, *arr2ref);
		exit(-1);
	}
	switch(a1->type){
		case BOOLEAN_ARRAYINSTANCE:
		case BYTE_ARRAYINSTANCE:
			memcpy(arr2+offset2,arr1+offset1,count);
			break;
		case CHAR_ARRAYINSTANCE:
		case SHORT_ARRAYINSTANCE:
			memcpy(arr2+offset2*2, arr1+offset1*2, count * 2);
			break;
		case OBJECT_ARRAYINSTANCE:
		case FLOAT_ARRAYINSTANCE:
		case INT_ARRAYINSTANCE:
			memcpy(arr2+offset2*4, arr1+offset1*4, count*4);
			break;
		case DOUBLE_ARRAYINSTANCE:
		case LONG_ARRAYINSTANCE:
			memcpy(arr2+offset2*8,arr1+offset1*8,count*8);
			break;
		default:
			fprintf(stderr, "ERROR: unknown arraytype: %d\n", a1->type);
	}
}

jint getVersion(JNIEnv * env){
	return 42;
}

jbyte * getByteArrayElements
      (JNIEnv *env, jbyteArray array, jboolean *isCopy){
	return heap+*array;
}

jclass findClass(JNIEnv * env, const char * classname){
	String * _classname=intern(_s(classname));
	return getClass(_classname, false);
}

jclass getObjectClass(JNIEnv * env, jobject obj){
	ClassInstance * ci=heap+*obj-sizeof(ClassInstance);
	return ci->clazz;
}

jfieldID getFieldID(JNIEnv * env, jclass cls, const char * name, const char * type){
	Class * clazz=(Class*)cls;
	Field * f = HashMap_get(clazz->ft,intern(_s(name))); 
	if(f==NULL){
		fprintf(stderr, "no such field: %s\n", name);
		fprintf(stderr, "in class: : ");
		fprintln(stderr, clazz->name);
		exit(-1);
	}
	return f->index;
}
jobject getObjectField(JNIEnv *env, jobject obj, jfieldID fieldID){
	//fprintf(stderr, "obj: %d findex: %d\n",obj,fieldID);
	uint32_t value=*(uint32_t*)(heap+*obj+fieldID*4);
	//fprintf(stderr, "value:: %d \n",value);
	return (heap+*obj+fieldID*4);

}
jint getIntField(JNIEnv *env, jobject obj, jfieldID fieldID){
	//fprintf(stderr, "obj: %d findex: %d\n",obj,fieldID);
	uint32_t value=*(uint32_t*)(heap+*obj+fieldID*4);
	//fprintf(stderr, "value:: %d \n",value);
	return *(uint32_t*)(heap+*obj+fieldID*4);

}

char* getStringUTFChars(JNIEnv *env, jstring str, jboolean *isCopy){
	uint32_t voref=getField(*str, intern(_s("value")));
	CharArrayInstance * cai=heap+voref-sizeof(ArrayInstance);
	uint16_t * arr=heap+voref;
	int32_t len=cai->size;
	char * c_str=malloc(len+1);
	for(int32_t i=0;i<len;++i)
		c_str[i]=arr[i];
	c_str[len]='\0';
	return c_str;
}

void releaseStringUTFChars(JNIEnv *env, jstring str, char * utfChars){
	free(utfChars);
}

jstring newStringUTF(JNIEnv *env, const char *utf){
	Thread * currentThread = pthread_getspecific(glob_threadID_key);
	FrameStack * stack=currentThread->stack;
	return createStringInstance(stack, _s(utf));
}

jobject newLocalRef(JNIEnv * env, jobject ref){
	Thread * currentThread = pthread_getspecific(glob_threadID_key);
	FrameStack * stack=currentThread->stack;
	return createJNIRef(stack, *ref);
}

void deleteLocalRef(JNIEnv *env, jobject obj){
	Thread * currentThread = pthread_getspecific(glob_threadID_key);
	FrameStack * stack=currentThread->stack;
	freeJNIRef(stack, obj);
}

jbyteArray newByteArray(JNIEnv * env, jsize len){
	fprintf(stderr, "in NewByteArray\n");
	Thread * currentThread = pthread_getspecific(glob_threadID_key);
	FrameStack * stack=currentThread->stack;
	uint32_t aref=createArrayInstance('B', len);
	jobject ref = createJNIRef(stack, aref);
	return ref;
}

jcharArray newCharArray(JNIEnv * env, jsize len){
	fprintf(stderr, "in NewCharArray\n");
	Thread * currentThread = pthread_getspecific(glob_threadID_key);
	FrameStack * stack=currentThread->stack;
	uint32_t aref=createArrayInstance('C', len);
	fprintf(stderr, "aref: %d\n", aref);
	jobject ref = createJNIRef(stack, aref);
	fprintf(stderr, "aref from jni: %d\n", *ref);
	return ref;
}

uint32_t createStringConstant(FrameStack * fstack, String * s){
	int32_t index=s->index;
	if(index!=-1) return index;
	//dogc++;
	uint32_t * oref=createStringInstance(fstack, s);
	index=addStaticRef();
	static_refs[index]=*oref;
	//dogc--;
	s->index=index;
	return index;
}

uint32_t * createStringInstance(FrameStack * fstack, String * s){
	Class * jls=getClass(java_lang_String,true);
	uint32_t oref=createClassInstance(jls);
	uint32_t * str=createJNIRef(fstack, oref);
 	uint32_t valueOref=createArrayInstance(T_CHAR, s->length);
	//CharArrayInstance * cai=heap+valueOref-sizeof(ArrayInstance);
	uint16_t * charArray=heap+valueOref;
	for(int32_t i=0;i<s->length;++i){
		charArray[i]=s->str[i];
	}
	putField(*str, intern(_s("value")), valueOref);
	return str;
}

jcharArray java_lang_String_init(JNIEnv * env, jstring this, jbyteArray barr){
	ByteArrayInstance * bai=heap+*barr-sizeof(ByteArrayInstance);
	//fprintf(stderr, "bytearray length: %d\n",bai->size);
	String * s = encodeUTF8(bai->size, heap+*barr);
	//fprintf(stderr, "after encoding: \n");
	//fprintln(stderr, s);
	//disableGC();
	//uint32_t aref = createArrayInstance('C', s->length);
	uint32_t * aref=newCharArray(env, s->length);
	//enableGC();
	memcpy(heap+*aref, s->str, s->length*2);
	deleteString(s);
	deleteLocalRef(env, aref);
	return aref;
}

void initJNI(){
	nativeMethods=newHashMap(32);
	nativeLibraries=newArrayList(8);
	addNativeMethod("java_lang_Class_forName", &forName);
	addNativeMethod("java_lang_System_print0", &print0);
	addNativeMethod("java_lang_System_loadLibrary", &loadLibrary);
	addNativeMethod("java_lang_System_arraycopy", &arraycopy);
	addNativeMethod("java_lang_Throwable_fillInStackTrace", &fillInStackTrace);
	addNativeMethod("java_lang_Object_getClass", &java_lang_Object_getClass);
	addNativeMethod("JVMTest_print", &printI);
	addNativeMethod("java_lang_String_init", &java_lang_String_init);
	jniEnv=malloc(sizeof(void*));
	*jniEnv=malloc(sizeof(JNINativeInterface ));
	(*jniEnv)->GetVersion=&getVersion;
	(*jniEnv)->FindClass=&findClass;
	(*jniEnv)->GetObjectClass=&getObjectClass;
	(*jniEnv)->GetFieldID=&getFieldID;
	(*jniEnv)->GetObjectField=&getObjectField;
	(*jniEnv)->GetIntField=&getIntField;
	(*jniEnv)->GetStringUTFChars=&getStringUTFChars;
	(*jniEnv)->ReleaseStringUTFChars=&releaseStringUTFChars;
	(*jniEnv)->NewStringUTF=&newStringUTF;
	(*jniEnv)->NewLocalRef=&newLocalRef;
	(*jniEnv)->NewByteArray=&newByteArray;
	(*jniEnv)->GetByteArrayElements=&getByteArrayElements;
	int32_t version=(*jniEnv)->GetVersion(jniEnv);
}


void addNativeMethod(char * nativeDesc,  void * nm){
	HashMap_put(nativeMethods, intern(_s(nativeDesc)), nm);
}

void * resolveNativeMethod(String * classname, String * name, String ** error){
	String * tmpClassname = replaceAll(classname, '/', '_');
	String * tmp=concat(tmpClassname,UNDERSCORE);
	String * signature=concat(tmp,name);
	String * nativeDesc=intern(signature);
#ifdef VERBOSE
	fprintf(stderr, "nativeDesc is ");
	fprintln(stderr, nativeDesc);
#endif
	void * fptr = HashMap_get(nativeMethods, nativeDesc);
	if(fptr==NULL){
		nativeDesc=intern(concat(Java_,nativeDesc));
		char * funcName=_cs(nativeDesc);
		for(int i=0;i<nativeLibraries->size;++i){
			void * lib=nativeLibraries->values[i];
			fptr = dlsym(lib, funcName);
			if(fptr!=NULL){
				free(funcName);
				return fptr;
			}
				
		}
		if (!fptr){
			//fprintf(stderr, "native method not found: ");
			//fprintln(stderr, nativeDesc);
			//fprintf(stderr, "Could not get function pointer for %s\n  error is: %s\n\n", funcName, dlerror());
			//exit(-1);
			*error=_s("could not resolve native method");
			return NULL;
		}
		free(funcName);
	}
	deleteString(tmpClassname);
	deleteString(tmp);
	return fptr;
}

