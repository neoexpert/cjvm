#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <signal.h>
#include <math.h>       /* fmod */
#include <stdbool.h>
#include <string.h>
#include <ffi.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../io/buffer.h"
#include "../io/scanner.h"
#include "constant.h"
#include "../util/hashmap.h"
#include "../util/linkedlist.h"
#include "../util/iarraylist.h"
#include "../util/arraylist.h"
#include "../lang/strings.h"
#include "attribute.h"
#include "field.h"
#include "method.h"
#include "class.h"
#include "heap.h"
#include "classloader.h"
#include "ops.h"
#include "frame.h"
#include "jni.h"
#include "bytecodeexecutor.h"
#include "../jni/jni_handler.h"

#ifdef DEBUGGER
int steps=0;
bool print_ops=true;
bool verbose=true;
int stepout=0;
#define BYTECODEPRINTER
#endif
int step(Method * m, Frame * cF, int stackoffset, FrameStack * frameStack){
#ifdef DEBUGGER
	stepout=0;
	printf("\n");
	printf("debugger commands:\n");
	printf("\tENTER: step\n");
	printf("\tr: Run\n");
	printf("\tv: toggle Verbose stepping\n");
	printf("\tt: printsTacktrace\n");
	printf("\tu: stepoUt\n");
	printf("\to: toggle instruction printing\n");
	printf("\tq: quit\n");
#endif
	int pc=0;
	int32_t * lv=frameStack->lv_stack+1;
	int32_t * opstack=frameStack->lv_stack;
	int lv_stack_capacity=frameStack->lv_stack_capacity;
	int stackpos=stackoffset;
	uint8_t * code=cF->code;
	Constant ** cpool=cF->cpool;
	int32_t i1, i2, i3, i4;
	int64_t l1,l2;
	float f1,f2;
	double d1,d2;
	uint32_t oref;
	uint16_t index;
	Constant *c1, *c2, *c3;
	Class * class;
	ClassInstance * ci;
	Field * f;
	
	//The Holy Grail
	mainLoop:
	do {
#ifdef DEBUGGER
	if(verbose){
		fprintf(stderr, "stackpos: %d\n",stackpos);
		printFrame(cF, frameStack->lv_stack, stackpos);
	}
	if(print_ops){
		fprintf(stderr,"%d %s\n",pc,OP_toString(code[pc]));
	}
	steps--;
	bool brk=true;
	if(stepout){
		if(frameStack->pos>=stepout){
			brk=false;
		}
		else
			stepout=0;
	}
	if(steps>0)
		brk=false;
	if(brk){
		char * dcmd=readLine();
		switch(dcmd[0]){
			case 'r':
				//run 1024 steps
				steps=1024;
				break;
			case 'f':
				//print state
				{
					verbose=!verbose;
				}
				break;
			case 'o':
				//toggle bytecode instructions printing
				print_ops=!print_ops;
				continue;
			case 't':
				//print stacktrace
				{
				int size=getStackSize(frameStack);
				for(int i=0;i<size;++i){
					Frame * f=getFrame(frameStack, i);
					fprintf(stderr,"%*s", i-1, "");
					printFrame(f, frameStack->lv_stack, f->stackpos);
				}
				}
				continue;;
			case 'u':
				stepout=frameStack->pos;
				break;
			case 'g':
				removeGarbage();
				continue;
			case 'q':
				exit(0);
				return 0;
		}
	}
#endif
		switch (code[pc]){
			case NOP:
				pc += 1;
				continue;
			case ACONST_NULL:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ICONST_M1:
				opstack[stackpos++]=-1;
				pc += 1;
				continue;
			case ICONST_0:
				opstack[stackpos++]=0;
				pc += 1;
				continue;
			case ICONST_1:
				opstack[stackpos++]=1;
				pc += 1;
				continue;
			case ICONST_2:
				opstack[stackpos++]=2;
				pc += 1;
				continue;
			case ICONST_3:
				opstack[stackpos++]=3;
				pc += 1;
				continue;
			case ICONST_4:
				opstack[stackpos++]=4;
				pc += 1;
				continue;
			case ICONST_5:
				opstack[stackpos++]=5;
				pc += 1;
				continue;
			case LCONST_0:
				*(int64_t*)(opstack+stackpos)=0;
				stackpos+=2;
				pc += 1;
				continue;
			case LCONST_1:
				*(int64_t*)(opstack+stackpos)=1;
				stackpos+=2;
				pc += 1;
				continue;
			case FCONST_0:
				*(float*)(opstack+stackpos)=0.0f;
				++stackpos;
				pc += 1;
				continue;
			case FCONST_1:
				*(float*)(opstack+stackpos)=1.0f;
				++stackpos;
				pc += 1;
				continue;
			case FCONST_2:
				*(float*)(opstack+stackpos)=2.0f;
				++stackpos;
				pc += 1;
				continue;
			case DCONST_0:
				*(double*)(opstack+stackpos)=0.0;
				stackpos+=2;
				pc += 1;
				continue;
			case DCONST_1:
				*(double*)(opstack+stackpos)=1.0;
				stackpos+=2;
				pc += 1;
				continue;
			case BIPUSH:
				opstack[stackpos++]=(int8_t) code[pc + 1];
				pc += 2;
				continue;
			case SIPUSH:
				opstack[stackpos++]=(int16_t)*((uint16_t*)(code+pc+1));
				pc += 3;
				continue;
			case LDC:
				{
					//Class_LDC(cpool, code[pc+1]);
					c1=cpool[code[pc+1]];
					switch(c1->tag){
							default:
									fprintf(stderr, "Class_LDC: unknown constant type. index: %d, tag: %d\n", code[pc+1], c1->tag);
									fprintf(stderr, "index1: %d index2: %d\n",c1->index1, c1->index2);
									exit(-1);
									return -1;
							case CInteger:
							case CFloat:
								code[pc]=_LDC;
									break;
							case CString:
									code[pc]=_LDC_OBJECT;
									if(c1->value!=NULL)continue;
									c1->value=malloc(4);
									cF->pc=pc;
									cF->stackpos=stackpos;
									*(int32_t*)(c1->value)=createStringConstant(frameStack, cpool[c1->index1]->str);
									break;
							case CClass:
									code[pc]=_LDC_OBJECT;
									if(c1->value!=NULL)continue;
									//fprintln(stderr, cpool[c1->index1]->str);
									cF->pc=pc;
									cF->stackpos=stackpos;
									class = getClass(cpool[c1->index1]->str, false);
									i1=getClassClassInstanceIndex(jniEnv, class);
									//fprintf(stderr, "orefIndex: %d\n", i1);
									c1->value=malloc(4);
									*(int32_t*)(c1->value)=i1;
									//fprintf(stderr, "LDC_OBJECT: %d\n", i1);
									break;
					}
				}
				continue;
			case _LDC:
				{
					Constant * c=cpool[code[pc+1]];
					opstack[stackpos++]=*(int32_t*)c->value;
				}
				pc += 2;
				continue;
			case _LDC_OBJECT:
				{
					Constant * c=cpool[code[pc+1]];
					opstack[stackpos++]=static_refs[*(int32_t*)c->value];
				}
				pc += 2;
				continue;
			case LDC_W:
				{
					index=*((uint16_t*)(code+pc+1));
					//Class_LDC(cpool, index);
					//code[pc]=_LDC_W;
					c1=cpool[index];
					switch(c1->tag){
							default:
									fprintf(stderr, "Class_LDC: unknown constant type. index: %d, tag: %d\n",index, c1->tag);
									fprintf(stderr, "index1: %d index2: %d\n",c1->index1, c1->index2);
									exit(0);
									return -1;
							case CInteger:
							case CFloat:
								code[pc]=_LDC_W;
									break;
							case CString:
									code[pc]=_LDC_W_OBJECT;
									if(c1->value!=NULL)continue;
									c1->value=malloc(4);
									*(int32_t*)(c1->value)=createStringConstant(frameStack, cpool[c1->index1]->str);
									//fprintf(stderr, "LDC String: ref: %d\n",*(int32_t*)c1->value);
									break;
							case CClass:
									code[pc]=_LDC_OBJECT;
									if(c1->value!=NULL)continue;
									//fprintln(stderr, cpool[c1->index1]->str);
									class = getClass(cpool[c1->index1]->str, false);
									i1=getClassClassInstanceIndex(jniEnv, class);
									//fprintf(stderr, "orefIndex: %d\n", i1);
									c1->value=malloc(4);
									*(int32_t*)(c1->value)=i1;
									break;
					}
				}
				continue;
			case _LDC_W:
				{
					index=*((uint16_t*)(code+pc+1));
					Constant * c=cpool[index];
					opstack[stackpos++]=*(int32_t*)c->value;
				}
				pc += 3;
				continue;
			case _LDC_W_OBJECT:
				{
					index=*((uint16_t*)(code+pc+1));
					Constant * c=cpool[index];
					opstack[stackpos++]=static_refs[*(int32_t*)c->value];
				}
				pc += 3;
				continue;
			case LDC2_W:
				{
					index=*((uint16_t*)(code+pc+1));
					Constant * c=cpool[index];
					*(int64_t*)(opstack+stackpos)=*(int64_t*)c->value;
					stackpos+=2;
				}
				pc += 3;
				continue;
			case ILOAD:
				opstack[stackpos++]=lv[code[pc + 1]];
				pc += 2;
				continue;
			case LLOAD:
				*(int64_t*)(opstack+stackpos)=*(int64_t*)(lv+code[pc + 1]);
				stackpos+=2;
				pc += 2;
				continue;
			case FLOAD:
				*(float*)(opstack+stackpos)=*(float*)(lv+code[pc + 1]);
				++stackpos;
				pc += 2;
				continue;
			case DLOAD:
				*(double*)(opstack+stackpos)=*(double*)(lv+code[pc + 1]);
				stackpos+=2;
				pc += 2;
				continue;
			case ALOAD:
				opstack[stackpos++]=lv[(int) code[pc + 1]];
				pc += 2;
				continue;
			case ILOAD_0:
				opstack[stackpos++]=lv[0];
				pc += 1;
				continue;
			case ILOAD_1:
				opstack[stackpos++]=lv[1];
				pc += 1;
				continue;
			case ILOAD_2:
				opstack[stackpos++]=lv[2];
				pc += 1;
				continue;
			case ILOAD_3:
				opstack[stackpos++]=lv[3];
				pc += 1;
				continue;
			case LLOAD_0:
				*(int64_t*)(opstack+stackpos)=*(int64_t*)(lv);
				stackpos+=2;
				pc += 1;
				continue;
			case LLOAD_1:
				*(int64_t*)(opstack+stackpos)=*(int64_t*)(lv+1);
				stackpos+=2;
				pc += 1;
				continue;
			case LLOAD_2:
				*(int64_t*)(opstack+stackpos)=*(int64_t*)(lv+2);
				stackpos+=2;
				pc += 1;
				continue;
			case LLOAD_3:
				*(int64_t*)(opstack+stackpos)=*(int64_t*)(lv+3);
				stackpos+=2;
				pc += 1;
				continue;
			case FLOAD_0:
				*(float*)(opstack+stackpos)=*(float*)(lv);
				++stackpos;
				pc += 1;
				continue;
			case FLOAD_1:
				*(float*)(opstack+stackpos)=*(float*)(lv+1);
				++stackpos;
				pc += 1;
				continue;
			case FLOAD_2:
				*(float*)(opstack+stackpos)=*(float*)(lv+2);
				++stackpos;
				pc += 1;
				continue;
			case FLOAD_3:
				*(float*)(opstack+stackpos)=*(float*)(lv+3);
				++stackpos;
				pc += 1;
				continue;
			case DLOAD_0:
				*(double*)(opstack+stackpos)=*(double*)(lv);
				stackpos+=2;
				pc += 1;
				continue;
			case DLOAD_1:
				*(double*)(opstack+stackpos)=*(double*)(lv+1);
				stackpos+=2;
				pc += 1;
				continue;
			case DLOAD_2:
				*(double*)(opstack+stackpos)=*(double*)(lv+2);
				stackpos+=2;
				pc += 1;
				continue;
			case DLOAD_3:
				*(double*)(opstack+stackpos)=*(double*)(lv+3);
				stackpos+=2;
				pc += 1;
				continue;
			case ALOAD_0:
				opstack[stackpos++]=lv[0];
				pc += 1;
				continue;
			case ALOAD_1:
				opstack[stackpos++]=lv[1];
				pc += 1;
				continue;
			case ALOAD_2:
				opstack[stackpos++]=lv[2];
				pc += 1;
				continue;
			case ALOAD_3:
				opstack[stackpos++]=lv[3];
				pc += 1;
				continue;
			case IALOAD:
				--stackpos;
				oref=opstack[stackpos-1];
				if(oref==0){
					String * message=intern(_s("atempt to read from null int array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				opstack[stackpos-1]=*(int32_t*)(heap+oref+opstack[stackpos]*4);
				pc += 1;
				continue;
			case LALOAD:
				oref=opstack[stackpos-2];
				if(oref==0){
					String * message=intern(_s("atempt to read from null long array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(heap+oref+opstack[stackpos-1]*8);
				pc += 1;
				continue;
			case FALOAD:
				--stackpos;
				oref=opstack[stackpos-1];
				if(oref==0){
					String * message=intern(_s("atempt to read from null float array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(float*)(opstack+stackpos-1)=*(float*)(heap+oref+opstack[stackpos]*4);
				pc += 1;
				continue;
			case DALOAD:
				oref=opstack[stackpos-2];
				if(oref==0){
					String * message=intern(_s("atempt to read from null double array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(double*)(opstack+stackpos-2)=*(double*)(heap+oref+opstack[stackpos-1]*8);
				pc += 1;
				continue;
			case AALOAD:
				--stackpos;
				oref=opstack[stackpos-1];
				if(oref==0){
					String * message=intern(_s("atempt to read from null Object array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				opstack[stackpos-1]=*(int32_t*)(heap+oref+opstack[stackpos]*4);
				pc += 1;
				continue;
			case BALOAD:
				--stackpos;
				oref=opstack[stackpos-1];
				if(oref==0){
					String * message=intern(_s("atempt to read from null byte array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				opstack[stackpos-1]=*(int8_t*)(heap+oref+opstack[stackpos]);
				pc += 1;
				continue;
			case CALOAD:
				--stackpos;
				oref=opstack[stackpos-1];
				if(oref==0){
					String * message=intern(_s("atempt to read from null char array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				opstack[stackpos-1]=*(uint16_t*)(heap+oref+opstack[stackpos]*2);
				pc += 1;
				continue;
			case SALOAD:
				--stackpos;
				oref=opstack[stackpos-1];
				if(oref==0){
					String * message=intern(_s("atempt to read from null short array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				opstack[stackpos-1]=*(int16_t*)(heap+oref+opstack[stackpos]*2);
				pc += 1;
				continue;
			case ISTORE:
				lv[code[pc + 1]] = opstack[--stackpos];
				pc += 2;
				continue;
			case LSTORE:
				stackpos-=2;
				*(int64_t*)(lv+code[pc + 1]) = *(int64_t*)(opstack+stackpos);
				pc += 2;
				continue;
			case FSTORE:
				--stackpos;
				*(float*)(lv+code[pc + 1]) = *(float*)(opstack+stackpos);
				pc += 2;
				continue;
			case DSTORE:
				stackpos-=2;
				*(double *)(lv+code[pc + 1]) = *(double *)(opstack+stackpos);
				pc += 2;
				continue;
			case ASTORE:
				lv[code[pc + 1]] = opstack[--stackpos];
				pc += 2;
				continue;
			case ISTORE_0:
				lv[0] = opstack[--stackpos];
				pc += 1;
				continue;
			case ISTORE_1:
				lv[1] = opstack[--stackpos];
				pc += 1;
				continue;
			case ISTORE_2:
				lv[2] = opstack[--stackpos];
				pc += 1;
				continue;
			case ISTORE_3:
				lv[3] = opstack[--stackpos];
				pc += 1;
				continue;
			case LSTORE_0:
				stackpos-=2;
				*(int64_t*)(lv) = *(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case LSTORE_1:
				stackpos-=2;
				*(int64_t*)(lv+1) = *(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case LSTORE_2:
				stackpos-=2;
				*(int64_t*)(lv+2) = *(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case LSTORE_3:
				stackpos-=2;
				*(int64_t*)(lv+3) = *(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case FSTORE_0:
				--stackpos;
				*(float*)(lv) = *(float*)(opstack+stackpos);
				pc += 1;
				continue;
			case FSTORE_1:
				--stackpos;
				*(float*)(lv+1) = *(float*)(opstack+stackpos);
				pc += 1;
				continue;
			case FSTORE_2:
				--stackpos;
				*(float*)(lv+2) = *(float*)(opstack+stackpos);
				pc += 1;
				continue;
			case FSTORE_3:
				--stackpos;
				*(float*)(lv+3) = *(float*)(opstack+stackpos);
				pc += 1;
				continue;
			case DSTORE_0:
				stackpos-=2;
				*(double*)(lv) = *(double*)(opstack+stackpos);
				pc += 1;
				continue;
			case DSTORE_1:
				stackpos-=2;
				*(double*)(lv+1) = *(double*)(opstack+stackpos);
				pc += 1;
				continue;
			case DSTORE_2:
				stackpos-=2;
				*(double*)(lv+2) = *(double*)(opstack+stackpos);
				pc += 1;
				continue;
			case DSTORE_3:
				stackpos-=2;
				*(double*)(lv+3) = *(double*)(opstack+stackpos);
				pc += 1;
				continue;
			case ASTORE_0:
				lv[0] = opstack[--stackpos];
				pc += 1;
				continue;
			case ASTORE_1:
				lv[1] = opstack[--stackpos];
				pc += 1;
				continue;
			case ASTORE_2:
				lv[2] = opstack[--stackpos];
				pc += 1;
				continue;
			case ASTORE_3:
				lv[3] = opstack[--stackpos];
				pc += 1;
				continue;
			case IASTORE:
				stackpos-=3;
				oref=opstack[stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to write to null int array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(int32_t*)(heap+oref+opstack[stackpos+1]*4)=opstack[stackpos+2];
				pc += 1;
				continue;
			case LASTORE:
				stackpos-=4;
				oref=opstack[stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to write to null long array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(int64_t*)(heap+oref+opstack[stackpos+1]*8)=*(int64_t*)(opstack+stackpos+2);
				pc += 1;
				continue;
			case FASTORE:
				stackpos-=3;
				oref=opstack[stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to write to null float array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(float*)(heap+oref+opstack[stackpos+1]*4)=*(float*)(opstack+stackpos+2);
				pc += 1;
				continue;
			case DASTORE:
				oref=opstack[stackpos-4];
				if(oref==0){
					String * message=intern(_s("atempt to write to null double array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(double*)(heap+oref+opstack[stackpos-3]*8)=*(double*)(opstack+stackpos-2);
				stackpos-=4;
				pc += 1;
				continue;
			case AASTORE:
				stackpos-=3;
				oref=opstack[stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to write to null Object array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(int32_t*)(heap+oref+opstack[stackpos+1]*4)=opstack[stackpos+2];
				pc += 1;
				continue;
			case BASTORE:
				stackpos-=3;
				oref=opstack[stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to write to null byte array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(int8_t*)(heap+oref+opstack[stackpos+1])=(int8_t)opstack[stackpos+2];
				pc += 1;
				continue;
			case CASTORE:
				stackpos-=3;
				oref=opstack[stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to write to null char array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(uint16_t*)(heap+oref+opstack[stackpos+1]*2)=(uint16_t)opstack[stackpos+2];
				pc += 1;
				continue;
			case SASTORE:
				stackpos-=3;
				oref=opstack[stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to write to null short array"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
				}
				*(int16_t*)(heap+oref+opstack[stackpos+1]*2)=(int16_t)opstack[stackpos+2];
				pc += 1;
				continue;
			case POP:
				--stackpos;
				pc += 1;
				continue;
			case POP2:
				stackpos-=2;
				pc += 1;
				continue;
			case DUP:
				opstack[stackpos]=opstack[stackpos-1];
				++stackpos;
				pc += 1;
				continue;
			case DUP_X1:
				oref = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				opstack[stackpos-2]=oref;
				opstack[stackpos-1]=i1;
				opstack[stackpos]=oref;
				++stackpos;
				pc += 1;
				continue;
			case DUP_X2:
				i1 = opstack[stackpos-1];
				i2 = opstack[stackpos-2];
				i3 = opstack[stackpos-3];
				opstack[stackpos-3]=i1;
				opstack[stackpos-2]=i3;
				opstack[stackpos-1]=i2;
				opstack[stackpos]=i1;
				++stackpos;
				pc += 1;
				continue;
			case DUP2:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				opstack[stackpos-2]=i1;
				opstack[stackpos-1]=i2;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case DUP2_X1:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				i3 = opstack[stackpos-3];
				opstack[stackpos-3]=i1;
				opstack[stackpos-2]=i2;
				opstack[stackpos-1]=i3;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case DUP2_X2:
				i2 = opstack[stackpos-1];
				i1 = opstack[stackpos-2];
				i4 = opstack[stackpos-3];
				i3 = opstack[stackpos-4];
				opstack[stackpos-4]=i1;
				opstack[stackpos-3]=i2;
				opstack[stackpos-2]=i3;
				opstack[stackpos-1]=i4;
				opstack[stackpos]=i1;
				opstack[stackpos+1]=i2;
				stackpos+=2;
				pc += 1;
				continue;
			case SWAP:
				i1=opstack[stackpos-1];
				i2=opstack[stackpos-2];
				opstack[stackpos-2]=i1;
				opstack[stackpos-1]=i2;
				pc += 1;
				continue;
			case IADD:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos]+opstack[stackpos-1];
				pc += 1;
				continue;
			case LADD:
				stackpos-=2;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2)+*(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case FADD:
				--stackpos;
				*(float*)(opstack+stackpos-1)=*(float*)(opstack+stackpos-1)+*(float*)(opstack+stackpos);
				pc += 1;
				continue;
			case DADD:
				stackpos-=2;
				*(double*)(opstack+stackpos-2)=*(double*)(opstack+stackpos-2)+*(double*)(opstack+stackpos);
				pc += 1;
				continue;
			case ISUB:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos-1]-opstack[stackpos];
				pc += 1;
				continue;
			case LSUB:
				stackpos-=2;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2)-*(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case FSUB:
				--stackpos;
				*(float*)(opstack+stackpos-1)=*(float*)(opstack+stackpos-1)-*(float*)(opstack+stackpos);
				pc += 1;
				continue;
			case DSUB:
				stackpos-=2;
				*(double*)(opstack+stackpos-2)=*(double*)(opstack+stackpos-2)-*(double*)(opstack+stackpos);
				pc += 1;
				continue;
			case IMUL:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos-1]*opstack[stackpos];
				pc += 1;
				continue;
			case LMUL:
				stackpos-=2;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2) * *(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case FMUL:
				--stackpos;
				*(float*)(opstack+stackpos-1)=*(float*)(opstack+stackpos-1) * *(float*)(opstack+stackpos);
				pc += 1;
				continue;
			case DMUL:
				stackpos-=2;
				*(double*)(opstack+stackpos-2)=*(double*)(opstack+stackpos-2) * *(double*)(opstack+stackpos);
				pc += 1;
				continue;
			case IDIV:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos-1]/opstack[stackpos];
				pc += 1;
				continue;
			case LDIV:
				stackpos-=2;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2) / *(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case FDIV:
				--stackpos;
				*(float*)(opstack+stackpos-1)=*(float*)(opstack+stackpos-1) / *(float*)(opstack+stackpos);
				pc += 1;
				continue;
			case DDIV:
				stackpos-=2;
				*(double*)(opstack+stackpos-2)=*(double*)(opstack+stackpos-2) / *(double*)(opstack+stackpos);
				pc += 1;
				continue;
			case IREM:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos-1]%opstack[stackpos];
				pc += 1;
				continue;
			case LREM:
				stackpos-=2;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2) % *(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case FREM:
				--stackpos;
				*(float*)(opstack+stackpos-1)=fmodf(*(float*)(opstack+stackpos-1), *(float*)(opstack+stackpos));
				pc += 1;
				continue;
			case DREM:
				stackpos-=2;
				*(double*)(opstack+stackpos-2)=fmodl(*(double*)(opstack+stackpos-2), *(double*)(opstack+stackpos));
				pc += 1;
				continue;
			case INEG:
				opstack[stackpos-1]=-opstack[stackpos-1];
				pc += 1;
				continue;
			case LNEG:
				*(int64_t*)(opstack+stackpos-2)= -*(int64_t*)(opstack+stackpos-2);
				pc += 1;
				continue;
			case FNEG:
				*(float*)(opstack+stackpos-1)=-*(float*)(opstack+stackpos-1);
				pc += 1;
				continue;
			case DNEG:
				*(double*)(opstack+stackpos-2)= -*(double*)(opstack+stackpos-2);
				pc += 1;
				continue;
			case ISHL:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos-1]<<opstack[stackpos];
				pc += 1;
				continue;
			case LSHL:
				--stackpos;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2)<<*(int32_t*)(opstack+stackpos);
				//opstack[stackpos-2]=opstack[stackpos-2].longValue() << opstack[stackpos].intValue();
				//opstack[stackpos-1]=null;
				pc += 1;
				continue;
			case ISHR:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos-1]>>opstack[stackpos];
				//opstack[stackpos-1]=opstack[stackpos-1].intValue() >> opstack[stackpos].intValue();
				pc += 1;
				continue;
			case LSHR:
				--stackpos;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2)>>*(int32_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case IUSHR:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos-1]>>opstack[stackpos];
				pc += 1;
				continue;
			case LUSHR:
				--stackpos;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2)>>*(int32_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case IAND:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos]&opstack[stackpos-1];
				pc += 1;
				continue;
			case LAND:
				stackpos-=2;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2)&*(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case IOR:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos]|opstack[stackpos-1];
				pc += 1;
				continue;
			case LOR:
				stackpos-=2;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2)|*(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case IXOR:
				--stackpos;
				opstack[stackpos-1]=opstack[stackpos]^opstack[stackpos-1];
				pc += 1;
				continue;
			case LXOR:
				stackpos-=2;
				*(int64_t*)(opstack+stackpos-2)=*(int64_t*)(opstack+stackpos-2)^*(int64_t*)(opstack+stackpos);
				pc += 1;
				continue;
			case IINC:
				i1=code[pc + 1];
				lv[i1] = lv[i1] + (int8_t)code[pc + 2];
				pc += 3;
				continue;
			case I2L:
				*(int64_t*)(opstack+stackpos-1)=(int64_t)opstack[stackpos-1];
				++stackpos;
				pc += 1;
				continue;
			case I2F:
				*(float*)(opstack+stackpos-1)=(float)opstack[stackpos-1];
				pc += 1;
				continue;
			case I2D:
				*(double*)(opstack+stackpos-1)=(double)opstack[stackpos-1];
				++stackpos;
				pc += 1;
				continue;
			case L2I:
				--stackpos;
				*(int32_t*)(opstack+stackpos-1)=(int32_t)*(int64_t*)(opstack+stackpos-1);
				pc += 1;
				continue;
			case L2F:
				--stackpos;
				*(float*)(opstack+stackpos-1)=(float)*(int64_t*)(opstack+stackpos-1);
				pc += 1;
				continue;
			case L2D:
				*(double*)(opstack+stackpos-2)=(double)*(int64_t*)(opstack+stackpos-2);
				pc += 1;
				continue;
			case F2I:
				*(int32_t*)(opstack+stackpos-1)=(int32_t)*(float*)(opstack+stackpos-1);
				pc += 1;
				continue;
			case F2L:
				*(int64_t*)(opstack+stackpos-1)=(int64_t)*(float*)(opstack+stackpos-1);
				++stackpos;
				pc += 1;
				continue;
			case F2D:
				*(double*)(opstack+stackpos-1)=(double)*(float*)(opstack+stackpos-1);
				++stackpos;
				pc += 1;
				continue;
			case D2I:
				--stackpos;
				*(int32_t*)(opstack+stackpos-1)=(int32_t)*(double*)(opstack+stackpos-1);
				pc += 1;
				continue;
			case D2L:
				*(int64_t*)(opstack+stackpos-2)=(int64_t)*(double*)(opstack+stackpos-2);
				pc += 1;
				continue;
			case D2F:
				--stackpos;
				*(float*)(opstack+stackpos-1)=(float)*(double*)(opstack+stackpos-1);
				pc += 1;
				continue;
			case I2B:
				opstack[stackpos-1]=(int8_t)opstack[stackpos-1];
				pc += 1;
				continue;
			case I2C:
				opstack[stackpos-1]=(uint16_t)opstack[stackpos-1];
				//*(uint16_t*)(opstack+stackpos-1)=(uint16_t)opstack[stackpos-1];
				pc += 1;
				continue;
			case I2S:
				opstack[stackpos-1]=(int16_t)opstack[stackpos-1];
				pc += 1;
				continue;
			case LCMP:
				l1 = (int64_t) *(int64_t*)(opstack+stackpos-2);
				l2 = (int64_t) *(int64_t*)(opstack+stackpos-4);
				if (l2 == l1) {
					opstack[stackpos-4]=0;
				} else if (l2 > l1) {
					opstack[stackpos-4]=1;
				} else
					opstack[stackpos-4]=-1;
				stackpos-=3;
				pc += 1;
				continue;
			case FCMPL:
				f1 = (float) opstack[--stackpos];
				f2 = (float) opstack[--stackpos];
				if (f2 == f1) {
					opstack[stackpos++]=0;
				} else if (f2 > f1) {
					opstack[stackpos++]=1;
				} else if (f2 < f1)
					opstack[stackpos++]=-1;
				else
					opstack[stackpos++]=-1;
				pc += 1;
				continue;
			case FCMPG:
				f1 = *(float*)(opstack+stackpos-1);
				f2 = *(float*)(opstack+stackpos-2);
				stackpos-=2;
				if (f2 == f1) {
					opstack[stackpos++]=0;
				} else if (f2 > f1) {
					opstack[stackpos++]=1;
				} else if (f2 < f1)
					opstack[stackpos++]=-1;
				else
					opstack[stackpos++]=1;
				pc += 1;
				continue;
			case DCMPL:
				d1 = *(double*)(opstack+stackpos-2);
				d2 = *(double*)(opstack+stackpos-4);
				stackpos-=4;
				if (d2 == d1) {
					opstack[stackpos++]=0;
				} else if (d2 > d1) {
					opstack[stackpos++]=1;
				} else if (d2 < d1)
					opstack[stackpos++]=-1;
				else
					opstack[stackpos++]=-1;
				pc += 1;
				continue;
			case DCMPG:
				d1 = *(double*)(opstack+stackpos-2);
				d2 = *(double*)(opstack+stackpos-4);
				stackpos-=4;
				if (d2 == d1) {
					opstack[stackpos++]=0;
				} else if (d2 > d1) {
					opstack[stackpos++]=1;
				} else if (d2 < d1)
					opstack[stackpos++]=-1;
				else
					opstack[stackpos++]=1;
				pc += 1;
				continue;
			case IFEQ:
				if (opstack[--stackpos] == 0)
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IFNE:
				if (opstack[--stackpos] != 0)
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IFLT:
				if ((int) opstack[--stackpos] < 0)
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IFGE:
				if ((int) opstack[--stackpos] >= 0)
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IFGT:
				if ((int) opstack[--stackpos] > 0)
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IFLE:
				if ((int) opstack[--stackpos] <= 0)
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IF_ICMPEQ:
				stackpos-=2;
				if ((int) opstack[stackpos+1] == (int) opstack[stackpos])
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IF_ICMPNE:
				stackpos-=2;
				if ((int) opstack[stackpos+1] != (int) opstack[stackpos])
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IF_ICMPLT:
				stackpos-=2;
				if ((int) opstack[stackpos+1] > (int) opstack[stackpos])
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IF_ICMPGE:
				stackpos-=2;
				if ((int) opstack[stackpos+1] <= (int) opstack[stackpos])
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IF_ICMPGT:
				stackpos-=2;
				if ((int) opstack[stackpos+1] < (int) opstack[stackpos])
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IF_ICMPLE:
				stackpos-=2;
				if ((int) opstack[stackpos+1] >= (int) opstack[stackpos])
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IF_ACMPEQ:
				stackpos-=2;
				if ((int) opstack[stackpos+1] == (int) opstack[stackpos])
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IF_ACMPNE:
				stackpos-=2;
				if ((int) opstack[stackpos+1] != (int) opstack[stackpos])
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case GOTO:
				pc+=*((short*)(code+pc+1));
				continue;
			case JSR:
				opstack[stackpos++]=pc + 3;
				//pc += i(code[pc + 1], code[pc + 2]);
				continue;
			case RET:
				pc =opstack[--stackpos];
				continue;
			case TABLESWITCH:
				i1 = opstack[--stackpos];
				int pc2 = (pc + 4) & 0xfffffffc;
				int dfp = pc2;
				pc2 += 4;
				int32_t low= *(int32_t*)(code+pc2);
				pc2 += 4;
				int32_t high= *(int32_t*)(code+pc2);
				pc2 += 4;
				if (i1 < low || i1 > high) {  // if its less than <low> or greater than <high>,
					// branch to default
					pc += *(int32_t*)(code+dfp);
				} else {
					pc2 += (i1 - low) * 4;
					pc += *(int32_t*)(code+pc2);
				}
				continue;
			case LOOKUPSWITCH:
				i1 = opstack[--stackpos];
				pc2 = (pc + 4) & 0xfffffffc;
				int32_t default_offset =*(int32_t*)(code+pc2);
				pc2+=4;
				i2=*(int32_t*)(code+pc2);
				pc2+=4;
				for (i3 = 0; i3 < i2; ++i3) {
					int32_t key=*(int32_t*)(code+pc2);
					pc2+=4;

					if (key == i1) {
						pc += *(int32_t*)(code+pc2);
						goto mainLoop;
					} else
						pc2 += 4;
				}
				pc += default_offset;
				continue;
			case IRETURN:
				++pc;
				i1=opstack[stackpos-1];
				//free(cF);
				cF=popFrameAndPeek(frameStack);
				if(cF==NULL) return 0;
				cpool=cF->cpool;
				code=cF->code;
				stackpos=cF->stackpos;
				opstack[stackpos++]=i1;
				pc=cF->pc;
				lv=cF->lv;

				continue;
			case LRETURN:
				++pc;
				l1=*(int64_t*)(opstack+stackpos-2);
				//free(cF);
				cF=popFrameAndPeek(frameStack);
				if(cF==NULL) return 0;
				cpool=cF->cpool;
				code=cF->code;
				stackpos=cF->stackpos;
				*(int64_t*)(opstack+stackpos)=l1;
				stackpos+=2;
				pc=cF->pc;
				lv=cF->lv;
				continue;
			case FRETURN:
				++pc;
				f1=*(float*)(opstack+stackpos-1);
				//free(cF);
				cF=popFrameAndPeek(frameStack);
				if(cF==NULL) return 0;
				cpool=cF->cpool;
				code=cF->code;
				stackpos=cF->stackpos;
				*(float*)(opstack+stackpos)=f1;
				++stackpos;
				pc=cF->pc;
				lv=cF->lv;
				continue;
			case DRETURN:
				++pc;
				d1=*(double*)(opstack+stackpos-2);
				//free(cF);
				cF=popFrameAndPeek(frameStack);
				if(cF==NULL) return 0;
				cpool=cF->cpool;
				code=cF->code;
				stackpos=cF->stackpos;
				*(double*)(opstack+stackpos)=d1;
				stackpos+=2;
				pc=cF->pc;
				lv=cF->lv;
				continue;
			case ARETURN:
				++pc;
				i1=opstack[stackpos-1];
				//free(cF);
				cF=popFrameAndPeek(frameStack);
				if(cF==NULL) return 0;
				cpool=cF->cpool;
				code=cF->code;
				stackpos=cF->stackpos;
				opstack[stackpos++]=i1;
				pc=cF->pc;
				lv=cF->lv;
				continue;
			case RETURN:
				pc += 1;
				//free(cF);
				cF = popFrameAndPeek(frameStack);
				if(cF==NULL) return 0;
				stackpos=cF->stackpos;
				cpool=cF->cpool;
				lv=cF->lv;
				code=cF->code;
				pc=cF->pc;
				continue;
			case GETSTATIC:
				index=*((uint16_t*)(code+pc+1));
				c1=cpool[index];
				c2=cpool[c1->index2];
				c2=cpool[c2->index1];

				c3=cpool[c1->index1];
				c3=cpool[c3->index1];
				cF->pc=pc;
				cF->stackpos=stackpos;
				class=getClass(c3->str, true);
				f = HashMap_get(class->ft,c2->str);
				//c1->index1=f->index;
				*((uint16_t*)(code+pc+1))=f->index;
				switch(f->type->str[0]){
					case 'L':
					case '[':
						code[pc]=GETSTATICREF;
						break;
					default:
						code[pc]=_GETSTATIC;
						break;
					case 'J':
					case 'D':
						code[pc]=GETSTATIC64;
						break;

				}
				//c1->value=f->value;
				//c1->tag=0;
				//goto getstatic;
				continue;
			case _GETSTATIC:
				index=*((uint16_t*)(code+pc+1));
				opstack[stackpos++]=static_fields[index];
				pc += 3;
				continue;
			case GETSTATICREF:
				index=*((uint16_t*)(code+pc+1));
				pc += 3;
				opstack[stackpos++]=static_refs[index];
				continue;
			case GETSTATIC64:
				index=*((uint16_t*)(code+pc+1));
				*(int64_t*)(opstack+stackpos)=*(int64_t*)(static_fields+index);
				stackpos+=2;
				pc += 3;
				continue;
			case PUTSTATIC:
				index=*((uint16_t*)(code+pc+1));
				c1=cpool[index];
				c2=cpool[c1->index2];
				c2=cpool[c2->index1];

				c3=cpool[c1->index1];
				c3=cpool[c3->index1];
				cF->pc=pc;
				cF->stackpos=stackpos;
				class=getClass(c3->str, true);
				f = HashMap_get(class->ft,c2->str);
				//c1->index1=f->index;
				*((uint16_t*)(code+pc+1))=f->index;
				switch(f->type->str[0]){
					case 'L':
					case '[':
						code[pc]=PUTSTATICREF;
						break;
					default:
						code[pc]=_PUTSTATIC;
						break;
					case 'J':
					case 'D':
						code[pc]=PUTSTATIC64;
						break;

				}

				//c1->value=f->value;
				//c1->tag=0;
				//*(int*)c1->value=opstack[--stackpos];
				continue;
			case PUTSTATICREF:
				index=*((uint16_t*)(code+pc+1));
				stackpos-=1;
				*(int32_t*)(static_refs+index)=*(int32_t*)(opstack+stackpos);
				pc += 3;
				continue;
			case _PUTSTATIC:
				index=*((uint16_t*)(code+pc+1));
				stackpos-=1;
				*(int32_t*)(static_fields+index)=*(int32_t*)(opstack+stackpos);
				pc += 3;
				continue;
			case PUTSTATIC64:
				index=*((uint16_t*)(code+pc+1));
				stackpos-=2;
				*(int64_t*)(static_fields+index)=*(int64_t*)(opstack+stackpos);
				pc += 3;
				continue;
			case GETFIELD:
				//ci=heap+oref-sizeof(ClassInstance);
				index=*((uint16_t*)(code+pc+1));
				c1=cpool[index];
				c2=cpool[c1->index2];
				c2=cpool[c2->index1];

				c3=cpool[c1->index1];
				c3=cpool[c3->index1];
				cF->pc=pc;
				cF->stackpos=stackpos;
				class=getClass(c3->str, true);
				f = resolveField(class,c2->str);
				if(f==NULL){
					fprintf(stderr,"field not found:");
					fprint(stderr, c3->str);
					fprint(stderr,c2->str);
					fprintf(stderr, "\n");
				}

				*((uint16_t*)(code+pc+1))=f->index;
				switch(f->type->str[0]){
					default:
						code[pc]=_GETFIELD;
						break;
					case 'J':
					case 'D':
						code[pc]=GETFIELD64;
						continue;
				}
				continue;
			case _GETFIELD:
				index=*((uint16_t*)(code+pc+1));
				oref = opstack[--stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to read field on null Object"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
					cpool=cF->cpool;
					code=cF->code;
					stackpos=cF->stackpos;
					pc=cF->pc;
					lv=cF->lv;
					continue;
				}
				//fprintf(stderr,"get field index is: %d value: %d\n", index, *(int32_t*)(heap+oref+index));
				//dumpInstance(oref);
				opstack[stackpos++]=*(int32_t*)(heap+oref+index*4);
				/*
				ci=heap+oref;
				opstack[stackpos++]=ci->fields[index];
				*/
				pc += 3;
				continue;
			case GETFIELD64:
				index=*((uint16_t*)(code+pc+1));
				//field is linked
				oref = opstack[--stackpos];
				*(int64_t*)(opstack+stackpos)=*(int64_t*)(heap+oref+index*4);
				/*
				ci=heap+oref;
				*(int64_t*)(opstack+stackpos)=*(int64_t*)(ci->fields+index);
				*/
				stackpos+=2;
				pc += 3;
				continue;
			case PUTFIELD:
				index=*((uint16_t*)(code+pc+1));
				c1=cpool[index];
				c2=cpool[c1->index2];
				c2=cpool[c2->index1];

				c3=cpool[c1->index1];
				c3=cpool[c3->index1];
				cF->pc=pc;
				cF->stackpos=stackpos;
				class=getClass(c3->str, true);
				//f = HashMap_get(class->ft,c2->str);
				f = resolveField(class,c2->str);
				if(f==NULL){
					printf("field not found:");
					print(c3->str);
					print(c2->str);
					printf("\n");
				}
				*((uint16_t*)(code+pc+1))=f->index;
				switch(f->type->str[0]){
					default:
						code[pc]=_PUTFIELD;
						break;
					case 'J':
					case 'D':
						code[pc]=PUTFIELD64;
						continue;
				}
				continue;
			case _PUTFIELD:
				index=*((uint16_t*)(code+pc+1));
				//field is linked
				i1 = opstack[--stackpos];
				oref = opstack[--stackpos];
				if(oref==0){
					String * message=intern(_s("atempt to write field on null Object"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
					cpool=cF->cpool;
					code=cF->code;
					stackpos=cF->stackpos;
					pc=cF->pc;
					lv=cF->lv;
					continue;
				}
				*(int32_t*)(heap+oref+index*4)=i1;
				//fprintf(stderr, "PUTFIELD: value: %d, index: %d\n",i1, index);
				//dumpInstance(oref);
				/*
				ci=heap+oref;
				ci->fields[index]=i1;
				*/
				pc += 3;
				continue;
			case PUTFIELD64:
				index=*((uint16_t*)(code+pc+1));
				//field is linked
				stackpos-=2;
				l1 = *(int64_t*)(opstack+stackpos);
				oref = opstack[--stackpos];
				/*
				ci=heap+oref;
				*(int64_t*)(ci->fields+index)=l1;
				*/
				*(int64_t*)(heap+oref+index*4)=l1;
				pc += 3;
				continue;
			case INVOKEVIRTUAL:
				index=*((uint16_t*)(code+pc+1));
				//pc += 3;
				c1=cpool[index];
				switch(c1->tag){
					case CInvokeVirtual:
						code[pc]=_INVOKEVIRTUAL;
						continue;
					default:
						break;
				}
				
				MethodHead * mh = c1->data;
				class=getClass(mh->classname, false);
				m=HashMap_get(class->vmt, mh->signature);
				if(m==NULL){
					fprintf(stderr, "could not resolve method:");
					fprint(stderr, mh->classname);
					fprintf(stderr, ".");
					fprintln(stderr, mh->signature);
				}
				c1->tag=CInvokeVirtual;
				c1->data=m;
				code[pc]=_INVOKEVIRTUAL;
				free(mh);
				continue;
			case _INVOKEVIRTUAL:
				index=*((uint16_t*)(code+pc+1));
				pc += 3;
				c1=cpool[index];
				//method is resolved
				m=(Method*)c1->data;
				uint16_t argsLength=m->argsLength;
				stackpos-=(argsLength);
				lv=opstack+stackpos;
				cF->stackpos=stackpos;
				cF->pc=pc;
				oref=opstack[stackpos];
				//fprintln(stderr, m->signature);
		//fprintf(stderr, "eoref later: %d\n", oref);
		//dumpInstance(oref);
				if(oref==0){
					String * message=intern(_s("invokevirtual on null Object"));
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
					if(cF==NULL)
						return -1;
					cpool=cF->cpool;
					code=cF->code;
					stackpos=cF->stackpos;
					pc=cF->pc;
					lv=cF->lv;
					continue;
				}
				ci=heap+oref-sizeof(ClassInstance);
				/*
				fprintln(stderr, m->signature);
				fprintf(stderr, "oref: %d\n",oref);
				*/
				m=getMethod(ci->clazz, m->signature);
				if(m==NULL){
					fprintf(stderr,"could not resolve method: ");
					fprint(stderr, ci->clazz->name);
					fprintf(stderr,".");
					m=(Method*)c1->data;
					fprintln(stderr, m->signature);
				}
				if(m->isNative){
					if(m->nativeHandler==NULL){
						String * error;
						m->nativeHandler=resolveNativeMethod(m->clazz->name, m->name, &error);
						if(m->nativeHandler==NULL){
							cF=throwNewException(UNSATISFIEDLINKERROR, frameStack, cF, pc, error);
							deleteString(error);
							if(cF==NULL)
								return -1;
							cpool=cF->cpool;
							code=cF->code;
							stackpos=cF->stackpos;
							pc=cF->pc;
							lv=cF->lv;
							continue;
						}
					}

					cF->stackpos=stackpos;
					//stackpos+=nativeHandler(m->nativeMethodType, m->nativeHandler, lv, frameStack->lv_stack+stackpos);
					//fprintln(stderr, m->signature);
					stackpos+=jni_handler(m->nativeMethodType, m->nativeHandler, lv, frameStack->lv_stack+stackpos);
					//stackpos=cF->stackpos;
					lv=cF->lv;
					continue;
				}
				cF=newFrame(frameStack, m, lv, stackpos);
				cpool=cF->cpool;
				code=cF->code;
				pc=0;
				stackpos+=m->max_lv;
				//pushFrame(frameStack, cF);
				continue;
			case INVOKESPECIAL:
				index=*((uint16_t*)(code+pc+1));
				//pc += 3;
				c1=cpool[index];
				switch(c1->tag){
					case CInvokeSpecial:
						code[pc]=_INVOKESPECIAL;
						continue;	
					case CInvokeSpecialNative:
						code[pc]=INVOKESPECIAL_NATIVE;
						continue;	
				}
				
				mh = c1->data;
				class=getClass(mh->classname, false);
				m=HashMap_get(class->smt, mh->signature);
				if(m->isNative){
					String * error;
					m->nativeHandler=resolveNativeMethod(class->name, mh->name, &error);
					//invokeNative(lv);
					if(m->nativeHandler==NULL){
						cF=throwNewException(UNSATISFIEDLINKERROR, frameStack, cF, pc, m->signature);
						deleteString(error);
						if(cF==NULL)
							return -1;
						cpool=cF->cpool;
						code=cF->code;
						stackpos=cF->stackpos;
						pc=cF->pc;
						lv=cF->lv;
						free(mh);
						continue;
					}
					c1->tag=CInvokeSpecialNative;
					c1->data=m;
					code[pc]=INVOKESPECIAL_NATIVE;
					continue;
				}
				c1->tag=CInvokeSpecial;
				c1->data=m;
				code[pc]=_INVOKESPECIAL;
				free(mh);
				continue;
			case _INVOKESPECIAL:
				index=*((uint16_t*)(code+pc+1));
				pc += 3;
				c1=cpool[index];
				//method is resolved
				m=(Method*)c1->data;
				argsLength=m->argsLength;
				stackpos-=(argsLength);
				lv=opstack+stackpos;
				cF->stackpos=stackpos;
				cF->pc=pc;

				//fprintln(stderr, m->signature);
				cF=newFrame(frameStack, m, lv, stackpos);
				cpool=cF->cpool;
				code=cF->code;

#ifdef BYTECODEPRINTER
				fprintf(stderr, "INVOKE: ");
				fprintln(stderr, m->signature);
				fprintf(stderr, "codeptr: %p \n", code);
#endif
				pc=0;
				stackpos+=m->max_lv;
				//pushFrame(frameStack, cF);
				continue;
			case INVOKESPECIAL_NATIVE:
				index=*((uint16_t*)(code+pc+1));
				pc += 3;
				c1=cpool[index];
				//method is resolved
				m=(Method*)c1->data;
				argsLength=m->argsLength;
				stackpos-=(argsLength);
				lv=opstack+stackpos;
				cF->stackpos=stackpos;
				cF->pc=pc;
				//stackpos+=nativeHandler(m->nativeMethodType, m->nativeHandler, lv, frameStack->lv_stack+stackpos);
				//fprintln(stderr, m->signature);
				stackpos+=jni_handler(m->nativeMethodType, m->nativeHandler, lv, frameStack->lv_stack+stackpos);
				cF->stackpos=stackpos;
				//stackpos=cF->stackpos;
				lv=cF->lv;
				//clear JNI refs
				frameStack->jni_refs_count = 0;
				continue;
			case INVOKESTATIC:
				index=*((uint16_t*)(code+pc+1));
				c1=cpool[index];
				switch(c1->tag){
					case CInvokeStatic:
						//method is resolved
						code[pc]=_INVOKESTATIC;
						continue;
					case CInvokeStaticNative:
						//native method is resolved
						code[pc]=INVOKESTATIC_NATIVE;
						continue;
				}
				mh=c1->data;
				class=getClass(mh->classname, false);
				m=HashMap_get(class->smt, mh->signature);
				c1->data=m;
				if(m->isNative){
					c1->tag=CInvokeStaticNative;
					if(m->nativeHandler==NULL){
						//printf("resolve nativeHandler\n");
						//println(signature);
						String * error;
						m->nativeHandler=resolveNativeMethod(class->name, mh->name, &error);
						if(m->nativeHandler==NULL){
							cF=throwNewException(UNSATISFIEDLINKERROR, frameStack, cF, pc, error);
							deleteString(error);
							if(cF==NULL)
								return -1;
							cpool=cF->cpool;
							code=cF->code;
							stackpos=cF->stackpos;
							pc=cF->pc;
							lv=cF->lv;
							free(mh);
							continue;
						}
						code[pc]=INVOKESTATIC_NATIVE;
					}
				}
				else{
					c1->tag=CInvokeStatic;
					code[pc]=_INVOKESTATIC;
				}
				free(mh);
				continue;
			case _INVOKESTATIC:
				index=*((uint16_t*)(code+pc+1));
				pc += 3;
				c1=cpool[index];
				//method is resolved
				m=(Method*)c1->data;
				argsLength=m->argsLength;
				stackpos-=argsLength;
				lv=opstack+stackpos;
				cF->stackpos=stackpos;
				cF->pc=pc;

				cF=newFrame(frameStack, m, lv, stackpos);
				cpool=cF->cpool;
				code=cF->code;
				pc=0;
				stackpos+=m->max_lv;
				if(stackpos+m->max_stack>=lv_stack_capacity){
					growFrameStack(frameStack, stackpos+m->max_stack);
				}
					
				//pushFrame(frameStack, cF);
				continue;
			case INVOKESTATIC_NATIVE:
				index=*((uint16_t*)(code+pc+1));
				pc += 3;
				c1=cpool[index];
				//method is resolved
				m=(Method*)c1->data;
				argsLength=m->argsLength;
				stackpos-=argsLength;
				lv=opstack+stackpos;
				cF->stackpos=stackpos;
				cF->pc=pc;
				//fprintln(stderr, m->signature);
				stackpos+=jni_handler(m->nativeMethodType, m->nativeHandler, lv, frameStack->lv_stack+stackpos);
				cF->stackpos=stackpos;
				//stackpos=cF->stackpos;
				lv=cF->lv;
				//clear JNI refs
				frameStack->jni_refs_count = 0;
				continue;
			case INVOKEINTERFACE:
				index=*((uint16_t*)(code+pc+1));
invokeinterface:
				c1=cpool[index];
				if(c1->tag==0){
					//uint8_t count = code[pc + 3];
					//uint8_t zero = code[pc + 4];
					pc += 5;
					//method is resolved
					m=(Method*)c1->data;
					//fprintln(stderr, m->signature);
					uint16_t argsLength=m->argsLength;
					stackpos-=(argsLength);
					lv=opstack+stackpos;
					cF->stackpos=stackpos;
					cF->pc=pc;
					oref=opstack[stackpos];
					
					if(oref==0){
						String * message=intern(_s("invokeinterface on null Object"));
						cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, message);
						if(cF==NULL)
							return -1;
						cpool=cF->cpool;
						code=cF->code;
						stackpos=cF->stackpos;
						pc=cF->pc;
						lv=cF->lv;
						continue;
					}
					//fprintf(stderr, "oref: %d\n",oref);
					ci=heap+oref-sizeof(ClassInstance);
					//fprintln(stderr, ci->clazz->name);
					m=getMethod(ci->clazz,m->signature);
					if(m->isNative){
						//invokeNative(lv);
						continue;
					}
					cF=newFrame(frameStack, m, lv, stackpos);
					cpool=cF->cpool;
					code=cF->code;
					pc=0;
					stackpos+=m->max_lv;
					//pushFrame(frameStack, cF);
					continue;
				}
				
				mh=c1->data;
				class=getClass(mh->classname, false);
				m=HashMap_get(class->vmt, mh->signature);
				if(m==NULL){
					fprintf(stderr, "method not found:\n");
					fprint(stderr, mh->classname);
					fprintf(stderr, ".");
					fprintln(stderr, mh->signature);
					exit(-1);
				}
				c1->tag=0;
				c1->data=m;
				free(mh);
				goto invokeinterface;

				/*
				   c0=constantPool[(((code[pc+1] ) << 8) | (code[pc+2]))];
				   m=c0.m;
				   if(m==null){
				   m = cC.getMethod((((code[pc+1] ) << 8) | (code[pc+2])));
				   c0.m= (Method) m;
				   }

				   short count = code[pc + 3];
				   short zero = code[pc + 4];
				   pc += 5;
				   cF.pc = pc;
				//if(count>1) {

				Number[] mlv = new Number[count];
				for (int ix = 0; ix < count; ++ix) {
				mlv[count - ix - 1] = opstack[--stackpos];
				}

				//}
				oref = mlv[0];

				if (oref == 0) {
				$this.throwNewException("java/lang/NullPointerException", pc, "invokeinterface: " + m.signature);
				cF = stack.peek();
				opstack=cF.stack;
				stackpos=cF.stackpos;
				pc = cF.pc;
				code = cF.code;
				lv = cF.lv;
				continue;
				}
				AClass mc = Heap.get(oref).clazz;
				m = mc.getMethod(m.signature);
				nf = new Frame((Method) m);


				if (m.isNative) {
				//m.invoke(cF.stack, this);
				throw new RuntimeException("check.that");
				//continue;
				}
				nf.lv = new Number[((Method) m).getMaxLocals()];
				nf.lvpt = new Number[((Method) m).getMaxLocals()];

				char[] args = m.args;
				int l = mlv.length;
				for (int j = 0; j < l; ++j) {
				if (j > 0)
				if (args[j - 1] == 'L')
				nf.lvpt[j] = (int) mlv[j];
				nf.lv[j] = mlv[j];
				}

				cF.stackpos=stackpos;
				cF = nf;
				pc = cF.pc;
				lv = cF.lv;

				cC = m.getMyClass();
				constantPool=cC.getConstantPool();
				stack.push(cF);
				opstack=cF.stack;
				stackpos=cF.stackpos;
				code = cF.code;
				*/
				continue;
			case INVOKEDYNAMIC:
				fprintf(stderr, "invokedynamic is not implemented\n");
				/*
				   i1 =(((code[pc+1] ) << 8) | (code[pc+2]));
				   pc += 5;
				   cF.pc = pc;
				   c0 = constantPool[i1];
				   BootstrapMethod mh = cC.getMethodHandle(c0.index1);
				   c0 = constantPool[c0.index2];
				   c1 = constantPool[c0.index1];
				   Constant c2 = constantPool[c0.index2];
				//System.out.println(fname.str+ftype.str);
				oref = mh.resolve(c1.str);
				switch (mh.referenceKind) {
				case BootstrapMethod.REF_invokeVirtual:
				break;
				case BootstrapMethod.REF_invokeStatic:
				if (cF.stackpos > 0)
				--stackpos;
				//ci = (ClassInstance) Heap.get(oref);
				break;
				default:
				throw new RuntimeException("referenceKind not supported");
				}
				*/
				//throw new RuntimeException("invokedynamic not implemented");
				continue;
			case NEW:
				index=*((uint16_t*)(code+pc+1));
				c1=cpool[index];
				if(c1->data!=NULL){
					code[pc]=_NEW;
					continue;
				}
				c2=cpool[c1->index1];
#ifdef BYTECODEPRINTER
				printf("NEW: ");
				println(c2->str);
#endif
				cF->pc=pc;
				cF->stackpos=stackpos;
				c1->data=getClass(c2->str,true);
				code[pc]=_NEW;
				continue;
			case _NEW:
				index=*((uint16_t*)(code+pc+1));
				c1=cpool[index];
				oref=createClassInstance(c1->data);
				opstack[stackpos++]=oref;
#ifdef BYTECODEPRINTER
				dumpInstance(oref);
#endif
				pc += 3;
				continue;
			case NEWARRAY:
				{
					int32_t size=opstack[stackpos-1];
					cF->pc=pc;
					cF->stackpos=stackpos;
					opstack[stackpos-1]=createArrayInstance(code[pc+1], size);
					pc += 2;
				}
				continue;
			case ANEWARRAY:
				{
					int32_t size=opstack[stackpos-1];
					cF->pc=pc;
					cF->stackpos=stackpos;
					opstack[stackpos-1]=createArrayInstance(T_OBJECT, size);
					pc += 3;
				}
				continue;
			case ARRAYLENGTH:
				{
				ArrayInstance * ai=heap+opstack[stackpos-1]-sizeof(ArrayInstance);
				//fprintf(stderr, "ARRAYLENGTH: %d\n",ai->size);
				opstack[stackpos-1]=ai->size;
				pc += 1;
				}
				continue;
			case ATHROW:
				oref = opstack[stackpos-1];
				if(oref==0){
					cF=throwNewException(NULL_POINTER_EXCEPTION, frameStack, cF, pc, NULL);
				}
				else{
					frameStack->lv_stack[0]=oref;
					ci=heap+oref-sizeof(ClassInstance);
					cF=throwException(ci->clazz, frameStack, cF, pc, (frameStack->pos-1)/sizeof(Frame));
				}
				if(cF==NULL)
					return -1;
				cpool=cF->cpool;
				code=cF->code;
				stackpos=cF->stackpos;
				pc=cF->pc;
				lv=cF->lv;


				/*
				   oref = opstack[--stackpos];
				   $this.throwException(pc, (Integer) oref);
				   cF = stack.peek();
				   pc=cF.pc;
				   cC = cF.getMyClass();
				   constantPool=cC.getConstantPool();
				   opstack=cF.stack;
				   stackpos=cF.stackpos;
				   code = cF.code;
				   lv = cF.lv;
				   */
				continue;
			case CHECKCAST:
				/*
				   oref = opstack[stackpos-1];
				   if(oref==0){
				   pc+=3;
				   continue;
				   }
				   c0=constantPool[(((code[pc+1] ) << 8) | (code[pc+2]))];
				   AClass cl2= c0.clazz;
				   if(cl2==null){
				   c1 = constantPool[c0.index1];
				   String classname=c1.str;
				   if (classname.startsWith("[")) {
				   pc+=3;
				   continue;
				   }
				   cl2 = ClassLoader.getClass(classname);
				   c0.clazz=cl2;
				   }

				   cl = Heap.get(oref).clazz;
				   if (!cl.isInstance(cl2)){
				//ClassCastException
				//throw new RuntimeException("CLASSCASTEXCEPTION: " + cl.getName() + " is not " + cl2.getName());
				}
				*/
				pc += 3;
				continue;
			case INSTANCEOF:
				/*
				   oref = opstack[--stackpos];
				   if (oref == 0) {
				   opstack[stackpos++]=0;
				   pc += 3;
				   continue;
				   }
				   i1 = ((code[pc + 1]) << 8) | (code[pc + 2]);
				   cl2 = cC.getClass(i1);
				   cl = Heap.get(oref).clazz;
				   if (cl.isInstance(cl2))
				   opstack[stackpos++]=1;
				   else
				   opstack[stackpos++]=0;
				   */
				pc += 3;
				continue;
			case MONITORENTER:
				/*
				   oref = opstack[--stackpos];
				   Instance i = Heap.get(oref);
				   if (i.monitorenter($this.thread.monitor)) {
				   $this.thread._wait();
				   return 1;
				   }
				   */
				pc += 1;
				continue;
			case MONITOREXIT:
				/*
				   oref = opstack[--stackpos];
				   i = Heap.get(oref);
				   MyThread next = i.monitorexit($this.thread.monitor);
				   if (next != null){
				   JVM.notify(next);
				   }
				   */
				pc += 1;
				continue;
			case WIDE:
				pc += 1;
				switch (code[pc]) {
					case ILOAD:
						opstack[stackpos++]=lv[*(uint16_t*)(code+pc + 1)];
						pc += 3;
						continue;
					case LLOAD:
						*(int64_t*)(opstack+stackpos)=*(int64_t*)(lv+*(uint16_t*)(code+pc + 1));
						stackpos+=2;
						pc += 3;
						continue;
					case FLOAD:
						*(float*)(opstack+stackpos)=*(float*)(lv+*(uint16_t*)(code+pc + 1));
						++stackpos;
						pc += 3;
						continue;
					case DLOAD:
						*(double*)(opstack+stackpos)=*(double*)(lv+*(uint16_t*)(code+pc + 1));
						stackpos+=2;
						pc += 3;
						continue;
					case ALOAD:
						opstack[stackpos++]=lv[*(uint16_t*)(code+pc + 1)];
						pc += 3;
						continue;
					case ISTORE:
						lv[*(uint16_t*)(code+pc + 1)]=opstack[--stackpos];
						pc += 3;
						continue;
					case LSTORE:
						stackpos-=2;
						*(int64_t*)(lv+*(uint16_t*)(code+pc + 1)) = *(int64_t*)(opstack+stackpos);
						pc += 3;
						continue;
					case FSTORE:
						--stackpos;
						*(float*)(lv+*(uint16_t*)(code+pc + 1)) = *(float*)(opstack+stackpos);
						pc += 3;
						continue;
					case DSTORE:
						stackpos-=2;
						*(double*)(lv+*(uint16_t*)(code+pc + 1)) = *(double*)(opstack+stackpos);
						pc += 3;
						continue;
					case ASTORE:
						oref=opstack[--stackpos];
						index=*(uint16_t*)(code+pc + 1);
						lv[index]=oref;
						pc += 3;
						continue;
					case IINC:
						index=*(uint16_t*)(code+pc + 1);
						i1 = lv[index];
						lv[index] = i1 + *(int16_t*)(code+pc + 3);
						pc += 5;
						continue;

				}
				return -1;
				//throw new RuntimeException("wide not implememted");
				//continue;
			case MULTIANEWARRAY:
				index=*((uint16_t*)(code+pc+1));
				c1=cpool[index];
				c1=cpool[c1->index1];
				
				uint8_t dimensions=code[pc + 3];
				char type=c1->str->str[dimensions];
				int32_t * sizes = malloc(4*dimensions);
				for (uint8_t i= 0; i < dimensions; ++i) {
					sizes[i] = opstack[--stackpos];
				}
				cF->pc=pc;
				cF->stackpos=stackpos;
				//gc();
				//dogc++;
				opstack[stackpos++]=multianewarray(dimensions, dimensions-1, sizes, type);
				//dogc--;
				free(sizes);

				pc += 4;
				continue;
			case IFNULL:
				if (opstack[--stackpos] == 0)
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case IFNONNULL:
				if (opstack[--stackpos] != 0)
					pc+=*((short*)(code+pc+1));
				else
					pc += 3;
				continue;
			case GOTO_W:
				pc +=((code[pc + 1]) << 24) | ((code[pc + 2]) << 16) |
					((code[pc + 3]) << 8) | (code[pc + 4]) ;
				continue;
			case JSR_W:
				opstack[stackpos++]=pc + 5;
				pc +=((code[pc + 1]) << 24) | ((code[pc + 2]) << 16) |
					((code[pc + 3]) << 8) | (code[pc + 4]) ;
				continue;
			default:
				//throw new UnsupportedOperationException("opcode:" + Integer.toHexString(code[pc]));
				printf("unsupported opcode: %#08x\n", code[pc]);
				pc += 1;
				// cs+= byteToHex(code[i])+"\n";
		}
	} while (1);
}
void printFrame(Frame * f, int32_t * lv_stack, int stackpos){
	Method * m=f->method;
	fprint(stderr,m->clazz->name);
	fprintf(stderr,".");
	fprintln(stderr,m->signature);
	int * off=f->lv;
	for(int i=0;i<m->max_lv;++i,++off){
		fprintf(stderr,"lv[%d]=%d\n",i,*off);
	}
	for(int i=0;i<m->max_stack;++i,++off){
		if((lv_stack+stackpos)==off)
			fprintf(stderr,"st[%d]=%d (%p)<\n",i,*off, off);
		else
			fprintf(stderr,"st[%d]=%d (%p)\n",i,  *off, off);
	}
}
