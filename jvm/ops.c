#include <stdio.h>
#include <stdlib.h>
#include "../util/mymalloc.h"
#include "ops.h"

char * OP_toString(unsigned char op){
	switch(op){
case 0x0: return "NOP";
case 0x1: return "ACONST_NULL";
case 0x2: return "ICONST_M1";
case 0x3: return "ICONST_0";
case 0x4: return "ICONST_1";
case 0x5: return "ICONST_2";
case 0x6: return "ICONST_3";
case 0x7: return "ICONST_4";
case 0x8: return "ICONST_5";
case 0x9: return "LCONST_0";
case 0xa: return "LCONST_1";
case 0xb: return "FCONST_0";
case 0xc: return "FCONST_1";
case 0xd: return "FCONST_2";
case 0xe: return "DCONST_0";
case 0xf: return "DCONST_1";
case 0x10: return "BIPUSH";
case 0x11: return "SIPUSH";
case 0x12: return "LDC";
case 0x13: return "LDC_W";
case 0x14: return "LDC2_W";
case 0x15: return "ILOAD";
case 0x16: return "LLOAD";
case 0x17: return "FLOAD";
case 0x18: return "DLOAD";
case 0x19: return "ALOAD";
case 0x1a: return "ILOAD_0";
case 0x1b: return "ILOAD_1";
case 0x1c: return "ILOAD_2";
case 0x1d: return "ILOAD_3";
case 0x1e: return "LLOAD_0";
case 0x1f: return "LLOAD_1";
case 0x20: return "LLOAD_2";
case 0x21: return "LLOAD_3";
case 0x22: return "FLOAD_0";
case 0x23: return "FLOAD_1";
case 0x24: return "FLOAD_2";
case 0x25: return "FLOAD_3";
case 0x26: return "DLOAD_0";
case 0x27: return "DLOAD_1";
case 0x28: return "DLOAD_2";
case 0x29: return "DLOAD_3";
case 0x2a: return "ALOAD_0";
case 0x2b: return "ALOAD_1";
case 0x2c: return "ALOAD_2";
case 0x2d: return "ALOAD_3";
case 0x2e: return "IALOAD";
case 0x2f: return "LALOAD";
case 0x30: return "FALOAD";
case 0x31: return "DALOAD";
case 0x32: return "AALOAD";
case 0x33: return "BALOAD";
case 0x34: return "CALOAD";
case 0x35: return "SALOAD";
case 0x36: return "ISTORE";
case 0x37: return "LSTORE";
case 0x38: return "FSTORE";
case 0x39: return "DSTORE";
case 0x3a: return "ASTORE";
case 0x3b: return "ISTORE_0";
case 0x3c: return "ISTORE_1";
case 0x3d: return "ISTORE_2";
case 0x3e: return "ISTORE_3";
case 0x3f: return "LSTORE_0";
case 0x40: return "LSTORE_1";
case 0x41: return "LSTORE_2";
case 0x42: return "LSTORE_3";
case 0x43: return "FSTORE_0";
case 0x44: return "FSTORE_1";
case 0x45: return "FSTORE_2";
case 0x46: return "FSTORE_3";
case 0x47: return "DSTORE_0";
case 0x48: return "DSTORE_1";
case 0x49: return "DSTORE_2";
case 0x4a: return "DSTORE_3";
case 0x4b: return "ASTORE_0";
case 0x4c: return "ASTORE_1";
case 0x4d: return "ASTORE_2";
case 0x4e: return "ASTORE_3";
case 0x4f: return "IASTORE";
case 0x50: return "LASTORE";
case 0x51: return "FASTORE";
case 0x52: return "DASTORE";
case 0x53: return "AASTORE";
case 0x54: return "BASTORE";
case 0x55: return "CASTORE";
case 0x56: return "SASTORE";
case 0x57: return "POP";
case 0x58: return "POP2";
case 0x59: return "DUP";
case 0x5a: return "DUP_X1";
case 0x5b: return "DUP_X2";
case 0x5c: return "DUP2";
case 0x5d: return "DUP2_X1";
case 0x5e: return "DUP2_X2";
case 0x5f: return "SWAP";
case 0x60: return "IADD";
case 0x61: return "LADD";
case 0x62: return "FADD";
case 0x63: return "DADD";
case 0x64: return "ISUB";
case 0x65: return "LSUB";
case 0x66: return "FSUB";
case 0x67: return "DSUB";
case 0x68: return "IMUL";
case 0x69: return "LMUL";
case 0x6a: return "FMUL";
case 0x6b: return "DMUL";
case 0x6c: return "IDIV";
case 0x6d: return "LDIV";
case 0x6e: return "FDIV";
case 0x6f: return "DDIV";
case 0x70: return "IREM";
case 0x71: return "LREM";
case 0x72: return "FREM";
case 0x73: return "DREM";
case 0x74: return "INEG";
case 0x75: return "LNEG";
case 0x76: return "FNEG";
case 0x77: return "DNEG";
case 0x78: return "ISHL";
case 0x79: return "LSHL";
case 0x7a: return "ISHR";
case 0x7b: return "LSHR";
case 0x7c: return "IUSHR";
case 0x7d: return "LUSHR";
case 0x7e: return "IAND";
case 0x7f: return "LAND";
case 0x80: return "IOR";
case 0x81: return "LOR";
case 0x82: return "IXOR";
case 0x83: return "LXOR";
case 0x84: return "IINC";
case 0x85: return "I2L";
case 0x86: return "I2F";
case 0x87: return "I2D";
case 0x88: return "L2I";
case 0x89: return "L2F";
case 0x8a: return "L2D";
case 0x8b: return "F2I";
case 0x8c: return "F2L";
case 0x8d: return "F2D";
case 0x8e: return "D2I";
case 0x8f: return "D2L";
case 0x90: return "D2F";
case 0x91: return "I2B";
case 0x92: return "I2C";
case 0x93: return "I2S";
case 0x94: return "LCMP";
case 0x95: return "FCMPL";
case 0x96: return "FCMPG";
case 0x97: return "DCMPL";
case 0x98: return "DCMPG";
case 0x99: return "IFEQ";
case 0x9a: return "IFNE";
case 0x9b: return "IFLT";
case 0x9c: return "IFGE";
case 0x9d: return "IFGT";
case 0x9e: return "IFLE";
case 0x9f: return "IF_ICMPEQ";
case 0xa0: return "IF_ICMPNE";
case 0xa1: return "IF_ICMPLT";
case 0xa2: return "IF_ICMPGE";
case 0xa3: return "IF_ICMPGT";
case 0xa4: return "IF_ICMPLE";
case 0xa5: return "IF_ACMPEQ";
case 0xa6: return "IF_ACMPNE";
case 0xa7: return "GOTO";
case 0xa8: return "JSR";
case 0xa9: return "RET";
case 0xaa: return "TABLESWITCH";
case 0xab: return "LOOKUPSWITCH";
case 0xac: return "IRETURN";
case 0xad: return "LRETURN";
case 0xae: return "FRETURN";
case 0xaf: return "DRETURN";
case 0xb0: return "ARETURN";
case 0xb1: return "RETURN";
case 0xb2: return "GETSTATIC";
case 0xb3: return "PUTSTATIC";
case 0xb4: return "GETFIELD";
case 0xb5: return "PUTFIELD";
case 0xb6: return "INVOKEVIRTUAL";
case 0xb7: return "INVOKESPECIAL";
case 0xb8: return "INVOKESTATIC";
case 0xb9: return "INVOKEINTERFACE";
case 0xba: return "INVOKEDYNAMIC";
case 0xbb: return "NEW";
case 0xbc: return "NEWARRAY";
case 0xbd: return "ANEWARRAY";
case 0xbe: return "ARRAYLENGTH";
case 0xbf: return "ATHROW";
case 0xc0: return "CHECKCAST";
case 0xc1: return "INSTANCEOF";
case 0xc2: return "MONITORENTER";
case 0xc3: return "MONITOREXIT";
case 0xc4: return "WIDE";
case 0xc5: return "MULTIANEWARRAY";
case 0xc6: return "IFNULL";
case 0xc7: return "IFNONNULL";
case 0xc8: return "GOTO_W";
case 0xc9: return "JSR_W";
					 

case _PUTSTATIC: 
	return "_PUTSTATIC ";
case _GETSTATIC: 
	return "_GETSTATIC"; 
case PUTSTATICREF: 
	return "PUTSTATICREF";
case GETSTATICREF: 
	return "GETSTATICREF";
case GETSTATIC64: 
	return "GETSTATIC64";
case PUTSTATIC64: 
	return "PUTSTATIC64";
case _GETFIELD: 
	return "_GETFIELD";
case _PUTFIELD: 
	return "_PUTFIELD";
case GETFIELD64: 
	return "GETFIELD64";
case PUTFIELD64:
	return "PUTFIELD64";
case _LDC:
	return "_LDC";
case _LDC_OBJECT:
	return "_LDC_OBJECT";
case _LDC_W:
	return "_LDC_W";
case _LDC_W_OBJECT:
	return "_LDC_W_OBJECT";
case _INVOKEVIRTUAL:
	return "_INVOKEVIRTUAL";
case _INVOKESPECIAL:
	return "_INVOKESPECIAL";
case INVOKESPECIAL_NATIVE:
	return "_INVOKESPECIAL_NATIVE";
case _INVOKESTATIC:
	return "_INVOKESTATIC";
case INVOKESTATIC_NATIVE:
	return "INVOKESTATICNATIVE";
case _NEW:
	return "_NEW";
	}
	return "UNKNOWN OP";
}
