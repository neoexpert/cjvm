#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <ffi.h>
#include <pthread.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../util/arraylist.h"
#include "../lang/strings.h"
#include "../io/buffer.h"
#include "constant.h"
#include "attribute.h"
#include "method.h"
#include "field.h"
#include "class.h"
#include "classloader.h"
#include "../util/linkedlist.h"
#include "../util/iarraylist.h"
#include "heap.h"
#include "frame.h"
#include "thread.h"
#include "jarclasspath.h"
#include "bytecodeexecutor.h"

void initClassLoader(){
	classpath=newArrayList(16);
	//ArrayList_add(classpath, _s("./"));
	addDirClassPath("./");
	addJarClassPath("./rt.jar");
	classExt=_s(".class");
	POINT=intern(_s("."));
	UNDERSCORE=intern(_s("_"));
	Java_=intern(_s("Java_"));

	classes=newHashMap(64);
	_clinit=intern(_s("<clinit>()V"));
	String * jlo=intern(_s("java/lang/Class"));
	getClass(jlo, false);
	String * jls=intern(_s("java/lang/Class"));
	java_lang_Class=getClass(jls, false);

	JAVA_LANG_THROWABLE=intern(_s("java/lang/Throwable"));
	NULL_POINTER_EXCEPTION=intern(_s("java/lang/NullPointerException"));
	UNSATISFIEDLINKERROR=intern(_s("java/lang/UnsatisfiedLinkError"));
	JAVA_LANG_STACKTRACEELEMENT=intern(_s("java/lang/StackTraceElement"));
	java_lang_String=intern(_s("java/lang/String"));
	getClass(NULL_POINTER_EXCEPTION,true);
}



Buffer * getBufferFromFile(ClassPath * cp, char * fileName){
	int64_t len=strlen(cp->source)+strlen(fileName)+1;
	char * filePath=malloc(len);
	snprintf(filePath, len, "%s%s", (char *)cp->source, fileName);
	Buffer * buf= ReadFile(filePath);
	free(filePath);
	return buf;
}

void addDirClassPath(char * path){
	ClassPath * cp=malloc(sizeof(ClassPath));
	cp->source=path;
	cp->code_source=intern(_s(path));
	cp->getBuffer=(BufferGetter*)&getBufferFromFile;
	cp->close=NULL;
	ArrayList_add(classpath, cp);
}

void deleteClassLoader(){
	Entry ** entries=classes->entries;
	for(int32_t i=0;i<classes->arrSize;++i){
		Entry * entry=entries[i];
		if(entry!=NULL){
			Class * class=entry->value;
#ifdef VERBOSE
			fprintf(stderr, "class to delete: ");
			fprintln(stderr, class->name);
#endif
			deleteClass(class);
		}
	}
	HashMap_delete(classes);
}

Class * getClass(String * name, bool initialize){
	Class * class = HashMap_get(classes,name);
	if(class==NULL){
#ifdef VERBOSE
		printf("loading class ");
		println(name);
#endif
		String * fileName=concat(name,classExt);
		for(int i=0;i<classpath->size;++i){
			ClassPath * cp = classpath->values[i];
			/*
			String * filePath=concat(cp, fileName);
			char * cFilePath=_cs(filePath);
			Buffer * buf= ReadFile(cFilePath);
			free(cFilePath);
			deleteString(filePath);
			*/
			String * fileName=concat(name,classExt);
			char * cFileName=_cs(fileName);
			//fprintf(stderr, "loadingClass %s from %d\n",cFileName, i);
			Buffer * buf = cp->getBuffer((struct ClassPath*)cp, cFileName);
			free(cFileName);
			deleteString(fileName);
			if(buf==NULL)
				continue;
			class=readClass(buf);
			deleteBufferAndContents(buf);
			class->code_source=cp->code_source;
		}
		if(class==NULL)
			return NULL;
		/*
		if(class==NULL){
			char * cFileName=_cs(fileName);
			Buffer * buf=readzipEntry(rt,cFileName);
			free(cFileName);
			if(buf==NULL)
				return NULL;
			class=readClass(buf);
			deleteBufferAndContents(buf);
		}*/
		deleteString(fileName);
		HashMap_put(classes,name,class);
		class->isInitialized=false;
	}
	if(initialize&&!class->isInitialized){
		clinit(class);
	}
	return class;
}

void clinit(Class * class){
#ifdef VERBOSE
	fprintf(stderr, "clinit ");
	fprintln(stderr, class->name);
#endif
	class->isInitialized=true;
	//disableGC();
	Method * m=HashMap_get(class->smt, _clinit);
	if(m!=NULL){
		Thread * currentThread=pthread_getspecific(glob_threadID_key);
		Thread * t=newThread();
		int res=Thread_start(t, m, class->cpool, 0);
		if(res!=0){
			fprintf(stderr, "could not initialize class: ");
			fprintln(stderr, class->name);
		}
		pthread_setspecific(glob_threadID_key, currentThread);

		/*
		FrameStack * fstack=newFrameStack(4096, 4096, 16);
		uint16_t max_lv=m->max_lv;
		Frame * f=newFrame(fstack, m, fstack->lv_stack ,max_lv);
		//pushFrame(fstack,f);
		int res=step(m, f, max_lv, fstack);
		*/
	}
	//enableGC();
	//getClassClassInstanceIndex(class);
}

