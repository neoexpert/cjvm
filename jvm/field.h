#ifndef FIELD_H
#define FIELD_H
typedef struct {
	String * classname;
	String * type;
}FieldHead;
typedef struct {
	uint16_t accessFlags;
	bool isStatic;
	String * name;
	String * type;
	uint16_t attrsCount;
	Attribute ** attrs;
	long default_value;
	//used to store static field value
	long * value;
	bool isRef;
	int index;
}Field;
Field * readField(Buffer * buf,Constant ** cpool);
String * constantValue;
void initFields();
void deleteField(Field * f);
#endif
