#ifndef CLASSLOADER_H
#define CLASSLOADER_H

typedef struct {
}ClassLoader;

struct ClassPath;

typedef Buffer * (BufferGetter)(struct ClassPath *, char*);
typedef void (ClassPathCloser)(struct ClassPath *);

typedef struct {
	void * source;
	String * code_source;
	BufferGetter * getBuffer;
	ClassPathCloser * close;
}ClassPath;

ArrayList * classpath;
void addDirClassPath(char * path);
HashMap * classes;
void initClassLoader();
void deleteClassLoader();
Class * getClass(String * name, bool initialize);
String * classExt;
String * _clinit;
void clinit(Class * class);
String * JAVA_LANG_THROWABLE;
String * NULL_POINTER_EXCEPTION;
String * JAVA_LANG_STACKTRACEELEMENT;
String * UNSATISFIEDLINKERROR;
String * java_lang_String;
String * POINT;
String * UNDERSCORE;
String * Java_;
Class * java_lang_Class;
#endif
