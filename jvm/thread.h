#ifndef THREAD_H
#define THREAD_H
typedef struct {
	FrameStack * stack;
	Node * threadNode;
}Thread;
Thread * newThread();
int Thread_start(Thread * t, Method * m, Constant ** cpool,int argc, ...);
void deleteThread(Thread * t);
pthread_key_t glob_threadID_key;
#endif
