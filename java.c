#include <stdio.h>
#include <stdlib.h>
#include "util/mymalloc.h"
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#ifdef __x86_64__
#include <execinfo.h>
#endif
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <ffi.h>
#include "types.h"
#include "lang/string.h"
#include "util/hashmap.h"
#include "util/linkedlist.h"
#include "util/iarraylist.h"
#include "util/arraylist.h"
#include "lang/strings.h"
#include "io/buffer.h"
#include "jvm/memory.h"
#include "jvm/constant.h"
#include "jvm/attribute.h"
#include "jvm/method.h"
#include "jvm/field.h"
#include "jvm/class.h"
#include "jvm/heap.h"
#include "jvm/jvm.h"
#include "jvm/classloader.h"
#include "jvm/jarclasspath.h"
#include "jvm/frame.h"
#include "jvm/jni.h"
#include "jvm/bytecodeexecutor.h"
#include "jvm/thread.h"
#include "util/zip/myzip.h"

#ifdef __x86_64__
void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}
#endif

void printUsage(char * programname){
	printf("usage: %s classname\n",programname);
}
int main(int argc, char ** argv){
	//char * locale = setlocale(LC_ALL,"");
	//printf("The current locale is %s\n",locale);
	if(!setlocale(LC_CTYPE,"")){
		fprintf(stderr, "setLocale failed\n");
	}


#ifdef __x86_64__
	signal(SIGSEGV, handler);   // install our handler
#endif

	if(argc<=1){
		printUsage(argv[0]);
		return 0;
	}
	initStrings();
	String * mainClass=NULL;
	char * jar=NULL;
	int argsoffset=2;
	int edenCapacity=8025;
	for(int i=1;i<argc;++i){
		char * arg=argv[i];
		if(arg[0]=='-'){
			if(!strcmp(arg, "-jar")){
				if(argc<=i+1){
					printUsage(argv[0]);
					return -1;
				}
				jar=argv[i+1];
				argsoffset=i+2;
				break;
			}
			if(!strcmp(arg, "--eden-capacity")){
				if(argc<=i+1){
					printUsage(argv[0]);
					return -1;
				}
				edenCapacity = atoi(argv[++i]);
				fprintf(stderr, "edenCapacity: %d\n",edenCapacity);
			}
		}
		else{
			mainClass=intern(_s(arg));
			break;
		}
	}
	if(mainClass==NULL){
		printUsage(argv[0]);
		return 0;
	}
	//fprintf(stderr, "argsoffset: %d\n", argsoffset);
	initMem();
	initHeap(edenCapacity);
	initAttributes();
	initClassLoader();
	initJNI();

	pthread_key_create(&glob_threadID_key,NULL);

	if(jar!=NULL){
		ClassPath * cp=addJarClassPath(jar);
		if(cp==NULL){
			fprintf(stderr, "could not open jar file: %s\n", jar);
			return -1;
		}
		Buffer * buf=cp->getBuffer((struct ClassPath*)cp, "META-INF/MANIFEST.MF");
		if(buf==NULL){
			fprintf(stderr, "Manifest file not found: %s\n", jar);
			return -1;
		}
		//fprintf(stderr, "Manifest file found!\n");
		char * line;
		while((line=Buffer_readLine(buf))!=NULL){
			//fprintf(stderr, "%s\n", line);
			if(startsWith(line, "Main-Class: ")){
				//fprintf(stderr, "true. %s\n", line);
				char * _mainClass=substring(line, 12, strlen(line));
				replace(_mainClass, '.', '/', strlen(_mainClass));
				//fprintf(stderr, "mainClass: %s\n", _mainClass);
				mainClass=intern(_s(_mainClass));
			}
		}

	}


	fprintf(stderr, "after heap init\n");
	createArrayInstance('L', argc-argsoffset);
	Class * class=getClass(mainClass, true);
	if(class==NULL){
		fprintf(stderr, "could not load mainClass: ");
		fprintln(stderr, mainClass);
		return -1;
	}
	Method * mainMethod=HashMap_get(class->smt,intern(_s("main([Ljava/lang/String;)V")));
	if(mainMethod==NULL){
		printf("main method not found\n");
		deleteClass(class);
		return -1;
	}
	//JVM * jvm =createJVM();

	Thread * t=newThread();
	//fprintf(stderr, "before disabling \n");
	//disableGC();
	uint32_t argsoref=createArrayInstance('L', argc-argsoffset);
	uint32_t * args=createJNIRef(t->stack, argsoref);
	//ObjectArrayInstance * args=heap+argsoref;
	for(int i=0;i<argc-argsoffset;++i){
		char * carg=argv[i+argsoffset];
		String * arg = intern(encodeUTF8(strlen(carg), (int8_t*)carg));
		uint32_t * oref=createStringInstance(t->stack, arg);
		*(uint32_t*)(heap+*args+i*4)=*oref;
	} 
	//dumpCurrentHeap();
	//enableGC();
	//fprintf(stderr, "adter enabling \n");
	int res=Thread_start(t, mainMethod, class->cpool, 1, *args);
	pthread_key_delete(glob_threadID_key);
	free(t);
	//invoke(jvm,mainMethod, argsoref, class->cpool);
	//dumpHeap();
	dumpThreads();
	deleteClassLoader();
	deleteStrings();
	return res;
}

