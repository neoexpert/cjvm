#include <stdio.h>
#include <stdlib.h>

typedef int METHOD(int64_t, int64_t, int64_t, int64_t);
int foo(int i, char c, short s, long l){
	printf("i: %d, c: %c s: %hd l:%ld\n", i, c, s, l);
	return 5;
}

int main(int argc, char ** argv){
	METHOD * ptr=(METHOD*)&foo;
	printf("res: %d\n", ptr(1,'2',3,4));
	printf("res: %d\n", foo(1,'2',3,4));
	return 0;
}
