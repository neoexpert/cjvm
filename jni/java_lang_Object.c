#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include "java_lang_Object.h"

/*
 * Class:     java_lang_Object
 * Method:    hashCode
 * Signature: ()I
 */
typedef void METHOD(JNIEnv*, int64_t, int64_t, int64_t, int64_t);
JNIEXPORT jint JNICALL Java_java_lang_Object_hashCodetest
  (JNIEnv * env, jobject $this, jlong l, jshort s, jlong l2){
	  	fprintf(stderr, "size1: %ld, size2: %ld, size3: %ld \n", sizeof(jobject), sizeof(jlong), sizeof(jshort));
	  	fprintf(stderr, "arg2: %ld, arg3: %hd arg4: %ld\n", l, s, l2);
		return 0;
	}

int main(int argc, char ** argv){
	METHOD * fptr=(METHOD*)&Java_java_lang_Object_hashCodetest;
	fptr(NULL,1,2,3,4);
	fprintf(stderr, "res: %d\n", Java_java_lang_Object_hashCodetest(NULL,NULL,2,3,4));
	return 0;
}
