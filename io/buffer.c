#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include "../util/mymalloc.h"
#include "../types.h"
#include "../lang/string.h"
#include "../util/hashmap.h"
#include "../lang/strings.h"
#include "buffer.h"

Buffer * ReadFile(char *filename){	
	int8_t *buffer = NULL;
	int string_size, read_size;
	FILE *handler = fopen(filename, "r");

	if (handler)
	{
		// Seek the last byte of the file
		fseek(handler, 0, SEEK_END);
		// Offset from the first to the last byte, or in other words, filesize
		string_size = ftell(handler);
		// go back to the start of the file
		rewind(handler);

		// Allocate a string that can hold it all
		buffer =  malloc(sizeof(int8_t) * (string_size + 1) );

		// Read it all in one operation
		read_size = fread(buffer, sizeof(char), string_size, handler);

		// fread doesn't set it so put a \0 in the last position
		// and buffer is now officially a string
		buffer[string_size] = '\0';

		if (string_size != read_size)
		{
			// Something went wrong, throw away the memory and set
			// the buffer to NULL
			free(buffer);
			buffer = NULL;
		}

		// Always remember to close the file.
		fclose(handler);
	}
	else return NULL;

	Buffer * buf=malloc(sizeof(Buffer));
	buf->buf=buffer;
	buf->pos=0;
	buf->capacity=string_size;
	return buf;
}

Buffer * Buffer_wrap(int8_t * buf){
	 Buffer * buffer=malloc(sizeof(Buffer));
	 buffer->buf=buf;
	 buffer->pos=0;
	 return buffer;
	
}
uint16_t swap(uint16_t s){
	return  (s>>8) | (s<<8);
}

int8_t get(Buffer * buf){
	int8_t c=buf->buf[buf->pos];
	buf->pos++;
	return c;
}

char * Buffer_readLine(Buffer * buf){
	int cap=64;
	int len=0;
	char * line = malloc(cap);
	int bcap=buf->capacity;
	for(int i=buf->pos;i<bcap;++i){
		char c=buf->buf[i];
		if(c=='\n'){
			line[len++]='\0';
			line=realloc(line, len);
			buf->pos+=len;
			return line;
		}
		if(len>=cap)
			line=realloc(line, cap*2);
		line[len++]=c;
	}
	return NULL;
}

void getArray(Buffer * buf, int8_t * dest, int32_t length){
	memcpy(dest, buf->buf+buf->pos, length);
	buf->pos+=length;
}

int64_t getLong(Buffer * buf){
	int64_t l=*(int64_t *)(buf->buf+buf->pos);
	buf->pos+=8;
	return __builtin_bswap64(l);
}

double getDouble(Buffer * buf){
	int64_t i=*(int64_t *)(buf->buf+buf->pos);
	i= __builtin_bswap64(i);
	buf->pos+=8;
	return *(double*)&i;
}

float getFloat(Buffer * buf){
	int32_t i=*(int32_t *)(buf->buf+buf->pos);
	i= __builtin_bswap32(i);
	buf->pos+=4;
	return *(float*)&i;
}

int32_t getInt(Buffer * buf){
	int32_t i=*(int32_t *)(buf->buf+buf->pos);
	buf->pos+=4;
	return __builtin_bswap32(i);
}

uint16_t getU16(Buffer * buf){
	uint16_t i=*(uint16_t *) (buf->buf+buf->pos);
	buf->pos+=2;
	return swap(i);
}

String * getUTF8(Buffer * buf){
	uint16_t len=getU16(buf);
	String * s = encodeUTF8(len,buf->buf+buf->pos);
	buf->pos+=len;
	return intern(s);
}

void deleteBuffer(Buffer * buf){
	free(buf);
}

void deleteBufferAndContents(Buffer * buf){
	free(buf->buf);
	free(buf);
}
