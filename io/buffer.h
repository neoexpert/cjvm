#ifndef BUFFER_H
#define BUFFER_H
typedef struct {
	int8_t * buf;
	int pos;
	int capacity;
} Buffer;
Buffer * ReadFile(char *filename);
Buffer * Buffer_wrap(int8_t *arr);
uint16_t swap(uint16_t s);
int8_t get(Buffer * buf);
void getArray(Buffer * buf, int8_t * dest, int32_t length);
int64_t getLong(Buffer * buf);
double getDouble(Buffer * buf);
float getFloat(Buffer * buf);
int32_t getInt(Buffer * buf);
uint16_t getU16(Buffer * buf);
uint16_t getU16BE(Buffer * buf);
String * getUTF8(Buffer * buf);
String * encodeUTF8(int len, int8_t * buf);
char * Buffer_readLine(Buffer * buf);
void deleteBuffer(Buffer * buf);
void deleteBufferAndContents(Buffer * buf);
#endif
