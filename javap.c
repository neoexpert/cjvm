#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "types.h"
#include "lang/string.h"
#include "util/hashmap.h"
#include "lang/strings.h"
#include "io/buffer.h"
#include "jvm/constant.h"
#include "jvm/attribute.h"
#include "jvm/method.h"
#include "jvm/field.h"
#include "jvm/class.h"
#include "jvm/classloader.h"

void printUsage(char * programname){
	fprintf(stderr, "usage: %s classname\n",programname);
}
const char * _class=".class";
int main(int argc, char ** argv)
{
	if(argc==1){
		printUsage(argv[0]);
		return 0;
	}
	initStrings();
	initMethods();
	initClassLoader();
	String * className=_s(argv[1]);
	Class * class=getClass(className);
	deleteClass(class);
	return 0;
}
