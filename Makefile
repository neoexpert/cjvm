java: util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c java.c bin/shared/filesystem.so bin/shared/zip.so bin/shared/bytebuffer.so jni/jni_handler.c
	gcc -g -rdynamic util/zip/*.c util/*.c io/*.c lang/*.c jvm/*.c jni/jni_handler.c java.c -pthread -lzip -lm -ldl -lffi -o java 

bin/javamem: util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c java.c
	gcc -DMEMCHECK -g -rdynamic -lm -lpthread -lzip util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c java.c -o javamem

bin/javadebug: util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c java.c
	gcc -DVERBOSE -Wunused-variable -g -rdynamic -lm -lpthread -lzip util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c java.c -o javadebug

javadebugger: util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c java.c bin/shared/filesystem.so bin/shared/zip.so bin/shared/bytebuffer.so jni/jni_handler.c
	gcc -g -rdynamic -Wunused-variable -DVERBOSE -DDEBUGGER util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c jni/jni_handler.c java.c -lm -lpthread -lzip -ldl -o javadebugger

bin/javap: util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c javap.c
	gcc -DVERBOSE -lm -lpthread -lzip util/*.c io/*.c lang/*.c jvm/*.c javap.c -o javap

bin/testmem: util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c jvm/tests/testmem.c
	gcc -g -rdynamic -lm -lpthread -lzip util/*.c util/zip/*.c io/*.c lang/*.c jvm/*.c jvm/tests/testmem.c -o testmem

bin/testhashmap: lang/*.c util/*.c util/tests/hashmaptest.c 
	cc lang/*.c util/*.c util/tests/hashmaptest.c -o testhashmap

testlinkedlist: lang/*.c util/*.c util/tests/linkedlisttest.c 
	cc lang/*.c util/*.c util/tests/linkedlisttest.c -o testlinkedlist

testarraylist: lang/*.c util/*.c util/tests/arraylisttest.c 
	cc lang/*.c util/*.c util/tests/arraylisttest.c -o testarraylist

bin/teststrings: lang/*.c util/*.c lang/tests/stringstest.c
	cc lang/*.c util/*.c lang/tests/stringstest.c -o teststrings

bin/shared/filesystem.so: libs/filesystem/filesystem.c
	gcc -I/usr/lib/jvm/java-8-openjdk-amd64/include/ -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux -shared -fPIC libs/filesystem/filesystem.c -o bin/shared/filesystem.so
bin/shared/zip.so: libs/zip/zip.c
	gcc -I/usr/lib/jvm/java-8-openjdk-amd64/include/ -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux -shared -fPIC libs/zip/zip.c -lzip -o bin/shared/zip.so
bin/shared/bytebuffer.so: libs/bytebuffer/bytebuffer.c
	gcc -I/usr/lib/jvm/java-8-openjdk-amd64/include/ -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux -shared -fPIC libs/bytebuffer/bytebuffer.c -lzip -o bin/shared/bytebuffer.so
jni/jni:
	cc -I/usr/lib/jvm/java-8-openjdk-amd64/include/ -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux jni/java_lang_Object.c -o jni/jni

jnibuilder: jnibuilder.c
	cc jnibuilder.c -lm -o jnibuilder

jni/jni_handler.c: jnibuilder
	./jnibuilder 5
