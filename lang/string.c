#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wchar.h>
#include "../util/mymalloc.h"
#include <inttypes.h>
#include <string.h>
#include "../types.h"
#include "string.h"
#include "../util/hashmap.h"
String * encodeUTF8(int32_t len, int8_t * buf){
	String * s=malloc(sizeof(String));
	int32_t hash=0;
	s->index=-1;
	s->str=malloc(sizeof(uint16_t)*len);
	int32_t strlen=0;
	for(int32_t i=0;i<len;){
		uint16_t c;
		int8_t b = buf[i];
		++i;
		if (b > 0){
			c=b;
			s->str[strlen++]=c;
		}
		else {
			int8_t b2 = buf[i];
			++i;
			if ((b & 0xf0) != 0xe0){
				c=((uint16_t) ((b & 0x1F) << 6 | (b2 & 0x3F)));
				s->str[strlen++]=c;
			}
			else {
				int8_t b3 = buf[i];
				++i;
				c=s->str[strlen++]=((uint16_t) ((b & 0x0F) << 12 | (b2 & 0x3F) << 6 | (b3 & 0x3F)));
				s->str[strlen++]=c;
			}
		}
		hash = 31 * hash + c;
		//s->str[i]=buf->buf[buf->pos+i];
	}
	s->hash=hash;
	s->length=strlen;
	if(len>strlen)
		s->str=realloc(s->str, sizeof(uint16_t)*strlen);
	return s;
}
String * _s(const char * cstr){
	//String * str=malloc(sizeof(String));
	//str->hash=0;
	//str->index=-1;
	int len=strlen(cstr);
	return encodeUTF8(len, (int8_t*)cstr);
		/*
	str->length=len;
	str->str=malloc(len*sizeof(uint16_t));
	for(int i=0;i<len;++i){
		str->str[i]=cstr[i];
	}
	//strcpy(str->str,cstr);
	return str;
	*/
}
char * _cs(String * s){
	int32_t len=s->length;
	char * cstr=malloc(len+1);
	for(int i=0;i<len;++i)
		cstr[i]=(char)s->str[i];
	cstr[len]='\0';
	return cstr;
}

void print(String * str){
	int len=str->length;
	for(int i=0;i<len;++i)
		putwchar((uint32_t)str->str[i]);
		
}
void fprint(FILE * stream, String * str){
	int len=str->length;
	for(int i=0;i<len;++i)
		fprintf(stream,"%c", (char)str->str[i]);
		
}
void println(String * str){
	print(str);
	wprintf(L"\n");
}
void fprintln(FILE * stream, String * str){
	fprint(stream, str);
	fprintf(stream,"\n");
}


String * concat(String * this, String * s){
	String * r=malloc(sizeof(String));
	r->hash=0;
	r->index=-1;
	r->length=this->length+s->length;
	int len=r->length;
	r->str=malloc(len*sizeof(uint16_t));
	for(int i=0;i<this->length;++i){
		r->str[i]=this->str[i];
	}
	int off=this->length;
	for(int i=0;i<s->length;++i){
		r->str[i+off]=s->str[i];
	}
	return r;
}

int32_t String_hashCode(String * s){
	int h = s->hash;
	int len=s->length;
	if (h == 0 && len> 0) {
		uint16_t * val = s->str;

		for(int i = 0; i < len; ++i){
			h = 31 * h + val[i];
		}
		s->hash = h;
	}
	return h;
}

bool String_equals(String * this, String * s){
	if(this==s)return true;
	int len=this->length;
	if(len!=s->length)
		return false;
	uint16_t * s0=this->str;
	uint16_t * s1=s->str;
	for(int i=0;i<len;++i)
		if(s0[i]!=s1[i])
			return false;
	return true;

}

void deleteString(String * s){
	free(s->str);
	free(s);
}

bool startsWith(const char *str, const char *pre){
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? false : memcmp(pre, str, lenpre) == 0;
}

char * substring(const char *str, int from, int to){
	int len=to-from;
	char * result=malloc(len+1);	
	memcpy(result, str+from, len);
	result[len-1]='\0';
	return result;
}
void replace(char *str, char c, char replacement, int length){
	for(int i=0;i<length;++i){
		if(str[i]==c)
			str[i]=replacement;
	}
}

String * replaceAll(String * this, char c, char replacement){
	String * result=malloc(sizeof(String));
	int32_t len=this->length;
	result->length = len;
	int32_t hash=0;
	result->index=-1;
	result->str=malloc(len*sizeof(uint16_t));
	for(int32_t i=0;i<len;++i){
		uint16_t ch=this->str[i];
		if(ch==c)
			ch=replacement;
		result->str[i]=ch;
		hash = 31 * hash + ch;
	}
	result->hash=hash;

	return result;
}
