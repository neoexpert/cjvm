#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../util/mymalloc.h"
#include <inttypes.h>
#include <string.h>
#include "../types.h"
#include "string.h"
#include "../util/hashmap.h"
#include "strings.h"
void initStrings(){
	strings=newHashMap(1024);
}
String * intern(String * s){
	String * interned=HashMap_get(strings,s);
	if(interned==NULL){
		interned=s;
		HashMap_put(strings,interned,interned);
	}
	else{
		deleteString(s);
	}
	return interned;
}

void deleteStrings(){
	Entry ** entries=strings->entries;
	int length=strings->arrSize+1;
	for(int i=0;i<length;++i){
		Entry * entry=entries[i];
		if(entry!=NULL){
			deleteString(entry->key);
			free(entry);
		}
	}
	free(entries);
	free(strings);
}
