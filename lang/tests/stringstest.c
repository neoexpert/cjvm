#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include "../../types.h"
#include "../string.h"
#include "../../util/hashmap.h"
#include "../strings.h"


char * randomStr(){
	int len=random() % 23+3;
	char * str=malloc(len);
	for(int i=0;i<len;++i){
		//char randomletter = 'A' + (random() % 26);
		char randomletter = "();$_/1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"[random () % (26*2+10+6)];
		str[i]=randomletter;
	}
	str[len-1]='\0';
	return str;
}
int main(int argc, char ** argv)
{
	time_t seed=time(0);
	initStrings();
	srand(seed);
	String * s=_s("test");
	if(s->length!=4)
		printf("wrong length\n");
	String * s1=intern(s);
	if(s!=s1)
		printf("interning failed\n");
	for(int i=0;i<1234567;++i){
		//printf("%d\n",i);
		//char * str=malloc(12);
		//sprintf(str, "%d", i);
		char * rstr=randomStr();
		String * s2=_s(rstr);
		//int32_t hash2=String_hashCode(s2);
		//fprintf(stderr, "hash2: %d\n", hash2);

		//println(s2);
		s2=intern(s2);
		//hash2=String_hashCode(s2);
		//fprintf(stderr, "interned hash2: %d\n", hash2);
		if(s2!=intern(_s(rstr))){
			fprintf(stderr, "interning error 1\n");
			fprintf(stderr, "seed: %ld\n", seed);
			exit(-1);
		}

		if(intern(_s("test"))!=s1){
			fprintf(stderr, "interning error 2\n");
			fprintf(stderr, "seed: %ld\n", seed);
			exit(-1);
		}
	}
	srand(seed);
	for(int i=0;i<1234567;++i){
		//printf("%d\n",i);
		//char * str=malloc(12);
		//sprintf(str, "%d", i);
		char * rstr=randomStr();
		String * s2=_s(rstr);

		String * s3=intern(s2);
		if(s2==s3){
			fprintf(stderr, "interning failed 3\n");
			fprintf(stderr, "seed: %ld\n", seed);
			exit(-1);
		}
	}
	fprintf(stderr, "returned: ");
	fprintln(stderr, s1);
	return 0;
}
