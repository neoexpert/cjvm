#ifndef STRING_H
#define STRING_H
typedef struct {
	uint16_t * str;
	uint32_t length;
	int32_t hash;
	int index;
}String;
String * _s(const char * c);
char * _cs(String * s);
void print(String * str);
void fprint(FILE * stream, String * str);
void fprintln(FILE * stream, String * str);
void println(String * str);
int32_t String_hashCode(String * c);
bool String_equals(String * this, String * s);
String * concat(String * this, String * s);
void deleteString(String * c);
String * encodeUTF8(int len, int8_t * buf);
bool startsWith(const char *str, const char *pre);
char * substring(const char *str, int from, int to);
void replace(char *str, char c, char replacement, int length);
String * replaceAll(String * this, char c, char replacement);
#endif
