#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wchar.h>
#include <limits.h>
#include <jni.h>
#include <unistd.h>

#include "java_nio_NativeByteBuffer.h"

jfieldID pathFieldID;
jfieldID fdFieldID;
bool initialized = false;
void init(JNIEnv * env){
	if(initialized)return;
	initialized=true;
}

void Java_java_nio_NativeByteBuffer_setByteOrder(JNIEnv * env, jobject this, jint order){
	fprintf(stderr, "setByteOrder: %d\n",order);
}

/*
 * Class:     java_nio_NativeByteBuffer
 * Method:    getInt
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_java_nio_NativeByteBuffer_getInt(JNIEnv * env, jobject this){
	fprintf(stderr, "getInt\n");
	return 0;
}


JNIEXPORT void JNICALL Java_java_nio_NativeByteBuffer_registerNatives(JNIEnv * env){
	init(env);
}

void __attribute__ ((constructor)) initLibrary(void) {
 //
 // Function that is called when the library is loaded
 //
	fprintf(stderr, "bytebuffer is initialized\n"); 
}
void __attribute__ ((destructor)) cleanUpLibrary(void) {
 //
 // Function that is called when the library is »closed«.
 //
    //fprintf(stderr, "Library is exited\n"); 
}
