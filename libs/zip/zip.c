#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wchar.h>
#include <limits.h>
#include <jni.h>
#include <unistd.h>
#include <zip.h>

#include "java_util_zip_ZipFile.h"

typedef struct zip zip;
typedef struct zip_file zip_file;

jfieldID pathFieldID;
jfieldID fdFieldID;
bool initialized = false;
void init(JNIEnv * env){
	if(initialized)return;
	/*
	jclass fileClass = (*env)->FindClass(env, "java/io/File");
	pathFieldID = (*env)->GetFieldID(env, fileClass, "path", "Ljava/lang/String;");
	jclass fileInputStreamClass = (*env)->FindClass(env, "java/io/FileInputStream");
	fdFieldID = (*env)->GetFieldID(env, fileInputStreamClass , "fd", "Ljava/lang/String;");
	*/
	initialized=true;
}

 
/*
 * Class:     java_util_zip_ZipFile
 * Method:    registerNatives
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_java_util_zip_ZipFile_registerNatives
  (JNIEnv * env){
 	init(env); 
  }

/*
 * Class:     java_util_zip_ZipFile
 * Method:    init
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT jlong JNICALL Java_java_util_zip_ZipFile_open
  (JNIEnv * env, jstring path){
	  const char * filepath = (*env)->GetStringUTFChars(env, path, NULL);
	  if(filepath == NULL) {
		  return 0;
	  }
	  fprintf(stderr, "zip file to open: %s\n",filepath);
	  int err = 0;
	  zip * file = zip_open(filepath, 0, &err);
	  if(err!=0){
		  fprintf(stderr, "error opening zip file %s: %d\n", filepath, err);
		  return 0;
	  }
	  (*env)->ReleaseStringUTFChars(env, path, filepath);
	  return (jlong)file;
}

/*
 * Class:     java_util_zip_ZipFile
 * Method:    getEntry
 * Signature: (J, Ljava/lang/String;)Ljava/util/zip/ZipEntry;
 */
JNIEXPORT jobject JNICALL Java_java_util_zip_ZipFile_getEntry
  (JNIEnv * env, jlong id, jstring name){
	zip * file = (zip*)id;
  	return 0;
  }

/*
 * Class:     java_util_zip_ZipFile
 * Method:    getBytes
 * Signature: (Ljava/lang/String;)[B
 */
JNIEXPORT jbyteArray JNICALL Java_java_util_zip_ZipFile_getBytes
  (JNIEnv * env, jlong id, jstring path){
	zip * file = (zip*)id;
	const char * zippath = (*env)->GetStringUTFChars(env, path, NULL);
	struct zip_stat st;
	zip_stat_init(&st);
	zip_stat(file, zippath, 0, &st);
	zip_file *f = zip_fopen(file, zippath, 0);
	if(f==NULL){
		return 0;
	}
	fprintf(stderr, "zip entry found: %s\n", zippath);
	//int8_t * contents = malloc(st.size);
	fprintf(stderr, "zip entry readen bytes: %ld\n", st.size);
	jbyteArray ba=(*env)->NewByteArray(env,st.size);
	jbyte* arr= (*env)->GetByteArrayElements(env, ba, NULL);
	zip_fread(f, arr, st.size);
	zip_fclose(f);
  	return ba;
  }

/*
 * Class:     java_util_zip_ZipFile
 * Method:    getSize
 * Signature: (Ljava/lang/String;)J
 */
JNIEXPORT jlong JNICALL Java_java_util_zip_ZipFile_getSize
(JNIEnv * env, jlong id, jstring name){
	zip * file = (zip*)id;
	struct zip_stat st;
	zip_stat_init(&st);
	const char * path = (*env)->GetStringUTFChars(env, name, NULL);
	zip_stat(file, path, 0, &st);
	(*env)->ReleaseStringUTFChars(env, name, path);
	return st.size;
}



void __attribute__ ((constructor)) initLibrary(void) {
 //
 // Function that is called when the library is loaded
 //
    //fprintf(stderr, "Library is initialized\n"); 
}
void __attribute__ ((destructor)) cleanUpLibrary(void) {
 //
 // Function that is called when the library is »closed«.
 //
    //fprintf(stderr, "Library is exited\n"); 
}
