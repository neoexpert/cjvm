/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class java_util_zip_ZipFile */

#ifndef _Included_java_util_zip_ZipFile
#define _Included_java_util_zip_ZipFile
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     java_util_zip_ZipFile
 * Method:    init
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT jlong JNICALL Java_java_util_zip_ZipFile_open
  (JNIEnv *, jstring);

/*
 * Class:     java_util_zip_ZipFile
 * Method:    getEntry
 * Signature: (Ljava/lang/String;)Ljava/util/zip/ZipEntry;
 */
JNIEXPORT jobject JNICALL Java_java_util_zip_ZipFile_getEntry
  (JNIEnv *, jlong, jstring);

/*
 * Class:     java_util_zip_ZipFile
 * Method:    getBytes
 * Signature: (Ljava/lang/String;)[B
 */
JNIEXPORT jbyteArray JNICALL Java_java_util_zip_ZipFile_getBytes
  (JNIEnv *, jlong, jstring);

/*
 * Class:     java_util_zip_ZipFile
 * Method:    getSize
 * Signature: (Ljava/lang/String;)J
 */
JNIEXPORT jlong JNICALL Java_java_util_zip_ZipFile_getSize
  (JNIEnv *, jlong, jstring);

/*
 * Class:     java_util_zip_ZipFile
 * Method:    registerNatives
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_java_util_zip_ZipFile_registerNatives
  (JNIEnv *);

#ifdef __cplusplus
}
#endif
#endif
