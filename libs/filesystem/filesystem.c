#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wchar.h>
#include <limits.h>
#include <jni.h>
#include <unistd.h>

#include "java_io_File.h"
#include "java_io_FileInputStream.h"


jfieldID pathFieldID;
jfieldID fdFieldID;
bool initialized = false;
void init(JNIEnv * env){
	if(initialized)return;
	jclass fileClass = (*env)->FindClass(env, "java/io/File");
	pathFieldID = (*env)->GetFieldID(env, fileClass, "path", "Ljava/lang/String;");
	jclass fileInputStreamClass = (*env)->FindClass(env, "java/io/FileInputStream");
	fdFieldID = (*env)->GetFieldID(env, fileInputStreamClass , "fd", "Ljava/lang/String;");
	initialized=true;
}

 
JNIEXPORT void JNICALL Java_java_io_File_registerNatives
(JNIEnv * env){
	init(env);
}

JNIEXPORT void JNICALL Java_java_io_FileInputStream_registerNatives
(JNIEnv * env){
	init(env);
}

/*
 * Class:     java_io_File
 * Method:    getAbsolutePath
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_java_io_File_getAbsolutePath
(JNIEnv * env, jobject this){
	jstring jpath = (*env)->GetObjectField(env, this, pathFieldID);
	const char *path = (*env)->GetStringUTFChars(env, jpath, NULL);
	char actualpath [PATH_MAX+1];
	char *ptr=realpath(path, actualpath);
	if(ptr==NULL)
		return 0;
	return (*env)->NewStringUTF(env,ptr);
}

/*
 * Class:     java_io_File
 * Method:    isDirectory
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_java_io_File_isDirectory
  (JNIEnv * env, jobject this){
		return 0;
	}

/*
 * Class:     java_io_File
 * Method:    getParent
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_java_io_File_getParent
  (JNIEnv * env, jobject this){
		return NULL;
	}

/*
 * Class:     java_io_File
 * Method:    exists
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_java_io_File_exists
(JNIEnv * env, jobject this){
	return 0;
}

/*
 * Class:     java_io_File
 * Method:    length
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_java_io_File_length
(JNIEnv * env, jobject this){
	//fprintf(stderr, "this was:%d\n",this);
	//jclass cls = (*env)->GetObjectClass(env, this);
	//jfieldID fidInt = (*env)->GetFieldID(env, cls, "path", "Ljava/lang/String;");
	jstring path = (*env)->GetObjectField(env, this, pathFieldID);
	const char *filename;
	filename = (*env)->GetStringUTFChars(env, path, NULL);
	if(filename == NULL) {
		return 0;
	}
	//fprintf(stderr, "fileName: %s\n",filename);
	struct stat st;
	stat(filename, &st);
	return st.st_size;
}

/*
 * Class:     java_io_FileInputStream
 * Method:    close
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_java_io_FileInputStream_close
  (JNIEnv * env, jobject this){
 	//fprintf(stderr, "close file\n"); 
	jint fd = (*env)->GetIntField(env, this, fdFieldID);
 	//fprintf(stderr, "fd is: %d\n", fd); 
	close(fd);
  }

/*
 * Class:     java_io_FileInputStream
 * Method:    open
 * Signature: (Ljava/io/File;)V
 */
JNIEXPORT jint JNICALL Java_java_io_FileInputStream_open
(JNIEnv * env, jobject this, jobject file){
	jstring path = (*env)->GetObjectField(env, file, pathFieldID);
	const char *filename;
	filename = (*env)->GetStringUTFChars(env, path, NULL);
	if(filename == NULL) {
		return 0;
	}
	//fprintf(stderr, "file to open: %s\n",filename);
	int fd=open(filename, O_RDWR);
	(*env)->ReleaseStringUTFChars(env, path, filename);
	return fd;
}

/*
 * Class:     java_io_FileInputStream
 * Method:    read
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_java_io_FileInputStream_read0
  (JNIEnv * env, jobject this){
		return -1;
  }

/*
 * Class:     java_io_FileInputStream
 * Method:    read
 * Signature: ([BII)I
 */
JNIEXPORT jint JNICALL Java_java_io_FileInputStream_read
(JNIEnv * env, jobject this, jbyteArray ba, jint offset, jint count){
	jint fd = (*env)->GetIntField(env, this, fdFieldID);
	jbyte* arr= (*env)->GetByteArrayElements(env, ba, NULL);
	//fprintf(stderr, "fd: %d offset: %d count: %d\n", fd, offset, count);
	return read(fd, arr, count);
}



void __attribute__ ((constructor)) initLibrary(void) {
 //
 // Function that is called when the library is loaded
 //
    //fprintf(stderr, "Library is initialized\n"); 
}
void __attribute__ ((destructor)) cleanUpLibrary(void) {
 //
 // Function that is called when the library is »closed«.
 //
    //fprintf(stderr, "Library is exited\n"); 
}
