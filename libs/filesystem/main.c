#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

void* getFunctionPointer(void* lib, const char* funcName) {
 //
 // Get the function pointer to the function
    void* fptr = dlsym(lib, funcName);
    if (!fptr) {
      fprintf(stderr, "Could not get function pointer for %s\n  error is: %s\n\n", funcName, dlerror());
      return NULL;
    }
    return fptr;
}

int main(int argc, char* argv[]) {
	fprintf(stderr, "starting...\n");
	void  (*fptr_add       )(char *);
	void* libfilesystem = dlopen("../../bin/shared/filesystem.so",  RTLD_LAZY | RTLD_GLOBAL);
	if (!libfilesystem) {
		//
		// Apparently, the library could not be opened
		fprintf(stderr, "Could not open libtq84.so\n");
		exit(1);
	}
	fptr_add=getFunctionPointer(libfilesystem, "open_file");
	fptr_add("thefile"); 

	fprintf(stderr, "start...\n");
  	return 0;
}
