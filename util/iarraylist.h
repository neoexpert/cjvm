#ifndef IARRAYLIST_H
#define IARRAYLIST_H

typedef struct IArrayList {
	int32_t * values;
	int size;
	int capacity;
}IArrayList;
IArrayList* newIArrayList(int initialCapacity);
void* IArrayList_remove(IArrayList * iai, int index);
void IArrayList_add(IArrayList * iai, int32_t value);
void IArrayList_add64(IArrayList * iai, int64_t value);
void IArrayList_fixSize(IArrayList * ai);

#endif
