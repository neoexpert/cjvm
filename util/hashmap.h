#ifndef HASHMAP_H
#define HASHMAP_H
typedef struct{
	String * key;
	void * value;
}Entry;
typedef struct {
	Entry ** entries;
	int arrSize;
}HashMap;
//initialSize should be power of 2
HashMap * newHashMap(unsigned int initialSize);

//roundes initialSize up to next power of 2
HashMap * newHashMapRU(unsigned int initialSize);
void HashMap_put(HashMap * hm, String * key, void * value);
bool HashMap_putEntry(Entry ** entries, Entry * newEntry, int length);
void * HashMap_get(HashMap *hm, String * key);
void HashMap_grow(HashMap * hm, int newSize);
void HashMap_dump(HashMap *hm);
void HashMap_delete(HashMap * hm);
#endif
