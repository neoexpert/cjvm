#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include "mymalloc.h"
#include <string.h>
#include "../types.h"
#include "../lang/string.h"
#include "hashmap.h"
#include "../lang/string.h"
#include "ihashmap.h"

//initialSize should be power of 2
IHashMap * newIHashMap(unsigned int initialSize){
	IHashMap * hm=malloc(sizeof(IHashMap));
	hm->arrSize=initialSize-1;
	hm->entries=calloc(initialSize,sizeof(Entry*));
	return hm;
}
//roundes initialSize up to next power of 2
IHashMap * newIHashMapRU(unsigned int initialSize){
	//see: https://stackoverflow.com/a/466242/5591561
	unsigned int v=initialSize; // compute the next highest power of 2 of 32-bit v
	--v;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	return newIHashMap(v);
}

void IHashMap_put(IHashMap * hm, int key, void * value){
	int hashCode=key;
	int pos=hashCode & hm->arrSize;
	IEntry * entry=hm->entries[pos];
	if(entry!=0){
		if(entry->key==key){
			entry->value=value;
			return;
		}
		IHashMap_grow(hm,(hm->arrSize+1)*2);
		IHashMap_put(hm,key,value);
		return;
	}
	entry=malloc(sizeof(IEntry));
	entry->key=key;
	entry->value=value;
	hm->entries[pos]=entry;
}

int IHashMap_putEntry(IEntry ** entries, IEntry * newEntry, int length){
	int hashCode=newEntry->key;
	int pos=hashCode & length;
	if(entries[pos]!=0){
		printf("colliion while growing\n");
		return 0;
	}
	entries[pos]=newEntry;
	return 1;
}

void * IHashMap_get(IHashMap * hm, int key){
	int hashCode=key;
	int pos=hashCode & hm->arrSize;
	IEntry * entry=hm->entries[pos];
	if(entry==0)return 0;
	if(entry->key==key)
		return entry->value;
	return 0;
}

void IHashMap_grow(IHashMap * hm,int newSize){
	int arrSize=newSize-1;
	IEntry ** oldData=hm->entries;
	int oldSize=hm->arrSize+1;
	IEntry ** newData=calloc(newSize,sizeof(IEntry*));
	for(int i=0;i<oldSize;++i){
		IEntry * entry=oldData[i];
		if(entry!=0)
			if(IHashMap_putEntry(newData,entry,arrSize)!=1){
				free(newData);
				IHashMap_grow(hm,newSize*2);
				return;
			}
	}
	hm->entries=newData;
	hm->arrSize=arrSize;
	free(oldData);
}

void IHashMap_dump(IHashMap *hm){
	printf("HashMap dump:\n");
	IEntry ** entries=hm->entries;
	int l=hm->arrSize+1;
	for(int i=0;i<l;++i){
		IEntry * entry=entries[i];
		if(entry!=0){
			String * value=(String *)entry->value;
			printf("%d -> ",entry->key);
			println(value);
		}
	}
}

void IHashMap_delete(IHashMap * hm){
	IEntry ** entries=hm->entries;
	int length=hm->arrSize;
	for(int i=0;i<length;++i){
		IEntry * entry=entries[i];
		if(entry!=NULL){
			//free(entry);
		}
	}
	free(entries);
	free(hm);
}

