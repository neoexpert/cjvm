#ifndef LINKEDLIST_H
#define LINKEDLIST_H
typedef struct Node {
	void * value;
	struct Node* next;
	struct Node* prev;
} Node;

typedef struct LinkedList {
	Node * first;
	Node * last;
}LinkedList;
Node* newNode(void * value);
LinkedList* newLinkedList();
void* LinkedList_removeFirst(LinkedList * ll);
void LinkedList_removeNode(LinkedList * ll, Node * n);
Node * LinkedList_add(LinkedList * ll, void * value);
Node * LinkedList_addFirst(LinkedList * ll, void * value);
void LinkedList_dump(LinkedList * ll);
#endif
