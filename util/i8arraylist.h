#ifndef I8ARRAYLIST_H
#define I8ARRAYLIST_H

typedef struct I8ArrayList {
	int8_t * values;
	int size;
	int capacity;
}I8ArrayList;
I8ArrayList* newI8ArrayList(int initialCapacity);
void* I8ArrayList_remove(I8ArrayList * iai, int index);
void I8ArrayList_add(I8ArrayList * iai, int8_t value);
void I8ArrayList_fixSize(I8ArrayList * ai);

#endif
