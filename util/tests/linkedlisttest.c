#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include "../../types.h"
#include "../../lang/string.h"
#include "../linkedlist.h"


const char * _class=".class";
int main(int argc, char ** argv)
{
	printf("LinkedList test\n");
	LinkedList * ll=newLinkedList();
	for(int i=0;i<123456;++i){
		printf("%d\n",i);
		int * value=malloc(4);
		printf("%d\n",i);
		*value=i;
		printf("%d\n",i);
		LinkedList_add(ll,value);
	}
	printf("LinkedList dump:\n");
	LinkedList_dump(ll);
	printf("LinkedList dump done\n");
	int fails=0;
	for(int i=0;i<123456;++i){
		printf("%d\n",i);
		int * value=LinkedList_removeFirst(ll);
		if(*value!=i){
			printf("wrong value. expected: %d, got: %d\n",i,*value);
			fails++;
		}
	}
	if(fails!=0)
		printf("test failed\n");

	for(int i=0;i<123456;++i){
		printf("%d\n",i);
		int * value=malloc(4);
		printf("%d\n",i);
		*value=i;
		printf("%d\n",i);
		LinkedList_addFirst(ll,value);
	}
	for(int i=123455;i>=0;--i){
		printf("%d\n",i);
		int * value=LinkedList_removeFirst(ll);
		if(*value!=i){
			printf("wrong value. expected: %d, got: %d\n",i,*value);
			fails++;
		}
	}

	return 0;
}
