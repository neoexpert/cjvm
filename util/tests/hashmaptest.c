#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include "../../types.h"
#include "../../lang/string.h"
#include "../hashmap.h"
#include "../../lang/strings.h"


int main(int argc, char ** argv)
{
	HashMap * hm=newHashMap(128);
	for(int i=0;i<123456;++i){
		printf("%d\n",i);
		char * str=malloc(12);
		sprintf(str, "%d", i);
		HashMap_put(hm,concat(_s("key"),_s(str)),concat(_s("bluber"),_s(str)));
	}

	for(int i=0;i<123456;++i){
		printf("%d\n",i);
		char * str=malloc(12);
		sprintf(str, "%d", i);
		String * value=HashMap_get(hm,concat(_s("key"),_s(str)));
		if(!String_equals(value, concat(_s("bluber"),_s(str)))){
			fprintf(stderr, "test failed\n");	
			exit(-1);
		}
	}
	HashMap_put(hm,_s("key"),_s("other"));
	HashMap_dump(hm);
	String * blu=HashMap_get(hm,_s("key"));
	printf("blu is: ");
	println(blu);
	HashMap_delete(hm);
	return 0;
}
