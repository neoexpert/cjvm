#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include "../../types.h"
#include "../../lang/string.h"
#include "../iarraylist.h"


const char * _class=".class";
int main(int argc, char ** argv)
{
	printf("arrayList test\n");
	IArrayList * al=newIArrayList(1);
	for(int i=0;i<123456;++i){
		printf("%d\n",i);
		IArrayList_add64(al,i);
	}
	for(int i=0;i<al->size;i+=2){
		int64_t value=*(int64_t*)(al->values+i);
		if(value!=i/2)
			printf("wrong value %ld != %d\n",value, i);
	}
	return 0;
}
