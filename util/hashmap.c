#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <math.h>
#include "mymalloc.h"
#include <string.h>
#include "../types.h"
#include "../lang/string.h"
#include "hashmap.h"
#include "../lang/string.h"
#include "hashmap.h"

//initialSize should be power of 2
HashMap * newHashMap(unsigned int initialSize){
	HashMap * hm=malloc(sizeof(HashMap));
	hm->arrSize=initialSize-1;
	hm->entries=calloc(initialSize,sizeof(Entry*));
	return hm;
}
//roundes initialSize up to next power of 2
HashMap * newHashMapRU(unsigned int initialSize){
	//see: https://stackoverflow.com/a/466242/5591561
	unsigned int v=initialSize; // compute the next highest power of 2 of 32-bit v
	--v;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	return newHashMap(v);
}

void HashMap_put(HashMap * hm, String * key, void * value){
	Entry * newEntry=malloc(sizeof(Entry));
	newEntry->key=key;
	newEntry->value=value;
	while(!HashMap_putEntry(hm->entries, newEntry, hm->arrSize)){		
		//fprintf(stderr, "growing to: %d\n",(hm->arrSize+1)*2);
			HashMap_grow(hm,(hm->arrSize+1)*2);
	}

	/*

	int hashCode=String_hashCode(key);
	int pos=hashCode & hm->arrSize;
	Entry * entry=hm->entries[pos];
	if(entry!=NULL){
		if(String_equals(entry->key,key)==1){
			entry->value=value;
			return;
		}
		bool grow=true;
		for(int i=1;i<128;i+=i){
			entry=hm->entries[pos+i];
			if(entry==NULL){
				grow=false;
				pos+=i;
			}
			else if(String_equals(entry->key,key)){
					entry->value=value;
					return;
			}
		}
		if(grow){
			HashMap_grow(hm,(hm->arrSize+1)*2);
			HashMap_put(hm,key,value);
			return;
		}
	}
	hm->entries[pos]=newEntry;
	*/
}

bool HashMap_putEntry(Entry ** entries, Entry * entry, int length){
	int hashCode=String_hashCode(entry->key);
	int pos=hashCode & length;
	Entry * test=entries[pos];
	if(test!=NULL){
		if(String_equals(entry->key,test->key)){
			test->value=entry->value;
			fprintf(stderr, "entry replaced for key: ");
			fprintln(stderr, test->key);
			free(entry);
			return true;
		}
		int trials=length*2;
		//fprintf(stderr, "trials: %d\n", trials);
		for(int i=1;i<trials;i*=2){
			int newPos=(pos+i)&length;
			test=entries[newPos];
			if(test==NULL){
				entries[newPos]=entry;
				return true;
			}
			else if(String_equals(entry->key,test->key)){
					test->value=entry->value;
					fprintf(stderr, "entry replaced for key: ");
					fprintln(stderr, test->key);
					free(entry);
					return true;
			}
		}
		return false;
	}
	entries[pos]=entry;
	return true;
}

void * HashMap_get(HashMap * hm, String * key){
	int hashCode=String_hashCode(key);
	int pos=hashCode & hm->arrSize;

	Entry * entry=hm->entries[pos];
	if(entry==NULL)return NULL;
	if(String_equals(entry->key,key))
		return entry->value;
	int length=hm->arrSize;
	int trials=length*2;
	for(int i=1;i<trials;i*=2){
		int newPos=(pos+i)&length;
		Entry * entry=hm->entries[newPos];
		if(entry==NULL)return NULL;
		if(String_equals(entry->key,key))
			return entry->value;
	}
	return NULL;
}

void HashMap_grow(HashMap * hm, int newSize){
	int arrSize=newSize-1;
	Entry ** oldData=hm->entries;
	int oldSize=hm->arrSize+1;
	Entry ** newData=calloc(newSize,sizeof(Entry*));
	for(int i=0;i<oldSize;++i){
		Entry * entry=oldData[i];
		if(entry!=NULL)
			if(!HashMap_putEntry(newData,entry,arrSize)){
				free(newData);
				HashMap_grow(hm,newSize*2);
				return;
			}
	}
	hm->entries=newData;
	hm->arrSize=arrSize;
	free(oldData);
}

void HashMap_dump(HashMap *hm){
	printf("HashMap dump:\n");
	Entry ** entries=hm->entries;
	int l=hm->arrSize+1;
	for(int i=0;i<l;++i){
		Entry * entry=entries[i];
		if(entry!=0){
			String * value=(String *)entry->value;
			print(entry->key);
			printf(" -> ");
			println(value);
		}
	}
}

void HashMap_delete(HashMap * hm){
	Entry ** entries=hm->entries;
	int length=hm->arrSize+1;
	for(int i=0;i<length;++i){
		Entry * entry=entries[i];
		if(entry!=NULL){
			free(entry);
		}
	}
	free(entries);
	free(hm);
}

