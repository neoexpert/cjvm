#include <stdio.h>
#include <stdlib.h>
#include "mymalloc.h"
#include <inttypes.h>
#include <string.h>
#include "../types.h"
#include "linkedlist.h"

LinkedList* newLinkedList(){
	LinkedList * ll=malloc(sizeof(LinkedList));
	ll->first=0;
	ll->last=0;
	return ll;
}

Node* newNode(void * value){
	Node * n= malloc(sizeof(Node));
	n->value=value;
	n->next=NULL;
	return n;
}

Node * LinkedList_add(LinkedList * ll, void * value){
	Node * node=newNode(value);
	Node * n = ll->last;
	if(n==NULL){
		ll->last=node;
		ll->first=node;
		node->prev=NULL;
		return node;
	}
	n->next=node;
	ll->last=node;
	node->prev=n;
	return node;
}

Node * LinkedList_addFirst(LinkedList * ll, void * value){
	Node * node=newNode(value);
	Node * n = ll->first;
	if(n==NULL){
		ll->last=node;
		ll->first=node;
		node->prev=NULL;
		return node;
	}
	node->next=n;
	node->prev=NULL;
	ll->first=node;
	return node;
}

void* LinkedList_removeFirst(LinkedList * ll){
	Node * first=ll->first;
	if(first==0)
		return 0;
	ll->first=first->next;
	void * value=first->value;
	free(first);
	return value;
}

void LinkedList_removeNode(LinkedList * ll, Node * n){
	if(n->prev!=NULL)
		n->prev->next=n->next;
	else
		ll->first=n->next;
	if(n->next!=NULL)
		n->next->prev=n->prev;
	else
		ll->last=n->prev;
	free(n);
}

void LinkedList_dump(LinkedList * ll){
	Node * n=ll->first;
	int pos=0;
	while(n!=0){
		printf("%d: -> %d\n",pos++,*((int *)n->value));
		n=n->next;
	}
}
