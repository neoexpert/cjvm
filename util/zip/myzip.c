#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <zip.h>
#include "../../types.h"
#include "../../lang/string.h"
#include "../../io/buffer.h"
#include "myzip.h"

bool openRTJAR(){
    //Open the ZIP archive
    int err = 0;
    rt = zip_open("./rt.jar", 0, &err);
		if(err!=0){
			fprintf(stderr, "error opening ./rt.jar file: %d\n",err);
			return false;
		}
		return true;
}

void * openZipFile(char * path){
	//Open the ZIP archive
	int err = 0;
	zip * file = zip_open(path, 0, &err);
	if(err!=0){
		fprintf(stderr, "error opening zip file %s: %d\n", path, err);
		return NULL;
	}
	return file;
}

void closeRTJAR(){
    zip_close(rt);
}

Buffer * readzipEntry(zip * zipFile, const char * path){
    //Open the ZIP archive

    //Search for the file of given name
    struct zip_stat st;
    zip_stat_init(&st);
    zip_stat(zipFile, path, 0, &st);

    //Alloc memory for its uncompressed contents

    //Read the compressed file
    zip_file *f = zip_fopen(zipFile, path, 0);
    if(f==NULL){
	    return NULL;
    }
    int8_t * contents = malloc(st.size);
    zip_fread(f, contents, st.size);
    zip_fclose(f);


    //Do something with the contents
    //delete allocated memory
    Buffer * buf=malloc(sizeof(Buffer));
    buf->buf=contents;
    buf->pos=0;
    buf->capacity=st.size;
    return buf;
}
