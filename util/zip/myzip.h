#ifndef MYZIP_H
#define MYZIP_H
typedef struct zip zip;
typedef struct zip_file zip_file;
Buffer * readzipEntry(zip * zipFile,const char * path);
bool openRTJAR();
void * openZipFile(char * path);
zip * rt;
#endif
