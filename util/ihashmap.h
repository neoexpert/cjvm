#ifndef INTHASHMAP_H
#define INTHASHMAP_H
typedef struct{
	int key;
	void * value;
}IEntry;
typedef struct {
	IEntry ** entries;
	int arrSize;
}IHashMap;
//initialSize should be power of 2
IHashMap * newIHashMap(unsigned int initialSize);

//roundes initialSize up to next power of 2
IHashMap * newIHashMapRU(unsigned int initialSize);
void IHashMap_put(IHashMap * hm, int key, void * value);
int IHashMap_putEntry(IEntry ** entries, IEntry * newEntry, int length);
void * IHashMap_get(IHashMap *hm, int key);
void IHashMap_grow(IHashMap * hm, int newSize);
void IHashMap_dump(IHashMap *hm);
void IHashMap_delete(IHashMap * hm);
#endif
