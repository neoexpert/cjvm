#include <stdio.h>
#include <stdlib.h>
#include "mymalloc.h"
#include <inttypes.h>
#include <string.h>
#include "../types.h"
#include "i8arraylist.h"

I8ArrayList* newI8ArrayList(int initialCapacity){
	I8ArrayList * iai = malloc(sizeof(I8ArrayList));
	iai->size=0;
	iai->capacity=initialCapacity;
	iai->values = malloc(initialCapacity);
	return iai;
}

void* I8ArrayList_remove(I8ArrayList * iai, int index){
	return NULL;
}

void I8ArrayList_add(I8ArrayList * iai, int8_t value){
	if(iai->size>=iai->capacity){
		int newCapacity=(iai->capacity*3)/2+1;
		iai->values=realloc(iai->values,newCapacity);
		iai->capacity=newCapacity;
	}
	iai->values[iai->size++]=value;
}

void I8ArrayList_fixSize(I8ArrayList * iai){
	iai->values=realloc(iai->values,iai->size);
}
