#include <stdio.h>
#include <stdlib.h>
#include "mymalloc.h"
#include <inttypes.h>
#include <string.h>
#include "../types.h"
#include "iarraylist.h"

IArrayList* newIArrayList(int initialCapacity){
	IArrayList * iai = malloc(sizeof(IArrayList));
	iai->size=0;
	iai->capacity=initialCapacity;
	iai->values = calloc(initialCapacity,sizeof(int32_t));
	return iai;
}

void * IArrayList_remove(IArrayList * iai, int index){
	return NULL;
}

void IArrayList_add(IArrayList * iai, int32_t value){
	if(iai->size>=iai->capacity){
		int newCapacity=(iai->capacity*3)/2+4;
		iai->values=realloc(iai->values,newCapacity*sizeof(int32_t));
		iai->capacity=newCapacity;
	}
	iai->values[iai->size++]=value;
}

void IArrayList_add64(IArrayList * iai, int64_t value){
	if(iai->size+2>=iai->capacity){
		int newCapacity=(iai->capacity*3)/2+8;
		iai->values=realloc(iai->values,newCapacity*sizeof(int32_t));
		iai->capacity=newCapacity;
	}
	*(int64_t*)(iai->values+iai->size)=value;
	iai->size+=2;
}

void IArrayList_fixSize(IArrayList * iai){
		iai->values=realloc(iai->values,iai->size*sizeof(int32_t));
}
