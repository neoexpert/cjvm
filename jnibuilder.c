#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>

uint16_t _calcNativeMethodType(int8_t returnType, char * args, int16_t len){
	int actual_length;
	int16_t type=0;
	for(int i=0;i<len;++i){
		switch(args[i]){
			default:
				type+=1*pow(4,i);
				continue;
			case 'L':
			case '[':
				type+=2*pow(4,i);
				continue;
			case 'J':
			case 'D':
				type+=3*pow(4,i);
				continue;

		}
	}
	type*=4;
	switch(returnType){
		case 'V':
			return type;
		default:
			return type+1;
		case 'L':
		case '[':
			return type+2;
		case 'J':
		case 'D':
			return type+3;
	}
}
bool addOne(char * args, int off, int maxLen){
	if(off>=maxLen) return false;
		switch(args[off]){
			case 'I':
				args[off]='L';
				break;
			case 'L':
				args[off]='J';
				break;
			case 'J':
				args[off]='I';
				return addOne(args, off+1, maxLen);
			default:
				args[off]='I';
				break;
				
		}
		return true;
}

void buildFunctionPointer(FILE * file, char rtype, char * args, int len){
	fprintf(file, "typedef ");
	switch(rtype){
		default:
			fprintf(file, "int32_t f_%s_%c(void * env", args, rtype);
			break;
		case 'V':
			fprintf(file, "void f_%s_%c(void * env", args, rtype);
			break;
		case 'L':
		case '[':
			fprintf(file, "uint32_t * f_%s_%c(void * env", args, rtype);
			break;
		case 'J':
		case 'D':
			fprintf(file, "int64_t f_%s_%c(void * env", args, rtype);
			break;
	}
	for(int i=0;i<len;++i){
		switch(args[i]){
			default:
				fprintf(file, ", int32_t");
				break;
			case 'L':
			case '[':
				fprintf(file, ", uint32_t *");
				break;
			case 'J':
			case 'D':
				fprintf(file, ", int64_t");
			break;
		}
	}
	fprintf(file, ");\n");

}
void buildArguments(FILE * file, char * args, int len){
	int offset=0;
	for(int i=0;i<len;++i){
		switch(args[i]){
			default:
				fprintf(file, ", lv[%d]", offset);
				offset+=1;
				break;
			case 'L':
			case '[':
				fprintf(file, ", (uint32_t*)&lv[%d]", offset);
				offset+=1;
				break;
			case 'J':
			case 'D':
				fprintf(file, ", *(int64_t*)(lv+%d)", offset);
				offset+=2;
			break;
		}
	}
}
int main(int argc, char ** argv){
	if(argc<=1)return 0;
	int maxArgs = atoi(argv[1]);
	FILE * jniHeader=fopen("jni/jni_handler.h","w");
	FILE * jniSource=fopen("jni/jni_handler.c","w");
	fprintf(jniSource, "//this file is automatically generated\n");
	fprintf(jniSource, "#include <stdlib.h>\n");
	fprintf(jniSource, "#include <stdarg.h>\n");
	fprintf(jniSource, "#include <stdio.h>\n");
	fprintf(jniSource, "#include <stdbool.h>\n");
	fprintf(jniSource, "#include <inttypes.h>\n");
	fprintf(jniSource, "#include \"jni_handler.h\"\n");
	fprintf(jniSource, "#include \"../lang/string.h\"\n");
	fprintf(jniSource, "#include \"../util/hashmap.h\"\n");
	fprintf(jniSource, "#include \"../util/arraylist.h\"\n");
	fprintf(jniSource, "#include \"../io/buffer.h\"\n");
	fprintf(jniSource, "#include \"../jvm/constant.h\"\n");
	fprintf(jniSource, "#include \"../jvm/attribute.h\"\n");
	fprintf(jniSource, "#include \"../jvm/field.h\"\n");
	fprintf(jniSource, "#include \"../jvm/method.h\"\n");
	fprintf(jniSource, "#include \"../jvm/class.h\"\n");
	fprintf(jniSource, "#include \"../jvm/frame.h\"\n");
	fprintf(jniSource, "#include \"../jvm/jni.h\"\n");
	fprintf(jniSource, "int jni_handler(int type, void * handler, int32_t * lv, int32_t * opstack){\n");
	fprintf(jniSource, "\tuint32_t * ref;\n");
	fprintf(jniSource, "\tswitch(type){\n");
	char * args=calloc(maxArgs,1);
	//args[0]='I';
	fprintf(jniHeader, "int jni_handler(int type, void * handler, int32_t * lv, int32_t * opstack);\n");
	while(true){
		int len=strlen(args);
		fprintf(jniHeader, "#define _%s_V %d\n", args,  _calcNativeMethodType('V', args, len));
		buildFunctionPointer(jniHeader, 'V', args, len);
		fprintf(jniSource, "\t\tcase _%s_V:\n", args);
		fprintf(jniSource, "\t\t\t((f_%s_V*)(handler))(jniEnv", args);
		buildArguments(jniSource, args, len);
		fprintf(jniSource, ");\n");
		fprintf(jniSource, "\t\t\treturn 0;\n");

		fprintf(jniHeader, "#define _%s_I %d\n", args,  _calcNativeMethodType('I', args, len));
		buildFunctionPointer(jniHeader, 'I', args, len);
		fprintf(jniSource, "\t\tcase _%s_I:\n", args);
		fprintf(jniSource, "\t\t\topstack[0]=((f_%s_I*)(handler))(jniEnv", args);
		buildArguments(jniSource, args, len);
		fprintf(jniSource, ");\n");
		fprintf(jniSource, "\t\t\treturn 1;\n");

		fprintf(jniHeader, "#define _%s_L %d\n", args,  _calcNativeMethodType('L', args, len));
		buildFunctionPointer(jniHeader, 'L', args, len);
		fprintf(jniSource, "\t\tcase _%s_L:\n", args);
		fprintf(jniSource, "\t\t\tref=((f_%s_L*)(handler))(jniEnv", args);
		buildArguments(jniSource, args, len);
		fprintf(jniSource, ");\n");
		fprintf(jniSource, "\t\t\topstack[0]=(ref==NULL?0:*ref);\n");
		fprintf(jniSource, "\t\t\treturn 1;\n");

		fprintf(jniHeader, "#define _%s_J %d\n", args,  _calcNativeMethodType('J', args, len));
		buildFunctionPointer(jniHeader, 'J', args, len);
		fprintf(jniSource, "\t\tcase _%s_J:\n", args);
		fprintf(jniSource, "\t\t\t*(int64_t*)(opstack)=((f_%s_J*)(handler))(jniEnv", args);
		buildArguments(jniSource, args, len);
		fprintf(jniSource, ");\n");
		fprintf(jniSource, "\t\t\treturn 2;\n");

		if(!addOne(args, 0, maxArgs)){
			fclose(jniHeader);
			fprintf(jniSource, "\t\tdefault:\n");
			fprintf(jniSource, "\t\t\tfprintf(stderr, \"unknown native method type: %%d\\n\", type);\n");
			fprintf(jniSource, "\t\t\texit(-1);\n");
			fprintf(jniSource, "\t\t\treturn 0;\n");
			fprintf(jniSource, "\t}\n");
			fprintf(jniSource, "}\n");
			fclose(jniSource);
			return 0;
		}
	}
}
